import os

DRY_RUN = False
DOMAIN = "https://docs.flow3r.garden"
BUILD_DIR = "_build"
FILE_TEMPLATE = '<meta http-equiv="refresh" content="0; url={url}" />\n<p><a href="{url}">Redirect</a></p>'

redirects = [
    # (obsolete, target)
    ("badge/firmware.html", "os/guide/structure.html"),
    ("badge/fonts.html", "app/guide/fonts.html"),
    ("badge/usage.html", "badge/getting_started.html"),
    ("badge/programming.html", "app/guide/basics.html"),
    ("badge/badge_link.html", "app/guide/badge_link.html"),
    ("badge/badge_net.html", "app/guide/badge_net.html"),
    ("badge/bl00mbox.html", "app/guide/bl00mbox.html"),
    ("badge/firmware-development.html", "os/guide/development.html"),
    ("api/badgenet.html","app/api/badgenet.html"),
    ("api/sys_display.html", "os/api/sys_display.html"),
    ("api/ctx.html", "app/api/ctx.html"),
    ("api/badgelink.html", "app/api/badgelink.html"),
    ("api/audio.html", "app/api/audio.html"),
    ("api/colours.html", "app/api/colours.html"),
    ("api/leds.html", "app/api/leds.html"),
    ("api/led_patterns.html", "app/api/led_patterns.html"),
    ("api/uos.html", "app/api/uos.html"),
    ("api/sys_kernel.html", "os/api/sys_kernel.html"),
    ("api/sys_buttons.html", "app/api/input.html"),
    ("api/captouch.html", "app/api/captouch.html"),
]

for obsolete, target in redirects:
    redirect_file_content = FILE_TEMPLATE.format(url = "/".join([DOMAIN, target]))
    redirect_file_path = os.path.join(BUILD_DIR, obsolete)
    
    if not DRY_RUN:
        redirect_dir_path, _ = os.path.split(redirect_file_path)
        os.makedirs(redirect_dir_path, exist_ok = True)

    if not os.path.isfile(redirect_file_path):
        print(f"writing to {redirect_file_path}")
        if not DRY_RUN:
            with open(redirect_file_path, "w") as f:
                f.write(redirect_file_content)
        else:
            print(f"{redirect_file_content}\n")
    else:
        print(f"file {redirect_file_path} already exists")
        
