Getting started
===============

Hold your flow3r with the pink part facing towards you, and the USB port facing
upwards

.. image:: overview.svg
  :width: 700px


Powering your flow3r
^^^^^^^^^^^^^^^^^^^^

The Flow3r needs electricity to run - either from a battery or over its USB port.

Once it has power available, you can turn it on by moving the right-hand side
power switch (next to the 'flow3r' label on the front of the badge) towards the
right.

You should then see the badge come to life and display 'Starting...' on the screen.

First boot calibration
^^^^^^^^^^^^^^^^^^^^^^

At the first boot flow3r needs to calibrate its captouch driver. The system will
guide you through this process. While calibration is happening, do not touch the
petals: The calibrating routine only cares about the baseline level, not the
response to touch and therefore expects no interaction.

This calibration can be repeated anytime at `System -> Settings -> Captouch Calibrator`.

Navigating the menu
^^^^^^^^^^^^^^^^^^^

The app shoulder button (left shoulder unless swapped in settings) is used to
navigate the menus of the badge. Pressing it left and right selects an option
in the menu. Pressing it down selects a menu option.

The OS shoulder button (right shoulder unless swapped in settings) can be
pressed down to quickly return 'back', either in a menu or an app.

Set volume
^^^^^^^^^^

flow3r has two built-in speakers. Their loudness can always be adjusted by
using the OS shoulder button (right shoulder unless swapped in settings), left
for lowering the volume and right for making it louder.

You can plug in a pair of headphones to the 3.5mm jack on the bottom-left petal.
The built-in speakers will then turn off and audio will go out through the
headphones. You can adjust their volume in the same way.

Use the context menus
^^^^^^^^^^^^^^^^^^^^^

flow3r has a context menu that is available at all times. Access it by holding
down the OS button for 1s. This menu provides 3 features for now:

- **help:** Applications may provide help texts to tell you how to use
  them. This is a relatively recent feature so it's not widely implemented yet,
  but we hope to change this soon!

- **mixer:** Some music apps can keep playing in the background after
  exiting them. The mixer allows to set volume levels for those different
  sources.

- **exit app:** Most apps can be exited by simply going back enough times
  by tapping the OS button a bunch. Some applications may not implement this
  properly though, in that case, you can always use this backup option to exit.
  If you are not in an application this option is called **go home** and returns
  you to the starting menu.

There is a secondary context menu which exists only when hovering over an
application in the system menus. You can access it by holding down the app
button for 1s. This menu provides 3 features:

- **fav:** Favorite apps are sorted to the top of the menu so that you can
  quickly access them. They are marked with a little "<3" next to the name.

- **boot:** This app is launched automatically at startup. Only one app
  can have this property. Autostart can be prevented by holding down any
  shoulder button.

- **delete:** Delete the application.

Set the handedness
^^^^^^^^^^^^^^^^^^

Many applications are intended to be used while holding flow3r in one hand
and touching the petals with the other. Ideally, your holding hand should be
able to comfortably reach the app button in this mode. For operating it
with the right index finger for example we personally find it most comfortable
to have the app button on the right side.

You can change this setting depending on your preferences in
`System -> Settings -> Swap Buttons`.

Making applications that work well for either handedness requires some degree
of cooperation from application developers so app store experience may vary.

Use apps
^^^^^^^^

The main menu shows a few different app categories, plus the `System` submenu.
Those are:

- **Badge:** These apps are intended to be used as a name tag for events. They
  usually need you to configure your personal display data as described in the
  `Configuration` section.

- **Music:** These apps are musical instruments and typically produce sound in
  some or the other way.

- **Media:** These apps replay media.

- **Games:** Games of all sorts. No stock application is in this category as of
  yet so it is hidden until you download one. 

- **Apps:** Catchall for apps that don't really fit in any other categories.

If it's unclear how an application is supposed to work, try the **help**
option in the context menu.

To exit an application, press the OS button down (maybe repeatedly), or fall
back to the context menu **exit app** option if this fails.

Music applications may continue playing in the background after they have
been exited; there is typically a means to avoid that which should be listed
in the help menu, but if not you can use the context menu **mixer** to mute
them.

Download new apps
^^^^^^^^^^^^^^^^^

You can find many community created apps in `System -> Get Apps`. You need an
active WiFi connection to access the app store. The WiFi menu will open
automatically if none is found.

Not all apps in the app store may be fully functional as flow3r firmware
evolved a lot since it was first released.
