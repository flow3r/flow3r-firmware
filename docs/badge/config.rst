.. include:: <isonum.txt>

Configuration
=============

System
------

Settings
^^^^^^^^

Menu for setting various system parameters.

- ``WiFi``: Enter SSID and password here to connect to WiFi. Note: WiFi consumes a lot of system resources, if you feel like an application is struggling try turning off WiFi. Petal 0 toggles WiFi, a bottom bar shows you the status. If WiFi is active and networks have been found, they are shown in a list. To connect, select one with the app button. The keyboard works similar to T9, with multiple presses on the top petals selecting characters from their displayed list with a timeout and the bottom petals performing additional state switching and text operations. Confirm by pressing the app button down. If the connection is saved, it is shown in yellow, while connecting in blue, and when connected in green.
- ``Enable WiFi on Boot``: Will attempt to connect to known WiFi networks at boot time.
- ``Show Icons``: Displays battery voltage and USB connection status overlay in menu screens.
- ``Swap buttons``: Use right button as app button and left button as os button instead of the other way around.
- ``Touch OS``: Activate captouch navigation in the system menus. Open help in a system menu while active for navigation details.
- ``Show FPS``: Displays FPS overlay.
- ``Debug: ftop``: Prints a task cpu load and memory report every 5 seconds on the USB serial port.
- ``Touch Overlay``: If a petal is pressed the positional output is displayed in an overlay.
- ``Restore Defaults``: Restores default settings.

A settings file with more options including headphone and speaker max/min volume and volume
adjust step is on the flash filesystem at ``/settings.json``.

Graphics Mode
^^^^^^^^^^^^^

Various graphics settings. If ``lock`` is enabled applications can not override these,
else they can set it to their individual preferences at runtime.


Get Apps
^^^^^^^^

Enter the app store. Requires WiFi connection.

Disk Mode (Flash)
^^^^^^^^^^^^^^^^^

Make the flash filesystem accessible as a block device via USB. Reboots on exit.

Disk Mode (SD)
^^^^^^^^^^^^^^^^^

Make the SD card filesystem accessible as a block device via USB. Reboots on exit.

Yeet local changes
^^^^^^^^^^^^^^^^^^

Restores python payload to the state of the last firmware updatei and reboots. This excludes
settings and files not present in the original payload.

Reboot
^^^^^^

Reboot flow3r.

Setting nick and pronouns
-------------------------

You can navigate to Badge |rarr| Nick to display your nick and pronouns. If
your nick is ``flow3r``, and you have no pronouns, congratulations! You're
ready to go. Otherwise, you'll have to connect your badge to a computer and
edit a file to change your nick and pronouns.

From the main menu, navigate to System |rarr| Disk Mode (Flash). Connect your
badge to a computer, and it will appear as a mass storage device (a.k.a.
pendrive). Open the file ```nick.json`` in a text editor and change your nick,
pronouns, font sizes for nick and pronouns, and whatever else you wish. Please
note that ``pronouns`` is a list, and should be formatted as such. for example:
``"pronouns": ["aa/bb", "cc/dd"],``

For the ``nick.json`` file to appear, you must have started the Nick app at
least once.

Use ``"color": "0xffffff",`` to color your name and pronouns.

Use ``"mode": "1",`` to use a different animation mode rotating your nick based on badge orientation.

When you're done editing, unmount/eject the badge from your computer
(``umount`` on Linux is enough) and press the OS shoulder button (right shoulder unless swapped in
settings) to exit Disk Mode. Then, go to Badge |rarr| Nick to see your changes!

If the ``nick.json`` file is unparseable or otherwise gets corrupted, it will be
overwritten with the default contents on next nick app startup.

