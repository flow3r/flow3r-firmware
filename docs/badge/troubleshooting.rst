Troubleshooting
---------------

*This page assumes that you're running the latest stable firmware. As such, we
suggest ensuring that you're on latest firmware before proceeding with
troubleshooting.*

Captouch doesn't work properly
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If petal presses are not registered or fake inputs occur randomly, recalibrate
the captouch driver in `System -> Settings -> Captouch Calibrator`. You can
visualize with `captouch demo` how your current calibration performs. Having a
USB cable or audio cables connected has slight influence on calibration, this
is usually negligible but if you don't reach satisfactory results it might be
worthwhile to try remove all connections (except for USB, if you don't have a
battery installed) before calibration.

If you have issues with positional captouch, be aware that the top petals have
large deadzones. If you struggle with *sometimes* unresponsive scrolling behavior,
it is likely that you go too far away from the center of the petal. You can use
`captouch demo` or the testing screen of `Captouch Calibrator` to familiarize
yourself with the responsive range. This issue sadly cannot be fixed in software.

If this doesn't solve your problems, please open an issue in our `Issue Tracker
<https://git.flow3r.garden/flow3r/flow3r-firmware/-/issues>`_ so that we can have
a look and account for whatever we may find in a future release.

*Note that the system menu doesn't use captouch by default and that the screen
doesn't have captouch functionality. You can activate experimental hybrid
trackpad/button system menu captouch control with System -> Settings -> Touch OS.
Consult the help text of any system menu after activating it if its behavior
confuses you.*

updat3r fails
^^^^^^^^^^^^^

updat3r relies on a functional SD card, see SD card issues below for details.

Many old apps are tagged as "new"
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is typically a bad stock SD card, see SD card issues below for details.

Copy all your files to a replacement except for `/user_data/app_settings.json`,
if this file does not exists all apps are registered as known. You can delete
it in the `fil3s` app too, but bad SD cards may result in the bug reoccuring.

SD card issues
^^^^^^^^^^^^^^

Sadly, some SD cards that flow3r shipped with do not work well with the driver.
Since many applications store their data on SD card this also means they might
run into issues (it's a random issue that occurs more with larger files so they
may appear to work just fine most of the time), it's best in this case to swap
to a different SD card.

*Note: We have found a potential pattern in the unreliable SD cards. You can check
the SD card status on the second page of System -> About. If our suspected pattern
matches, there will be a (Unreliable model) warning displayed above the storage
indicator.*

You can do so by carefully opening your badge and replacing the stock microSD
card with a new one, formatted as FAT32. We have used microSD cards of various
sizes without issues (16-256GB). Be careful not to overtighten the screws when
reassembling as this can rip the screw holders out of the PCB fairly easily!

You might want to prefill the `/music/` folder with your favorite .mp3 tunes
to use with the mp3 player app as file transfers via flow3r USB to the SD card
are much slower than card readers.

If you accidentially ripped off a screw holder it's possible to fix it with
some careful and patient soldering, we'll try to add a video guide soon!
*(If you do wanna try it beforehand: Be careful, it needs a lot of heat which
makes it easy to "swipe off" other tiny components that are harder to repair.
This is not a safe first soldering experience.)*

Volume control doesn't work
^^^^^^^^^^^^^^^^^^^^^^^^^^^

You probably have the `Show FPS` option enabled in the settings. This is primarily
a debug feature and blocks every other overlay since drawing them affects frame
rate negatively (including the FPS monitor). This behavior will probably be
improved in a future firmware release.
