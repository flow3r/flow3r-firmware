Blinky
======

Let's start cold with two examples to get a feel for the code. The upcoming `Environment` section
will show you how to run them, the `Basics` section after that will explain how the code works.

Auto Blinky
-----------

.. code-block:: python

    from st3m.application import Application
    import leds

    class AutoBlinky(Application):
        def get_help(self):
            return "This app blinks all LEDs and the display automatically."

        def __init__(self, app_ctx):
            super().__init__(app_ctx)

            self.timer_ms = 0
            self.colors = (
                (1,1,0),
                (0,1,1),
                (1,0,1),
            )
            self.active_color = self.colors[0]
            self.blink_time_ms = 500

        def think(self, ins, delta_ms):
            super().think(ins, delta_ms)
            self.timer_ms += delta_ms
            self.timer_ms %= self.blink_time_ms * len(self.colors)

            index = self.timer_ms // self.blink_time_ms
            self.active_color = self.colors[index]

            leds.set_all_rgb(*self.active_color)
            leds.update()

        def draw(self, ctx):
            ctx.rgb(*self.active_color).rectangle(-120, -120, 240, 240).fill()


Note how the active color is not reset when exiting and entering the application: The object does not
get destroyed when the user exits (see ``Application.on_enter()`` in the ``st3m.application`` module).

Captouch Blinky
---------------

.. code-block:: python

    from st3m.application import Application
    import leds

    class CaptouchBlinky(Application):
        def get_help(self):
            context_sensitive_help = (
                "This app changes color of all LEDs and the display "
                "when touching a top petal. "
            )
            context_sensitive_help += f"The current RGB values are {self.active_color}."
            return context_sensitive_help

        def __init__(self, app_ctx):
            super().__init__(app_ctx)

            self.colors = (
                (1,1,0),
                (0,1,1),
                (1,0,1),
                (0,1,0),
                (0,0,1),
            )
            self.active_color = self.colors[0]

        def think(self, ins, delta_ms):
            super().think(ins, delta_ms)

            for x in range(0, 10, 2):
                if self.input.captouch.petals[x].whole.pressed:
                    self.active_color = self.colors[x//2]

            leds.set_all_rgb(*self.active_color)
            leds.update()

        def draw(self, ctx):
            ctx.rgb(*self.active_color).rectangle(-120, -120, 240, 240).fill()

The ``self.input`` object does edge detection here (documented in the ``st3m.input`` module).
