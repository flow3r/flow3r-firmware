.. flow3r documentation master file, created by
   sphinx-quickstart on Sun Jun 11 19:43:11 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to flow3r's documentation!
==================================

.. toctree::
   :maxdepth: 1
   :caption: User manual

   badge/assembly.rst
   badge/getting_started.rst
   badge/config.rst
   badge/updating_firmware.rst
   badge/troubleshooting.rst
   badge/mods.rst
   badge/hardware_specs.rst

.. toctree::
   :maxdepth: 1
   :caption: App programming

   app/guide/blinky.rst
   app/guide/environment.rst
   app/guide/basics.rst
   app/guide/savefiles.rst
   app/guide/bl00mbox.rst
   app/guide/fonts.rst
   app/guide/badge_link.rst
   app/guide/badge_net.rst
   app/guide/qwiic.rst

.. toctree::
   :maxdepth: 1
   :caption: App API

   app/api/application.rst
   app/api/input.rst
   app/api/captouch.rst
   app/api/widgets.rst
   app/api/ctx.rst
   app/api/colours.rst
   app/api/leds.rst
   app/api/led_patterns.rst
   app/api/view.rst
   app/api/audio.rst
   app/api/badgelink.rst
   app/api/badgenet.rst
   app/api/uos.rst

.. toctree::
   :maxdepth: 1
   :caption: OS programming

   os/guide/structure.rst
   os/guide/development.rst

.. toctree::
   :maxdepth: 1
   :caption: OS API

   os/api/sys_audio.rst
   os/api/sys_display.rst
   os/api/sys_kernel.rst

.. toctree::
   :maxdepth: 1
   :caption: About

   badge/license.rst

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
