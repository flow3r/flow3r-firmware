#include "bl00mbox_os.h"

#ifdef BL00MBOX_FREERTOS
bool bl00mbox_create_lock(bl00mbox_lock_t * lock){
    assert(*lock == NULL);
    * lock = xSemaphoreCreateMutex();
    return(*lock != NULL);
}
void bl00mbox_delete_lock(bl00mbox_lock_t * lock){ vSemaphoreDelete(* lock); }
void bl00mbox_take_lock(bl00mbox_lock_t * lock){ xSemaphoreTake(* lock, portMAX_DELAY); }
void bl00mbox_give_lock(bl00mbox_lock_t * lock){ xSemaphoreGive(* lock); }
#ifdef BL00MBOX_DEBUG
void bl00mbox_wait() { vTaskDelay(10); }
#endif
#endif

#if defined(BL00MBOX_ESPIDF)
// macros in header
#elif defined(BL00MBOX_PRINTF)
#include <stdarg.h>
#include <string.h>
static void _bl00mbox_log(char * pre, char * txt, va_list args){
    int len = strlen(pre) + strlen(txt) + 2;
    char msg[len];
    snprintf(msg, len, "%s%s\n", pre, txt);
    vprintf(msg, args);
}
void bl00mbox_log_error(char * txt, ...){
    va_list args;
    va_start(args, txt);
    _bl00mbox_log("bl00mbox error: ", txt, args);
    va_end(args);
};
void bl00mbox_log_info(char * txt, ...){
    va_list args;
    va_start(args, txt);
    _bl00mbox_log("bl00mbox info: ", txt, args);
    va_end(args);
};
#else
void bl00mbox_log_error(char * txt, ...){};
void bl00mbox_log_info(char * txt, ...){};
#endif
