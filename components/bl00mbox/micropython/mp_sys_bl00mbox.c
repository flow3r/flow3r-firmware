// SPDX-License-Identifier: CC0-1.0
#include <stdio.h>

#include "py/obj.h"
#include "py/runtime.h"

#include "bl00mbox.h"
#include "bl00mbox_os.h"
#include "bl00mbox_plugin_registry.h"
#include "bl00mbox_user.h"
#include "radspa.h"

#if !MICROPY_ENABLE_FINALISER
#error "bl00mbox needs finaliser"
#endif

#include "py/objexcept.h"
MP_DEFINE_EXCEPTION(ReferenceError, Exception)

typedef struct _channel_core_obj_t {
    mp_obj_base_t base;
    // will be NULLed on original if channel has been manually deleted
    // both for weak and regular
    bl00mbox_channel_t *chan;

    // pointer to object that weakly references original channel object
    // there is no more than one weak channel reference obj per channel
    mp_obj_t weak;

    // things we don't want garbage collected:
    // - channel plugin
    mp_obj_t channel_plugin;
    // - list of all sources connect to the mixer
    mp_obj_t sources_mx;

#ifdef BL00MBOX_CALLBACKS_FUNSAFE
    // the channel may be become reachable after having been unreachable.
    // if the callback was collected during this phase it results in a
    // use-after-free, we're relying on the fact that if the callback
    // was collected the channel must have been collected as well since
    // the garbage collector sweeps everything directly after marking.
    // conservative false positives for the channel are ok since this
    // means the callback can't be collected either.
    // see bl00mbox_config.h for more notes on this.
    mp_obj_t callback;
#endif
} channel_core_obj_t;

STATIC const mp_obj_type_t channel_core_type;

typedef struct _plugin_core_obj_t {
    mp_obj_base_t base;
    // important note: the plugin may have been garbage collected along with
    // its channel or it might have been deleted, in either case this pointer
    // is set to NULL, so you need to check before using it. if you do anything
    // that might trigger a gc (like creating objects) you need to check this
    // pointer for NULL again.
    bl00mbox_plugin_t *plugin;
    // list of all source plugins to avoid gc
    mp_obj_t sources;
} plugin_core_obj_t;

STATIC const mp_obj_type_t plugin_core_type;

typedef struct _signal_core_obj_t {
    mp_obj_base_t base;
    mp_obj_t plugin_core;
    // same rules for garbage collection as for plugin_core_obj_t apply here
    // too, call plugin_core_verify() on plugin_core after each potential
    // garbage collection keeping a direct reference to keep the plugin alive as
    // long as there's a signal in reach (unless the channel gets gc'd of
    // course)
    bl00mbox_signal_t signal;
} signal_core_obj_t;

STATIC const mp_obj_type_t signal_core_type;

STATIC mp_obj_t signal_core_make_new(const mp_obj_type_t *type, size_t n_args,
                                     size_t n_kw, const mp_obj_t *args);

static inline void channel_core_verify(channel_core_obj_t *channel_core) {
    if (!channel_core->chan)
        mp_raise_msg(&mp_type_ReferenceError,
                     MP_ERROR_TEXT("channel was deleted"));
}

static inline void plugin_core_verify(plugin_core_obj_t *plugin_core) {
    if (!plugin_core->plugin)
        mp_raise_msg(&mp_type_ReferenceError,
                     MP_ERROR_TEXT("plugin was deleted"));
}

static void bl00mbox_error_unwrap(bl00mbox_error_t error) {
    switch (error) {
        case BL00MBOX_ERROR_OOM:
            mp_raise_msg(&mp_type_OSError, MP_ERROR_TEXT("out of memory"));
        case BL00MBOX_ERROR_INVALID_CONNECTION:
            mp_raise_TypeError(MP_ERROR_TEXT("can't connect that"));
        case BL00MBOX_ERROR_INVALID_IDENTIFIER:
            mp_raise_TypeError(MP_ERROR_TEXT("bad identifier"));
        default:;
    }
}

void bl00mbox_disconnect_rx_callback(void *rx, uint16_t signal_index) {
    plugin_core_obj_t *self = rx;
    mp_obj_list_t *list = MP_OBJ_TO_PTR(self->sources);
    if (signal_index < list->len) {
        list->items[signal_index] = mp_const_none;
    } else {
        bl00mbox_log_error("plugin signal list too short");
    }
}

void bl00mbox_connect_rx_callback(void *rx, void *tx, uint16_t signal_index) {
    plugin_core_obj_t *self = rx;
    mp_obj_list_t *list = MP_OBJ_TO_PTR(self->sources);
    if (signal_index < list->len) {
        list->items[signal_index] = MP_OBJ_FROM_PTR(tx);
    } else {
        bl00mbox_log_error("plugin signal list too short");
    }
}

void bl00mbox_disconnect_mx_callback(void *chan, void *tx) {
    channel_core_obj_t *self = chan;
    // don't have to check for duplicates, bl00mbox_user does that for us
    mp_obj_list_remove(self->sources_mx, MP_OBJ_FROM_PTR(tx));
}

void bl00mbox_connect_mx_callback(void *chan, void *tx) {
    channel_core_obj_t *self = chan;
    // don't have to check for duplicates, bl00mbox_user does that for us
    mp_obj_list_append(self->sources_mx, MP_OBJ_FROM_PTR(tx));
}

static char *strdup_raise(char *str) {
    char *ret = strdup(str);
    if (!ret) bl00mbox_error_unwrap(BL00MBOX_ERROR_OOM);
    return ret;
}

static mp_obj_t get_connections_from_array(bl00mbox_array_t *array) {
    if (!array) bl00mbox_error_unwrap(BL00MBOX_ERROR_OOM);
    if (array->len) {
        int len = array->len;
        bl00mbox_signal_t *signals[len];
        memcpy(&signals, &array->elems, sizeof(void *) * len);
        // need to free before we can longjump
        free(array);
        mp_obj_t elems[len];
        int output_index = 0;
        for (int i = 0; i < len; i++) {
            plugin_core_obj_t *core = signals[i]->plugin->parent;
            // gc can happen during this loop. the objects we created are safe
            // as they are on the stack, but unprocessed signals might've been
            // collected
            if (!core->plugin) continue;
            mp_obj_t args[2] = { MP_OBJ_FROM_PTR(core),
                                 mp_obj_new_int(signals[i]->index) };
            elems[output_index] =
                signal_core_make_new(&signal_core_type, 2, 0, args);
            output_index++;
        }
        return mp_obj_new_list(output_index, elems);
    } else {
        free(array);
        return mp_obj_new_list(0, NULL);
    }
}

static mp_obj_t get_plugins_from_array(bl00mbox_array_t *array) {
    if (!array) bl00mbox_error_unwrap(BL00MBOX_ERROR_OOM);
    if (array->len) {
        int len = array->len;
        bl00mbox_plugin_t *plugins[len];
        memcpy(&plugins, &array->elems, sizeof(void *) * len);
        // need to free before we can longjump
        free(array);
        mp_obj_t elems[len];
        int output_index = 0;
        for (int i = 0; i < len; i++) {
            plugin_core_obj_t *core = plugins[i]->parent;
            // gc shouldn't happen during this loop but we keep it in anyways :3
            if (!core->plugin) continue;
            elems[output_index] = MP_OBJ_FROM_PTR(core);
            output_index++;
        }
        return mp_obj_new_list(output_index, elems);
    } else {
        free(array);
        return mp_obj_new_list(0, NULL);
    }
}

// ========================
//    PLUGIN REGISTRY
// ========================

// TODO: clean this up

STATIC mp_obj_t mp_plugin_index_get_id(mp_obj_t index) {
    radspa_descriptor_t *desc =
        bl00mbox_plugin_registry_get_descriptor_from_index(
            mp_obj_get_int(index));
    if (desc == NULL) return mp_const_none;
    return mp_obj_new_int(desc->id);
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(mp_plugin_index_get_id_obj,
                                 mp_plugin_index_get_id);

STATIC mp_obj_t mp_plugin_index_get_name(mp_obj_t index) {
    /// prints name
    radspa_descriptor_t *desc =
        bl00mbox_plugin_registry_get_descriptor_from_index(
            mp_obj_get_int(index));
    if (desc == NULL) return mp_const_none;
    return mp_obj_new_str(desc->name, strlen(desc->name));
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(mp_plugin_index_get_name_obj,
                                 mp_plugin_index_get_name);

STATIC mp_obj_t mp_plugin_index_get_description(mp_obj_t index) {
    /// prints name
    radspa_descriptor_t *desc =
        bl00mbox_plugin_registry_get_descriptor_from_index(
            mp_obj_get_int(index));
    if (desc == NULL) return mp_const_none;
    return mp_obj_new_str(desc->description, strlen(desc->description));
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(mp_plugin_index_get_description_obj,
                                 mp_plugin_index_get_description);

STATIC mp_obj_t mp_plugin_registry_num_plugins(void) {
    return mp_obj_new_int(bl00mbox_plugin_registry_get_plugin_num());
}
STATIC MP_DEFINE_CONST_FUN_OBJ_0(mp_plugin_registry_num_plugins_obj,
                                 mp_plugin_registry_num_plugins);

// ========================
//        CHANNELS
// ========================

static mp_obj_t create_channel_plugin(bl00mbox_channel_t *chan) {
    plugin_core_obj_t *self = m_new_obj_with_finaliser(plugin_core_obj_t);
    self->base.type = &plugin_core_type;
    self->plugin = chan->channel_plugin;
    self->plugin->parent = self;
    self->plugin->parent_self_ref = &self->plugin;

    size_t num_signals = self->plugin->rugin->len_signals;
    mp_obj_t nones[num_signals];
    for (size_t i = 0; i < num_signals; i++) nones[i] = mp_const_none;
    self->sources = mp_obj_new_list(num_signals, nones);

    return MP_OBJ_FROM_PTR(self);
}

static mp_obj_t create_weak_channel_core(channel_core_obj_t *self) {
    channel_core_obj_t *weak = m_new_obj_with_finaliser(channel_core_obj_t);
    weak->base.type = &channel_core_type;
    weak->weak = MP_OBJ_FROM_PTR(weak);
    weak->chan = self->chan;
    weak->chan->weak_parent_self_ref = &weak->chan;

    // only the real channel should prevent these from being gc'd
    weak->sources_mx = MP_OBJ_NULL;
    weak->channel_plugin = MP_OBJ_NULL;
#ifdef BL00MBOX_CALLBACKS_FUNSAFE
    weak->callback = MP_OBJ_NULL;
#endif
    return MP_OBJ_FROM_PTR(weak);
}

STATIC mp_obj_t channel_core_make_new(const mp_obj_type_t *type, size_t n_args,
                                      size_t n_kw, const mp_obj_t *args) {
    mp_arg_check_num(n_args, n_kw, 1, 1, false);
    channel_core_obj_t *self = m_new_obj_with_finaliser(channel_core_obj_t);
    self->chan = NULL;  // must initialize if obj gets gc'd due to exception

    self->base.type = &channel_core_type;
    self->sources_mx = mp_obj_new_list(0, NULL);
    const char *name = strdup_raise(mp_obj_str_get_str(args[0]));

    bl00mbox_channel_t *chan = bl00mbox_channel_create();
    if (!chan) bl00mbox_error_unwrap(BL00MBOX_ERROR_OOM);

    chan->name = name;
    chan->parent_self_ref = &self->chan;
    chan->parent = self;

    self->chan = chan;
    self->weak = create_weak_channel_core(self);
    self->channel_plugin = create_channel_plugin(chan);
#ifdef BL00MBOX_CALLBACKS_FUNSAFE
    self->callback = mp_const_none;
#endif
    bl00mbox_log_info("created channel %s", chan->name);
    return MP_OBJ_FROM_PTR(self);
}

STATIC mp_obj_t mp_channel_core_get_weak(mp_obj_t self_in) {
    channel_core_obj_t *self = MP_OBJ_TO_PTR(self_in);
    channel_core_verify(self);
    return self->weak;
}
MP_DEFINE_CONST_FUN_OBJ_1(mp_channel_core_get_weak_obj,
                          mp_channel_core_get_weak);

mp_obj_t mp_channel_core_del(mp_obj_t self_in) {
    channel_core_obj_t *self = MP_OBJ_TO_PTR(self_in);
    if (self->chan && (self->weak != self_in)) {
        bl00mbox_log_info("destroyed channel %s", self->chan->name);
        bl00mbox_channel_destroy(self->chan);
    }
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_1(mp_channel_core_del_obj, mp_channel_core_del);

mp_obj_t mp_channel_core_delete(mp_obj_t self_in) {
    channel_core_obj_t *self = MP_OBJ_TO_PTR(self_in);
    channel_core_verify(self);
    bl00mbox_log_info("destroyed channel %s", self->chan->name);
    bl00mbox_channel_destroy(self->chan);
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_1(mp_channel_core_delete_obj, mp_channel_core_delete);

mp_obj_t mp_channel_core_clear(mp_obj_t self_in) {
    channel_core_obj_t *self = MP_OBJ_TO_PTR(self_in);
    channel_core_verify(self);
    self->callback = mp_const_none;
    bl00mbox_channel_clear(self->chan);
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_1(mp_channel_core_clear_obj, mp_channel_core_clear);

STATIC mp_obj_t mp_channel_core_get_connected_mx(mp_obj_t self_in) {
    channel_core_obj_t *self = MP_OBJ_TO_PTR(self_in);
    channel_core_verify(self);
    bl00mbox_array_t *array =
        bl00mbox_channel_collect_connections_mx(self->chan);
    return get_connections_from_array(array);
}
MP_DEFINE_CONST_FUN_OBJ_1(mp_channel_core_get_connected_mx_obj,
                          mp_channel_core_get_connected_mx);

STATIC mp_obj_t mp_channel_core_get_plugins(mp_obj_t self_in) {
    channel_core_obj_t *self = MP_OBJ_TO_PTR(self_in);
    channel_core_verify(self);
    bl00mbox_array_t *array = bl00mbox_channel_collect_plugins(self->chan);
    return get_plugins_from_array(array);
}
MP_DEFINE_CONST_FUN_OBJ_1(mp_channel_core_get_plugins_obj,
                          mp_channel_core_get_plugins);

STATIC void channel_core_attr(mp_obj_t self_in, qstr attr, mp_obj_t *dest) {
    channel_core_obj_t *self = MP_OBJ_TO_PTR(self_in);
    bl00mbox_channel_t *chan = self->chan;

    // if gc_sweep tries to load __del__ we mustn't raise exceptions, setjmp
    // isn't set up
    if (attr != MP_QSTR___del__) channel_core_verify(self);

    if (dest[0] != MP_OBJ_NULL) {
        bool attr_exists = true;
        if (attr == MP_QSTR_volume) {
            int gain = abs(mp_obj_get_int(dest[1]));
            chan->volume = gain > 32767 ? 32767 : gain;
        } else if (attr == MP_QSTR_sys_gain) {
            int gain = abs(mp_obj_get_int(dest[1]));
            chan->sys_gain = gain > 32767 ? 32767 : gain;
        } else if (attr == MP_QSTR_compute_rms) {
            chan->compute_rms = mp_obj_is_true(dest[1]);
            if (!chan->compute_rms) chan->mean_square = 0;
        } else if (attr == MP_QSTR_background_mute_override) {
            bl00mbox_channel_set_background_mute_override(
                chan, mp_obj_is_true(dest[1]));
        } else if (attr == MP_QSTR_foreground) {
            bl00mbox_channel_set_foreground(chan, mp_obj_is_true(dest[1]));
#ifdef BL00MBOX_CALLBACKS_FUNSAFE
        } else if (attr == MP_QSTR_callback) {
            ((channel_core_obj_t *)chan->parent)->callback = dest[1];
#endif
        } else {
            attr_exists = false;
        }
        if (attr_exists) dest[0] = MP_OBJ_NULL;
    } else {
        if (attr == MP_QSTR_volume) {
            dest[0] = mp_obj_new_int(chan->volume);
        } else if (attr == MP_QSTR_sys_gain) {
            dest[0] = mp_obj_new_int(chan->sys_gain);
        } else if (attr == MP_QSTR_mean_square) {
            dest[0] = mp_obj_new_int(chan->mean_square);
        } else if (attr == MP_QSTR_compute_rms) {
            dest[0] = mp_obj_new_bool(chan->compute_rms);
        } else if (attr == MP_QSTR_name) {
            char *ret = strdup_raise(chan->name);
            dest[0] = mp_obj_new_str(ret, strlen(ret));
            free(ret);
        } else if (attr == MP_QSTR_channel_plugin) {
            dest[0] = ((channel_core_obj_t *)chan->parent)->channel_plugin;
        } else if (attr == MP_QSTR_callback) {
#ifdef BL00MBOX_CALLBACKS_FUNSAFE
            dest[0] = ((channel_core_obj_t *)chan->parent)->callback;
#else
            dest[0] = mp_const_none;
#endif
        } else if (attr == MP_QSTR_background_mute_override) {
            dest[0] = mp_obj_new_bool(chan->background_mute_override);
        } else if (attr == MP_QSTR_foreground) {
            dest[0] = mp_obj_new_bool(bl00mbox_channel_get_foreground(chan));
        } else if (attr == MP_QSTR_num_plugins) {
            dest[0] = mp_obj_new_int(bl00mbox_channel_plugins_num(chan));
        } else if (attr == MP_QSTR_num_conns) {
            dest[0] = mp_obj_new_int(bl00mbox_channel_conns_num(chan));
        } else if (attr == MP_QSTR_num_mixer) {
            dest[0] = mp_obj_new_int(bl00mbox_channel_mixer_num(chan));
        } else {
            dest[1] = MP_OBJ_SENTINEL;
        }
    }
}

STATIC const mp_rom_map_elem_t channel_core_locals_dict_table[] = {
    { MP_ROM_QSTR(MP_QSTR___del__), MP_ROM_PTR(&mp_channel_core_del_obj) },
    { MP_ROM_QSTR(MP_QSTR_delete), MP_ROM_PTR(&mp_channel_core_delete_obj) },
    { MP_ROM_QSTR(MP_QSTR_clear), MP_ROM_PTR(&mp_channel_core_clear_obj) },
    { MP_ROM_QSTR(MP_QSTR_get_weak),
      MP_ROM_PTR(&mp_channel_core_get_weak_obj) },
    { MP_ROM_QSTR(MP_QSTR_get_connected_mx),
      MP_ROM_PTR(&mp_channel_core_get_connected_mx_obj) },
    { MP_ROM_QSTR(MP_QSTR_get_plugins),
      MP_ROM_PTR(&mp_channel_core_get_plugins_obj) },
};
STATIC MP_DEFINE_CONST_DICT(channel_core_locals_dict,
                            channel_core_locals_dict_table);

STATIC MP_DEFINE_CONST_OBJ_TYPE(channel_core_type, MP_QSTR_ChannelCore,
                                MP_TYPE_FLAG_NONE, make_new,
                                channel_core_make_new, locals_dict,
                                &channel_core_locals_dict, attr,
                                channel_core_attr);

static mp_obj_t mp_collect_channels(mp_obj_t active_in) {
    // critical phase: make sure to not trigger any garbage collection until
    // these are on the stack!!
    bl00mbox_array_t *chans =
        bl00mbox_collect_channels(mp_obj_is_true(active_in));
    mp_obj_t ret = MP_OBJ_NULL;
    if (chans && chans->len) {
        mp_obj_t elems[chans->len];
        size_t output_index = 0;
        for (size_t input_index = 0; input_index < chans->len; input_index++) {
            bl00mbox_channel_t *chan = chans->elems[input_index];
            channel_core_obj_t *core = chan->parent;
            if (core->base.type != &channel_core_type) {
                bl00mbox_log_error("channel core corrupted");
                continue;
            }
            elems[output_index] = MP_OBJ_FROM_PTR(core);
            output_index++;
        }
        ret = mp_obj_new_list(output_index, elems);
    } else {
        ret = mp_obj_new_list(0, NULL);
    }
    free(chans);
    return ret;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(mp_collect_channels_obj, mp_collect_channels);

// ========================
//         PLUGINS
// ========================

STATIC mp_obj_t plugin_core_make_new(const mp_obj_type_t *type, size_t n_args,
                                     size_t n_kw, const mp_obj_t *args) {
    mp_arg_check_num(n_args, n_kw, 3, 3, false);
    plugin_core_obj_t *self = m_new_obj_with_finaliser(plugin_core_obj_t);
    self->base.type = &plugin_core_type;

    channel_core_obj_t *chan_core = MP_OBJ_TO_PTR(args[0]);
    if (chan_core->base.type != &channel_core_type) {
        mp_raise_TypeError(MP_ERROR_TEXT("first argument must be ChannelCore"));
    }
    channel_core_verify(chan_core);
    bl00mbox_channel_t *chan = chan_core->chan;
    int plugin_kind = mp_obj_get_int(args[1]);
    int init_var = mp_obj_get_int(args[2]);

    bl00mbox_plugin_t *plugin =
        bl00mbox_plugin_create(chan, plugin_kind, init_var);

    if (!plugin) bl00mbox_error_unwrap(BL00MBOX_ERROR_OOM);

    self->plugin = plugin;
    plugin->parent = self;
    plugin->parent_self_ref = &self->plugin;

    // create empty source list
    size_t num_signals = plugin->rugin->len_signals;
    mp_obj_t nones[num_signals];
    for (size_t i = 0; i < num_signals; i++) nones[i] = mp_const_none;
    self->sources = mp_obj_new_list(num_signals, nones);

    bl00mbox_log_info("created plugin %s",
                      self->plugin->rugin->descriptor->name);
    return MP_OBJ_FROM_PTR(self);
}

mp_obj_t mp_plugin_core_del(mp_obj_t self_in) {
    plugin_core_obj_t *self = MP_OBJ_TO_PTR(self_in);
    // do not verify here as it will result in prints from the gc if the channel
    // object was already collected. don't gc the channel plugin, it is deleted
    // with the channel.
    if (self->plugin &&
        (self->plugin->rugin->descriptor->id != BL00MBOX_CHANNEL_PLUGIN_ID)) {
        bl00mbox_log_info("deleted plugin %s",
                          self->plugin->rugin->descriptor->name);
        bl00mbox_plugin_destroy(self->plugin);
    }
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_1(mp_plugin_core_del_obj, mp_plugin_core_del);

mp_obj_t mp_plugin_core_delete(mp_obj_t self_in) {
    plugin_core_obj_t *self = MP_OBJ_TO_PTR(self_in);
    plugin_core_verify(self);
    if (self->plugin->rugin->descriptor->id == BL00MBOX_CHANNEL_PLUGIN_ID) {
        mp_raise_TypeError(
            MP_ERROR_TEXT("cannot manually delete channel plugin"));
    }
    bl00mbox_log_info("deleted plugin");
    bl00mbox_plugin_destroy(self->plugin);
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_1(mp_plugin_core_delete_obj, mp_plugin_core_delete);

STATIC void plugin_core_attr(mp_obj_t self_in, qstr attr, mp_obj_t *dest) {
    plugin_core_obj_t *self = MP_OBJ_TO_PTR(self_in);
    bl00mbox_plugin_t *plugin = self->plugin;

    // if gc_sweep tries to load __del__ we mustn't raise exceptions, setjmp
    // isn't set up
    if (attr != MP_QSTR___del__) plugin_core_verify(self);

    if (dest[0] != MP_OBJ_NULL) {
        if (attr == MP_QSTR_always_render) {
            bl00mbox_plugin_set_always_render(plugin, mp_obj_is_true(dest[1]));
            dest[0] = MP_OBJ_NULL;
        }
    } else {
        if (attr == MP_QSTR_name) {
            // no need to strdup here, descriptors don't die
            char *ret = plugin->rugin->descriptor->name;
            dest[0] = mp_obj_new_str(ret, strlen(ret));
        } else if (attr == MP_QSTR_id) {
            dest[0] = mp_obj_new_int(plugin->id);
        } else if (attr == MP_QSTR_plugin_id) {
            dest[0] = mp_obj_new_int(plugin->rugin->descriptor->id);
        } else if (attr == MP_QSTR_init_var) {
            dest[0] = mp_obj_new_int(plugin->init_var);
        } else if (attr == MP_QSTR_num_signals) {
            dest[0] = mp_obj_new_int(plugin->rugin->len_signals);
        } else if (attr == MP_QSTR_always_render) {
            dest[0] = mp_obj_new_bool(plugin->always_render);
        } else if (attr == MP_QSTR_table_len) {
            dest[0] = mp_obj_new_int(plugin->rugin->plugin_table_len);
        } else if (attr == MP_QSTR_table_pointer) {
            // if there's no table this returns 0, which is fine by us
            int16_t *ret = plugin->rugin->plugin_table;
            dest[0] = mp_obj_new_int_from_uint((uint32_t)ret);
        } else {
            dest[1] = MP_OBJ_SENTINEL;
        }
    }
}

STATIC const mp_rom_map_elem_t plugin_core_locals_dict_table[] = {
    { MP_ROM_QSTR(MP_QSTR___del__), MP_ROM_PTR(&mp_plugin_core_del_obj) },
    { MP_ROM_QSTR(MP_QSTR_delete), MP_ROM_PTR(&mp_plugin_core_delete_obj) },
};
STATIC MP_DEFINE_CONST_DICT(plugin_core_locals_dict,
                            plugin_core_locals_dict_table);

STATIC MP_DEFINE_CONST_OBJ_TYPE(plugin_core_type, MP_QSTR_PluginCore,
                                MP_TYPE_FLAG_NONE, make_new,
                                plugin_core_make_new, locals_dict,
                                &plugin_core_locals_dict, attr,
                                plugin_core_attr);

// ========================
//         SIGNALS
// ========================

STATIC mp_obj_t signal_core_make_new(const mp_obj_type_t *type, size_t n_args,
                                     size_t n_kw, const mp_obj_t *args) {
    mp_arg_check_num(n_args, n_kw, 2, 2, false);

    plugin_core_obj_t *plugin_core = MP_OBJ_TO_PTR(args[0]);
    if (plugin_core->base.type != &plugin_core_type) {
        mp_raise_TypeError(MP_ERROR_TEXT("first argument must be PluginCore"));
    }
    // do this before verification
    signal_core_obj_t *self = m_new_obj(signal_core_obj_t);
    int index = mp_obj_get_int(args[1]);

    plugin_core_verify(plugin_core);

    radspa_t *rugin = plugin_core->plugin->rugin;
    if (index >= rugin->len_signals) {
        mp_raise_msg(&mp_type_IndexError,
                     MP_ERROR_TEXT("signal index out of range"));
    }
    self->base.type = &signal_core_type;
    self->plugin_core = plugin_core;
    self->signal.rignal = &rugin->signals[index];
    self->signal.plugin = plugin_core->plugin;
    self->signal.index = index;
    return MP_OBJ_FROM_PTR(self);
}

STATIC mp_obj_t mp_signal_core_connect(mp_obj_t self_in, mp_obj_t other_in) {
    signal_core_obj_t *self = MP_OBJ_TO_PTR(self_in);
    plugin_core_verify(MP_OBJ_TO_PTR(self->plugin_core));
    signal_core_obj_t *other = MP_OBJ_TO_PTR(other_in);
    plugin_core_verify(MP_OBJ_TO_PTR(other->plugin_core));
    bl00mbox_error_unwrap(
        bl00mbox_signal_connect(&self->signal, &other->signal));
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_2(mp_signal_core_connect_obj, mp_signal_core_connect);

// legacy support
STATIC mp_obj_t mp_signal_core_connect_mx(mp_obj_t self_in) {
    signal_core_obj_t *self = MP_OBJ_TO_PTR(self_in);
    plugin_core_verify(MP_OBJ_TO_PTR(self->plugin_core));
    bl00mbox_error_unwrap(bl00mbox_signal_connect_mx(&self->signal));
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_1(mp_signal_core_connect_mx_obj,
                          mp_signal_core_connect_mx);

STATIC mp_obj_t mp_signal_core_disconnect(mp_obj_t self_in) {
    signal_core_obj_t *self = MP_OBJ_TO_PTR(self_in);
    plugin_core_verify(MP_OBJ_TO_PTR(self->plugin_core));
    bl00mbox_signal_disconnect(&self->signal);
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_1(mp_signal_core_disconnect_obj,
                          mp_signal_core_disconnect);

STATIC mp_obj_t mp_signal_core_get_connected(mp_obj_t self_in) {
    signal_core_obj_t *self = MP_OBJ_TO_PTR(self_in);
    plugin_core_verify(MP_OBJ_TO_PTR(self->plugin_core));
    bl00mbox_array_t *array =
        bl00mbox_signal_collect_connections(&self->signal);
    return get_connections_from_array(array);
}
MP_DEFINE_CONST_FUN_OBJ_1(mp_signal_core_get_connected_obj,
                          mp_signal_core_get_connected);

STATIC void signal_core_attr(mp_obj_t self_in, qstr attr, mp_obj_t *dest) {
    signal_core_obj_t *self = MP_OBJ_TO_PTR(self_in);
    plugin_core_obj_t *plugin_core = MP_OBJ_TO_PTR(self->plugin_core);
    bl00mbox_signal_t *signal = &self->signal;
    radspa_signal_t *rignal = signal->rignal;

    // if gc_sweep tries to load __del__ we mustn't raise exceptions, setjmp
    // isn't set up
    if (attr != MP_QSTR___del__) plugin_core_verify(plugin_core);

    if (dest[0] != MP_OBJ_NULL) {
        if (attr == MP_QSTR_value) {
            if (bl00mbox_signal_is_input(signal)) {
                bl00mbox_error_unwrap(
                    bl00mbox_signal_set_value(signal, mp_obj_get_int(dest[1])));
                dest[0] = MP_OBJ_NULL;
            }
        }
    } else {
        if (attr == MP_QSTR_index) {
            dest[0] = mp_obj_new_int(signal->index);
        } else if (attr == MP_QSTR_plugin_core) {
            dest[0] = plugin_core;
        } else if (attr == MP_QSTR_name) {
            char *ret = strdup_raise(rignal->name);
            dest[0] = mp_obj_new_str(ret, strlen(ret));
            free(ret);
        } else if (attr == MP_QSTR_mpx) {
            dest[0] = mp_obj_new_int(rignal->name_multiplex);
        } else if (attr == MP_QSTR_description) {
            char *ret = strdup_raise(rignal->description);
            dest[0] = mp_obj_new_str(ret, strlen(ret));
            free(ret);
        } else if (attr == MP_QSTR_unit) {
            char *ret = strdup_raise(rignal->unit);
            dest[0] = mp_obj_new_str(ret, strlen(ret));
            free(ret);
        } else if (attr == MP_QSTR_value) {
            dest[0] = mp_obj_new_int(bl00mbox_signal_get_value(signal));
        } else if (attr == MP_QSTR_connected_mx) {
            bool ret = false;
            if (bl00mbox_signal_is_output(signal)) {
                bl00mbox_connection_t *conn =
                    bl00mbox_connection_from_signal(signal);
                if (conn) ret = conn->connected_to_mixer;
            }
            dest[0] = mp_obj_new_bool(ret);
        } else if (attr == MP_QSTR_hints) {
            // this should maybe be uint someday :>
            dest[0] = mp_obj_new_int(rignal->hints);
        } else {
            dest[1] = MP_OBJ_SENTINEL;
        }
    }
}

STATIC const mp_rom_map_elem_t signal_core_locals_dict_table[] = {
    { MP_ROM_QSTR(MP_QSTR_connect), MP_ROM_PTR(&mp_signal_core_connect_obj) },
    { MP_ROM_QSTR(MP_QSTR_connect_mx),
      MP_ROM_PTR(&mp_signal_core_connect_mx_obj) },
    { MP_ROM_QSTR(MP_QSTR_disconnect),
      MP_ROM_PTR(&mp_signal_core_disconnect_obj) },
    { MP_ROM_QSTR(MP_QSTR_get_connected),
      MP_ROM_PTR(&mp_signal_core_get_connected_obj) },
};
STATIC MP_DEFINE_CONST_DICT(signal_core_locals_dict,
                            signal_core_locals_dict_table);

STATIC MP_DEFINE_CONST_OBJ_TYPE(signal_core_type, MP_QSTR_SignalCore,
                                MP_TYPE_FLAG_NONE, make_new,
                                signal_core_make_new, locals_dict,
                                &signal_core_locals_dict, attr,
                                signal_core_attr);

STATIC const mp_rom_map_elem_t bl00mbox_globals_table[] = {
    { MP_OBJ_NEW_QSTR(MP_QSTR___name__),
      MP_OBJ_NEW_QSTR(MP_QSTR_sys_bl00mbox) },

    // PLUGIN REGISTRY
    { MP_ROM_QSTR(MP_QSTR_plugin_registry_num_plugins),
      MP_ROM_PTR(&mp_plugin_registry_num_plugins_obj) },
    { MP_ROM_QSTR(MP_QSTR_plugin_index_get_id),
      MP_ROM_PTR(&mp_plugin_index_get_id_obj) },
    { MP_ROM_QSTR(MP_QSTR_plugin_index_get_name),
      MP_ROM_PTR(&mp_plugin_index_get_name_obj) },
    { MP_ROM_QSTR(MP_QSTR_plugin_index_get_description),
      MP_ROM_PTR(&mp_plugin_index_get_description_obj) },

    // CHANNELS
    { MP_OBJ_NEW_QSTR(MP_QSTR_ChannelCore), (mp_obj_t)&channel_core_type },
    { MP_ROM_QSTR(MP_QSTR_collect_channels),
      MP_ROM_PTR(&mp_collect_channels_obj) },

    // PLUGINS
    { MP_OBJ_NEW_QSTR(MP_QSTR_PluginCore), (mp_obj_t)&plugin_core_type },

    // SIGNALS
    { MP_OBJ_NEW_QSTR(MP_QSTR_SignalCore), (mp_obj_t)&signal_core_type },

    // CONSTANTS
    { MP_ROM_QSTR(MP_QSTR_SIGNAL_HINT_OUTPUT),
      MP_ROM_INT(RADSPA_SIGNAL_HINT_OUTPUT) },
    { MP_ROM_QSTR(MP_QSTR_SIGNAL_HINT_INPUT),
      MP_ROM_INT(RADSPA_SIGNAL_HINT_INPUT) },
    { MP_ROM_QSTR(MP_QSTR_SIGNAL_HINT_SCT),
      MP_ROM_INT(RADSPA_SIGNAL_HINT_SCT) },
    { MP_ROM_QSTR(MP_QSTR_SIGNAL_HINT_GAIN),
      MP_ROM_INT(RADSPA_SIGNAL_HINT_GAIN) },
    { MP_ROM_QSTR(MP_QSTR_SIGNAL_HINT_TRIGGER),
      MP_ROM_INT(RADSPA_SIGNAL_HINT_TRIGGER) },
    { MP_ROM_QSTR(MP_QSTR_SIGNAL_HINT_DEPRECATED),
      MP_ROM_INT(RADSPA_SIGNAL_HINT_DEPRECATED) },
    { MP_ROM_QSTR(MP_QSTR_BL00MBOX_CHANNEL_PLUGIN_ID),
      MP_ROM_INT(BL00MBOX_CHANNEL_PLUGIN_ID) },

    { MP_ROM_QSTR(MP_QSTR_ReferenceError),
      MP_ROM_PTR(&mp_type_ReferenceError) },
};

STATIC MP_DEFINE_CONST_DICT(mp_module_bl00mbox_globals, bl00mbox_globals_table);

const mp_obj_module_t bl00mbox_user_cmodule = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t *)&mp_module_bl00mbox_globals,
};

MP_REGISTER_MODULE(MP_QSTR_sys_bl00mbox, bl00mbox_user_cmodule);
