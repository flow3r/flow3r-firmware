//SPDX-License-Identifier: CC0-1.0
#pragma once
#include "bl00mbox_config.h"
#include "bl00mbox_os.h"
#include "bl00mbox_containers.h"

#include <stdio.h>
#include <math.h>
#include <string.h>
#include "radspa.h"
#include "radspa_helpers.h"

struct _bl00mbox_plugin_t;
struct _bl00mbox_connection_source_t;
struct _bl00mbox_channel_root_t;
struct _bl00mbox_channel_t;

extern int16_t * bl00mbox_line_in_interlaced;

// pointer is unique identifier, no memcpy of this!
typedef struct _bl00mbox_plugin_t{
    radspa_t * rugin; // radspa plugin
    char * name;
    uint32_t id; // unique number in channel to for UI purposes

    uint32_t render_pass_id; // may be used by host to determine whether recomputation is necessary
    uint32_t init_var; // init var that was used for plugin creation
    volatile bool is_being_rendered; // true if rendering the plugin is in progress, else false.
    bool always_render;
    struct _bl00mbox_channel_t * channel; // channel that owns the plugin

    void * parent;
    struct _bl00mbox_plugin_t ** parent_self_ref;
} bl00mbox_plugin_t;

// pointer is NOT unique identifier, memcpy allowed
typedef struct {
    bl00mbox_plugin_t * plugin;
    radspa_signal_t * rignal;
    int index;
} bl00mbox_signal_t;

typedef struct _bl00mbox_connection_t{ //child of bl00mbox_ll_t
    int16_t buffer[BL00MBOX_MAX_BUFFER_LEN]; // MUST stay on top of struct bc type casting! TODO: offsetof()
    bl00mbox_signal_t source;
    bl00mbox_set_t subscribers; // content: bl00mbox_signal_t;
    bool connected_to_mixer; // legacy thing, don't wanna sentinel subsribers
} bl00mbox_connection_t;

// pointer is unique identifier, no memcpy of this!
typedef struct _bl00mbox_channel_t{
    char * name;

    int32_t volume;
    bool background_mute_override;

    // secondary gain that the channel user is supposed to leave untouched so that the OS
    // can change gain too, for example for a system mixer. we still want to apply both in the
    // same pass, so we keep it here.
    int32_t sys_gain;

    // whether to keep track of the rms volume of the channel. adds a bit of cpu load, best
    // keep it off if not used.
    bool compute_rms;
    // we are storing the un-rooted value. this means that if you want the value in decibels
    // you have to only do the log operation and can skip the root as you can express roots
    // as divisions in the log domain which is much cheaper.
    uint32_t mean_square;
    // average output used for DC blocking.
    int32_t dc;

    uint32_t render_pass_id; // may be used by host to determine whether recomputation is necessary

    uint32_t plugin_id; // used to give each plugin in channel a unique identifier

    // we render all of these and add them up. it's a legacy thing from when we had a output mixer
    // but that kinda mixes poorly (heh) with our general API.
    bl00mbox_set_t roots; // content: bl00mbox_connection_t
    bl00mbox_set_t connections; // content: bl00mbox_connection_t
    bl00mbox_set_t always_render; // content: bl00mbox_plugin_t
    bl00mbox_set_t plugins; // content: bl00mbox_plugin_t

    bl00mbox_plugin_t * channel_plugin;

    bl00mbox_lock_t render_lock;
    // take .render_lock before changing these
    bl00mbox_array_t * render_plugins;
    bl00mbox_array_t * render_buffers;

    void * parent;
    struct _bl00mbox_channel_t ** parent_self_ref;
    struct _bl00mbox_channel_t ** weak_parent_self_ref;
} bl00mbox_channel_t;

void bl00mbox_audio_init();

bl00mbox_channel_t * bl00mbox_channel_create();
void bl00mbox_audio_plugin_render(bl00mbox_plugin_t * plugin);
bool bl00mbox_channel_get_foreground(bl00mbox_channel_t * chan);
void bl00mbox_channel_set_foreground(bl00mbox_channel_t * chan, bool enable);
void bl00mbox_channel_set_background_mute_override(bl00mbox_channel_t * chan, bool enable);
void bl00mbox_channel_clear(bl00mbox_channel_t * chan);
void bl00mbox_channel_destroy(bl00mbox_channel_t * chan);
void bl00mbox_channel_event(bl00mbox_channel_t * channel);
bl00mbox_array_t * bl00mbox_collect_channels(bool active);
