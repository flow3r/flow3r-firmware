//SPDX-License-Identifier: CC0-1.0
#pragma once

#include <stdio.h>
#include <math.h>
#include <string.h>

#include "bl00mbox_plugin_registry.h"
#include "bl00mbox_audio.h"
#include "bl00mbox_os.h"
#include "bl00mbox_containers.h"

#include <stdint.h>
#include "bl00mbox_audio.h"
#include "radspa_helpers.h"

// lazy 2nd error channel: pointer return types resulting in NULL means OOM

uint16_t bl00mbox_channel_plugins_num(bl00mbox_channel_t * chan);
uint16_t bl00mbox_channel_conns_num(bl00mbox_channel_t * chan);
uint16_t bl00mbox_channel_mixer_num(bl00mbox_channel_t * chan);
bl00mbox_array_t * bl00mbox_channel_collect_plugins(bl00mbox_channel_t * chan);
bl00mbox_array_t * bl00mbox_channel_collect_connections_mx(bl00mbox_channel_t * chan);

bl00mbox_plugin_t * bl00mbox_plugin_create_unlisted(bl00mbox_channel_t * chan, radspa_descriptor_t * desc, uint32_t init_var);
bl00mbox_plugin_t * bl00mbox_plugin_create(bl00mbox_channel_t * chan, uint32_t id, uint32_t init_var);
void bl00mbox_plugin_destroy(bl00mbox_plugin_t * plugin);
void bl00mbox_plugin_set_always_render(bl00mbox_plugin_t * plugin, bool value);

bl00mbox_error_t bl00mbox_signal_set_value(bl00mbox_signal_t * signal, int value);
int16_t bl00mbox_signal_get_value(bl00mbox_signal_t * signal);
bl00mbox_error_t bl00mbox_signal_connect(bl00mbox_signal_t * some, bl00mbox_signal_t * other);
bl00mbox_error_t bl00mbox_signal_connect_mx(bl00mbox_signal_t * some);
void bl00mbox_signal_disconnect(bl00mbox_signal_t * signal);
bl00mbox_array_t * bl00mbox_signal_collect_connections(bl00mbox_signal_t * signal);

bl00mbox_connection_t * bl00mbox_connection_from_signal(bl00mbox_signal_t * signal);

bool bl00mbox_signal_is_input(bl00mbox_signal_t * signal);
bool bl00mbox_signal_is_output(bl00mbox_signal_t * signal);
