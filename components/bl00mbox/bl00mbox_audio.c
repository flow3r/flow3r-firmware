//SPDX-License-Identifier: CC0-1.0
#include "bl00mbox_audio.h"
#include "bl00mbox_user.h"
#include "bl00mbox_os.h"
#include "bl00mbox_channel_plugin.h"
#include <assert.h>

static uint16_t full_buffer_len;

static uint32_t render_pass_id;

int16_t * bl00mbox_line_in_interlaced = NULL;

static bl00mbox_lock_t user_lock = NULL;
// grab user_lock for r/w
static bl00mbox_set_t * all_chans = NULL;
// grab user_lock for r/w
static bl00mbox_set_t * background_mute_override_chans = NULL;
// grab user_lock for r/w
static bl00mbox_channel_t * foreground_chan = NULL;

// grab both user_lock and active_chans_lock for writing.
// for user-side reading grab user_lock.
// render task reads with active_chans_lock.
static bl00mbox_array_t * active_chans = NULL;
static bl00mbox_lock_t active_chans_lock = NULL;

static void update_active_chans(){
    // must be called after changing foreground_chan or background_mute_override_chans
    // while _still_ holding user_lock but not active_chans_lock
    size_t num_chans = background_mute_override_chans->len;
    if(foreground_chan) num_chans++;
    bl00mbox_array_t * new_active_chans = malloc(sizeof(bl00mbox_array_t) + num_chans * sizeof(void *));
    if(new_active_chans){
        size_t index = 0;
        if(foreground_chan){
            new_active_chans->elems[index] = foreground_chan;
            index++;
        }
        bl00mbox_set_iter_t iter;
        bl00mbox_set_iter_start(&iter, background_mute_override_chans);
        bl00mbox_channel_t * chan;
        while((chan = bl00mbox_set_iter_next(&iter))){
            if(chan == foreground_chan) continue;
            new_active_chans->elems[index] = chan; 
            index++;
        }
        new_active_chans->len = index;
    } else {
        bl00mbox_log_error("out of memory");
    }

#ifdef BL00MBOX_DEBUG
    if(new_active_chans) bl00mbox_log_error("active chans: %d", (int) new_active_chans->len);
#endif
    bl00mbox_array_t * previous_active_chans;
    bl00mbox_take_lock(&active_chans_lock);
    previous_active_chans = active_chans;
    active_chans = new_active_chans;
    bl00mbox_give_lock(&active_chans_lock);
    free(previous_active_chans);
}

bl00mbox_array_t * bl00mbox_collect_channels(bool active){
    bl00mbox_array_t * ret = NULL;
    bl00mbox_take_lock(&user_lock);
    if(active){
        if(active_chans){
            size_t ret_size = sizeof(bl00mbox_array_t);
            ret_size += active_chans->len * sizeof(void *);
            ret = malloc(ret_size);
            if(ret) memcpy(ret, active_chans, ret_size);
        } else {
            ret = malloc(sizeof(bl00mbox_array_t));
            if(ret) ret->len = 0;
        }
    } else {
        ret = bl00mbox_set_to_array(all_chans);
    }
    bl00mbox_give_lock(&user_lock);
    if(!ret) bl00mbox_log_error("out of memory");
    return ret;
}

void bl00mbox_channel_set_background_mute_override(bl00mbox_channel_t * chan, bool enable){
    bl00mbox_take_lock(&user_lock);
    chan->background_mute_override = enable;
    bool update;
    if(enable){
        update = bl00mbox_set_add(background_mute_override_chans, chan);
    } else {
        update = bl00mbox_set_remove(background_mute_override_chans, chan);
    }
    if(update) update_active_chans();
    bl00mbox_give_lock(&user_lock);
}

bool bl00mbox_channel_get_foreground(bl00mbox_channel_t * chan){
    return foreground_chan == chan;
}

void bl00mbox_channel_set_foreground(bl00mbox_channel_t * chan, bool enable){
    if(bl00mbox_channel_get_foreground(chan) == enable) return;
    bl00mbox_take_lock(&user_lock);
    foreground_chan = enable ? chan : NULL;
    update_active_chans();
    bl00mbox_give_lock(&user_lock);
}

void bl00mbox_channel_event(bl00mbox_channel_t * chan){
#ifdef BL00MBOX_AUTO_FOREGROUNDING
    bl00mbox_channel_set_foreground(chan, true);
#endif
}

bl00mbox_channel_t * bl00mbox_channel_create(){
    bl00mbox_channel_t * chan = calloc(1, sizeof(bl00mbox_channel_t));
    if(!chan) goto failed;
    chan->volume = BL00MBOX_DEFAULT_CHANNEL_VOLUME;
    chan->sys_gain = 4096;

    // must be destroyed manually as it's not in the plugin list
    chan->channel_plugin = bl00mbox_plugin_create_unlisted(chan, &bl00mbox_channel_plugin_desc, 0);
    if(!chan->channel_plugin) goto failed;

    if(!bl00mbox_create_lock(&chan->render_lock)) goto failed;

    bl00mbox_take_lock(&user_lock);
    if(!bl00mbox_set_add_skip_unique_check(all_chans, chan)) goto failed;
    foreground_chan = chan;
    update_active_chans();
    bl00mbox_give_lock(&user_lock);
    return chan;

failed:
    if(chan){
        if(chan->channel_plugin){
            // supress errors
            chan->channel_plugin->parent_self_ref = &chan->channel_plugin;
            bl00mbox_plugin_destroy(chan->channel_plugin);
        }
        if(chan->render_lock) bl00mbox_delete_lock(&chan->render_lock);
        free(chan);
    }
    bl00mbox_log_error("channel allocation failed");
    return NULL;
}

void bl00mbox_channel_clear(bl00mbox_channel_t * chan){
    // note: this does NOT destroy the channel_plugin as it is unlisted
    bl00mbox_set_iter_t iter;
    bl00mbox_set_iter_start(&iter, &chan->plugins);
    bl00mbox_plugin_t * plugin;
    while((plugin = bl00mbox_set_iter_next(&iter))){
        bl00mbox_plugin_destroy(plugin);
    }
    chan->plugin_id = 0;
}

void bl00mbox_channel_destroy(bl00mbox_channel_t * chan){
    bl00mbox_channel_clear(chan);

    // remove from all lists
    bl00mbox_take_lock(&user_lock);
    bool is_active = false;
    if(foreground_chan == chan){
        foreground_chan = NULL;
        is_active = true;
    }
    if(bl00mbox_set_remove(background_mute_override_chans, chan)){
        is_active = true;
    }
    if(is_active) update_active_chans();
    bl00mbox_set_remove(all_chans, chan);
    bl00mbox_give_lock(&user_lock);

    // remove from parent
    if(* (chan->parent_self_ref) != chan){
        bl00mbox_log_error("channel: parent_self_ref improper: channel %p, ref %p",
                            chan, * (chan->parent_self_ref));
    }
    * (chan->parent_self_ref) = NULL;

    // remove from weak parent
    if(chan->weak_parent_self_ref){
        if(* (chan->weak_parent_self_ref) != chan){
            bl00mbox_log_error("channel: weak_parent_self_ref improper: channel %p, ref %p",
                                chan, * (chan->weak_parent_self_ref));
        }
        * (chan->weak_parent_self_ref) = NULL;
    }

#ifdef BL00MBOX_DEBUG
    if(chan->connections.len) bl00mbox_log_error("connections nonempty");
    if(chan->roots.len) bl00mbox_log_error("roots nonempty");
    if(chan->always_render.len) bl00mbox_log_error("always render nonempty");
    if(chan->plugins.len) bl00mbox_log_error("plugins nonempty");
    bl00mbox_wait();
#endif

    // be really sure that nobody else holds the lock. the renderer
    // doesn't at this point, but if there's multiple tasks running
    // clients there may be collisions.
    // since the client api is generally not thread safe at this point
    // it's okay, we can add the feature easily by just wrapping _all_
    // client api in a lock at some point in the future.
    bl00mbox_delete_lock(&chan->render_lock);

    if(chan->channel_plugin) bl00mbox_plugin_destroy(chan->channel_plugin);
    free(chan->render_plugins);
    free(chan->render_buffers);
    free(chan->name);
    free(chan);
}

void bl00mbox_audio_plugin_render(bl00mbox_plugin_t * plugin){
    if(plugin->render_pass_id == render_pass_id) return;
#ifdef BL00MBOX_LOOPS_ENABLE
    if(plugin->is_being_rendered) return;
#endif
    plugin->is_being_rendered = true;
    plugin->rugin->render(plugin->rugin, full_buffer_len, render_pass_id);
    plugin->render_pass_id = render_pass_id;
    plugin->is_being_rendered = false;
}

static bool _bl00mbox_audio_channel_render(bl00mbox_channel_t * chan, int16_t * out, bool adding){
    chan->render_pass_id = render_pass_id;

    int32_t vol = radspa_mult_shift(chan->volume, chan->sys_gain);
    if(!vol) return false; // don't render if muted

    // render these even if nothing is plugged into mixer
    if(chan->render_plugins){
        for(size_t i = 0; i < chan->render_plugins->len; i++){
            bl00mbox_audio_plugin_render(chan->render_plugins->elems[i]);
        }
    }

    bl00mbox_channel_plugin_update_values(chan->channel_plugin->rugin, render_pass_id);

    if(!(chan->render_buffers && chan->render_buffers->len)) return false;


    int32_t acc[full_buffer_len];

    // first one non-adding
    int16_t * buffer = chan->render_buffers->elems[0];
    if(buffer[1] == -32768){
        for(size_t i = 0; i < full_buffer_len; i++){
            acc[i] = buffer[0];
        }
    } else {
        for(size_t i = 0; i < full_buffer_len; i++){
            acc[i] = buffer[i];
        }
    }

    // rest adding
    for(size_t i = 1; i < chan->render_buffers->len; i++){
        buffer = chan->render_buffers->elems[i];
        if(buffer[1] == -32768){
            if(buffer[0]){
                for(size_t i = 0; i < full_buffer_len; i++){
                    acc[i] += buffer[0];
                }
            }
        } else {
            for(size_t i = 0; i < full_buffer_len; i++){
                acc[i] += buffer[i];
            }
        }
    }

    for(uint16_t i = 0; i < full_buffer_len; i++){
        // flip around for rounding towards zero/mulsh boost
        int invert = chan->dc < 0 ? -1 : 1;
        chan->dc = chan->dc * invert;
        chan->dc = ((uint64_t) chan->dc * (((1<<12) - 1)<<20)) >> 32;
        chan->dc = chan->dc * invert;

        chan->dc += acc[i];

        acc[i] -= (chan->dc >> 12);
    }
    if(adding){
        for(uint16_t i = 0; i < full_buffer_len; i++){
            out[i] = radspa_add_sat(radspa_gain(acc[i], vol), out[i]);
        }
    } else {
        for(uint16_t i = 0; i < full_buffer_len; i++){
            out[i] = radspa_gain(acc[i], vol);
        }
    }
    if(chan->compute_rms){
        for(uint16_t i = 0; i < full_buffer_len; i++){
            int32_t sq = acc[i];
            sq = (sq * sq) - chan->mean_square;
            // always round down with negative sq so that decay always works.
            // bitshift instead of div does that for us nicely.
            // cannot underflow as ((-a) >> 11) can never be less than -a.
            chan->mean_square += sq >> 11;
        }
    }
    return true;
}

static bool bl00mbox_audio_channel_render(bl00mbox_channel_t * chan, int16_t * out, bool adding){
    if(render_pass_id == chan->render_pass_id) return false;
    bl00mbox_take_lock(&chan->render_lock);
    bool ret = _bl00mbox_audio_channel_render(chan, out, adding);
    bl00mbox_give_lock(&chan->render_lock);
    // null it out if nothing was rendered
    chan->mean_square *= ret;
    return ret;
}

void bl00mbox_audio_render(int16_t * rx, int16_t * tx, uint16_t len){
    full_buffer_len = len/2;
    bl00mbox_line_in_interlaced = rx;
    int16_t acc[full_buffer_len];
    bool acc_init = false;

    bl00mbox_take_lock(&active_chans_lock);
    render_pass_id++;
    if(active_chans){
        for(size_t i = 0; i < active_chans->len; i++){
            acc_init = bl00mbox_audio_channel_render(active_chans->elems[i], acc, acc_init) || acc_init;
        }
    }
    bl00mbox_give_lock(&active_chans_lock);

    if(acc_init){
        for(uint16_t i = 0; i < full_buffer_len; i++){
            tx[2*i] = acc[i];
            tx[2*i+1] = acc[i];
        }
    } else {
        memset(tx, 0, len * sizeof(int16_t));
    }
}

void bl00mbox_audio_init(){
    assert(bl00mbox_create_lock(&active_chans_lock));

    // micropython objects are generally not thread safe, so for most of the user API we need
    // not care about locking after they've been created. however, bl00mbox Channel objects may
    // be created by different threads (i.e., thread API), so these sets must be fully thread safe.
    assert(bl00mbox_create_lock(&user_lock));

    all_chans = calloc(1, sizeof(bl00mbox_set_t));
    assert(all_chans);

    background_mute_override_chans = calloc(1, sizeof(bl00mbox_set_t));
    assert(background_mute_override_chans);
}
