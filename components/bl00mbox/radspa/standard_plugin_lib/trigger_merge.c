#include "trigger_merge.h"
#include <stdio.h>

radspa_descriptor_t trigger_merge_desc = {
    .name = "trigger_merge",
    .id = 38,
    .description = "merges multiple trigger inputs together. lazy. if a start and a stop "
                    "collide the stop is preferred."
                    "\ninit_var: number of inputs, default 0, max 127",
    .create_plugin_instance = trigger_merge_create,
    .destroy_plugin_instance = radspa_standard_plugin_destroy
};

typedef struct {
    int16_t trigger_out_prev;
    int16_t blocked_stop;
    int16_t trigger_in_prev[];
} trigger_merge_data_t;

void trigger_merge_run(radspa_t * plugin, uint16_t num_samples, uint32_t render_pass_id){
    int num_inputs = plugin->len_signals - 1;
    trigger_merge_data_t * data = plugin->plugin_data;
    bool block_stop_events = plugin->plugin_table[0];

    int16_t i;
    int16_t merged_trigger = 0; 
    int16_t last_timestamp = -1;
    for(uint8_t j = 0; j < num_inputs; j++){
        int16_t trigger_in = radspa_trigger_get_const(&plugin->signals[j], &data->trigger_in_prev[j], (uint16_t *) &i, num_samples, render_pass_id);
        if((last_timestamp > i) || (!trigger_in)) continue;
        if((trigger_in > 0) && (last_timestamp == i)){
            if(merged_trigger != -1){
                merged_trigger = merged_trigger > trigger_in ? merged_trigger : trigger_in;
            }
        } else {
            merged_trigger = trigger_in;
        }
        last_timestamp = i;
    }
    if(merged_trigger > 0){
        radspa_trigger_start(merged_trigger, &(data->trigger_out_prev));
        data->blocked_stop = false;
    } else if(data->blocked_stop && (!block_stop_events)){
        radspa_trigger_stop(&(data->trigger_out_prev));
        data->blocked_stop = false;
    } else if(merged_trigger < 0){
        if(!block_stop_events){
            radspa_trigger_stop(&(data->trigger_out_prev));
        } else {
            data->blocked_stop = true;
        }
    }
    radspa_signal_set_const_value(&plugin->signals[num_inputs], data->trigger_out_prev);
}

radspa_t * trigger_merge_create(uint32_t init_var){
    if(init_var > 127) init_var = 127;
    if(!init_var) init_var = 1;
    uint32_t size = sizeof(trigger_merge_data_t) + init_var * sizeof(int16_t);
    radspa_t * plugin = radspa_standard_plugin_create(&trigger_merge_desc, init_var + 1, size, 1);
    if(plugin == NULL) return NULL;
    plugin->render = trigger_merge_run;

    radspa_signal_set_group(plugin, init_var, 1, 0, "input", RADSPA_SIGNAL_HINT_INPUT | RADSPA_SIGNAL_HINT_TRIGGER, 0);
    radspa_signal_set(plugin, init_var, "output", RADSPA_SIGNAL_HINT_OUTPUT | RADSPA_SIGNAL_HINT_TRIGGER, 0);
    return plugin;
}
