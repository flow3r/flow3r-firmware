#pragma once
#include "radspa.h"
#include "radspa_helpers.h"
// SPECIAL REQUIREMENTS
#include "bl00mbox_audio.h"

#define BL00MBOX_CHANNEL_PLUGIN_ID 4002

extern radspa_descriptor_t bl00mbox_channel_plugin_desc;
radspa_t * bl00mbox_channel_plugin_create(uint32_t init_var);
void bl00mbox_channel_plugin_run(radspa_t * channel_plugin, uint16_t num_samples, uint32_t render_pass_id);

void bl00mbox_channel_plugin_update_values(radspa_t * channel_plugin, uint32_t render_pass_id);
