#pragma once

// Highest-level driver for captouch on the flow3r badge. Uses 2x AD7147.

// The flow3r has 10 touch petals. 5 petals on the top layer, 5 petals on the
// bottom layer.
//
// Top petals have three capacitive pads. Bottom petals have two capacitive
// pads.
//
// The petals are numbered from 0 to 9 (inclusive). Petal 0 is next to the USB
// port, and is a top petal. Petal 1 is a bottom petal to its right. Petal 2 is
// a top petal to its right, and the rest continue clockwise accordingly.

#include <stdbool.h>
#include <stdint.h>

#define FLOW3R_BSP_CAPTOUCH_RAD_UNIT (16384.)
#define FLOW3R_BSP_CAPTOUCH_PHI_UNIT (16384.)
#define FLOW3R_BSP_CAPTOUCH_COVERAGE_UNIT (1024.)

// allows to compare two different positional algorithms.
// unlocks the "._pos2" attribute on regular captouch output (but not on log
// frames) #define FLOW3R_BSP_CAPTOUCH_SECONDARY_OUTPUT

typedef struct {
    int16_t rad;
    int16_t phi;
#ifdef FLOW3R_BSP_CAPTOUCH_SECONDARY_OUTPUT
    int16_t rad2;
    int16_t phi2;
#endif
    uint16_t coverage;
    uint8_t index : 4;
    uint8_t mode : 2;
    uint8_t press_event : 1;
    uint8_t pressed : 1;
} flow3r_bsp_captouch_petal_data_t;

typedef struct {
    flow3r_bsp_captouch_petal_data_t petals[10];
} flow3r_bsp_captouch_data_t;

void flow3r_bsp_captouch_get(flow3r_bsp_captouch_data_t *dest);
void flow3r_bsp_captouch_refresh_events();

// Initialize captouch subsystem.
void flow3r_bsp_captouch_init();

// Request captouch calibration.
void flow3r_bsp_captouch_calibration_request();

// Returns true if captouch is currently calibrating.
int flow3r_bsp_captouch_calibrating();

// Set/get calibration data. data[] should be at least 120 entries long.
bool flow3r_bsp_captouch_get_calibration_data(int32_t *data);
void flow3r_bsp_captouch_set_calibration_data(int32_t *data);

// experiments
float flow3r_bsp_captouch_get_rad(flow3r_bsp_captouch_petal_data_t *petal,
                                  uint8_t smooth, uint8_t drop_first,
                                  uint8_t drop_last);
float flow3r_bsp_captouch_get_phi(flow3r_bsp_captouch_petal_data_t *petal,
                                  uint8_t smooth, uint8_t drop_first,
                                  uint8_t drop_last);

void flow3r_bsp_captouch_get_rad_raw(flow3r_bsp_captouch_petal_data_t *petal,
                                     float *ret);
void flow3r_bsp_captouch_get_phi_raw(flow3r_bsp_captouch_petal_data_t *petal,
                                     float *ret);

// uint8_t petal, bool pressed, uint32_t time_us, int32_t rad, int32_t phi,
// int32_t coverage
typedef void (*flow3r_bsp_data_callback_t)(uint8_t, uint8_t, bool, uint32_t,
                                           int32_t, int32_t, int32_t);
void flow3r_bsp_captouch_set_data_callback(flow3r_bsp_data_callback_t fun);

typedef enum {
    PETAL_MODE_OFF = 0,
    PETAL_MODE_0D = 1,
    PETAL_MODE_1D = 2,
    PETAL_MODE_2D = 3,
} flow3r_bsp_captouch_petal_mode_t;

void flow3r_bsp_captouch_set_petal_modes(
    flow3r_bsp_captouch_petal_mode_t *data);
void flow3r_bsp_captouch_get_petal_modes(
    flow3r_bsp_captouch_petal_mode_t *data);
