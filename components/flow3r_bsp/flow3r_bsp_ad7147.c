#include "flow3r_bsp_ad7147.h"
#include "flow3r_bsp_ad7147_hw.h"
#include "flow3r_bsp_captouch.h"
#include "flow3r_bsp_i2c.h"

#include "esp_err.h"
#include "esp_log.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "sdkconfig.h"

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "freertos/task.h"

#include "driver/gpio.h"
#include "esp_timer.h"

// #define FLOW3R_BSP_CAPTOUCH_DEBUG_PROFILING
// #define FLOW3R_BSP_CAPTOUCH_DEBUG_CURSED_PROFILING
// #define FLOW3R_BSP_CAPTOUCH_DEBUG_SEQUENCING
// #define FLOW3R_BSP_CAPTOUCH_DEBUG_CALIBRATION
// #define FLOW3R_BSP_CAPTOUCH_DEBUG_PETAL 2

/*
時折誰かが問う
いつまでどこまで向かう気かと
BABYどこまででも
*/

#define MAX_POS_AFE (63)
#define MAX_NEG_AFE (63)
#define MAX_AFE (MAX_POS_AFE + MAX_NEG_AFE)

static ad7147_chip_t _top = {
    .name = "top",
    .is_bot = false,
    .num_petals = 4,
    .petals = { 0, 4, 6, 8 },
};

static ad7147_chip_t _bot = {
    .name = "bot",
    .is_bot = true,
    .num_petals = 6,
    .petals = { 1, 2, 3, 5, 7, 9 },
};

// callback function for the logging feature
static flow3r_bsp_data_callback_t _on_data = NULL;
void flow3r_bsp_ad7147_set_data_callback(flow3r_bsp_data_callback_t fun) {
    _on_data = fun;
}

// modes that the petals are supposed to be in (excl. calibration and
// transients)
static flow3r_bsp_captouch_petal_mode_t _petal_modes[10] = {
    PETAL_MODE_2D, PETAL_MODE_1D, PETAL_MODE_2D, PETAL_MODE_1D, PETAL_MODE_2D,
    PETAL_MODE_1D, PETAL_MODE_2D, PETAL_MODE_1D, PETAL_MODE_2D, PETAL_MODE_1D,
};

static const char *TAG = "flow3r-bsp-ad7147";

// output data that gets continuously written to and
// memcpy'd when the user requests.
static flow3r_bsp_captouch_data_t captouch_data;

// user-facing calibration data
static ad7147_petal_calib_t calibration_data[10];
// lock for calibration_data
static SemaphoreHandle_t calibration_lock = NULL;
// calibration data used internally by the chip tasks
static ad7147_petal_calib_t _chip_calibration_data[10];

// if you want both locks it's important to always take/free them in the
// same order to avoid deadlock. use these functions to avoid such bugs.
// xTicksToWait is implemented very halfassed and is the wait time for each
// lock, i.e. you might wait twice as long
static BaseType_t xSemaphoreTakeCaptouchOutput(TickType_t xTicksToWait) {
    if (xSemaphoreTake(_top.output_lock, xTicksToWait) == pdFALSE)
        return pdFALSE;
    if (xSemaphoreTake(_bot.output_lock, xTicksToWait) == pdTRUE) return pdTRUE;
    xSemaphoreGive(_top.output_lock);
    return pdFALSE;
}
static BaseType_t xSemaphoreGiveCaptouchOutput() {
    bool ret = true;
    ret = (xSemaphoreGive(_bot.output_lock) == pdTRUE) && ret;
    ret = (xSemaphoreGive(_top.output_lock) == pdTRUE) && ret;
    return ret ? pdTRUE : pdFALSE;
}

// we just want a function with that name really
static BaseType_t xSemaphoreBonk(SemaphoreHandle_t handle,
                                 TickType_t xTicksToWait) {
    if (xSemaphoreTake(handle, xTicksToWait) == pdFALSE) return pdFALSE;
    return xSemaphoreGive(handle);
}

// tasks that generate captouch_data
static TaskHandle_t _top_task_handle = NULL;
static TaskHandle_t _bot_task_handle = NULL;

// notifications for _*_task_handle
// chip gpio interrupt
#define NOTIF_GPIO 1
// send this if you have written new target modes to ad7147_chip_t.user_modes to
// apply them.
#define NOTIF_MODE_CHANGE 2
// send this to start a calibration.
#define NOTIF_CALIB_START 4
// send this to stop a calibration. note: this does not restore previous
// calibration data. use this only for restarting calibration or applying
// external calibration, else you might end up with poor performance.
#define NOTIF_CALIB_STOP 8
// send this to if you have written new data to calibration_data to apply it.
#define NOTIF_CALIB_CHANGE 16
// used internally to synchronize calibration stages between chips for
// consistent EMI, don't send externally.
#define NOTIF_INTERNAL_CALIB_NEXT_STAGE 32
// used internally to iterate over calibrations.
#define NOTIF_INTERNAL_CALIB_CHANGE 64

#define NOTIF_CLEAR_MASK 127

// container for unprocessed petal data.
// 10 petals, 4 potential pad positions according to
// petal_kind_t. some fields are never used.
static uint16_t raw_petals[10][4];

typedef struct {
    size_t petal_number;
    petal_kind_t pad_kind;
} pad_mapping_t;

typedef struct {
    bool press_event_new;
    bool fresh;
} press_latch_t;

static press_latch_t latches[10];

static void petal_process(int index, uint32_t timestamp, int mode,
                          SemaphoreHandle_t lock);

static void petals_process(ad7147_chip_t *chip, uint32_t timestamp) {
    for (int i = 0; i < chip->num_petals; i++) {
        int petal = chip->petals[i];
        petal_process(petal, timestamp, chip->modes[petal], chip->output_lock);
    }
}

static void petals_clear(ad7147_chip_t *chip, uint32_t timestamp) {
    for (int i = 0; i < chip->num_petals; i++) {
        int petal = chip->petals[i];
        petal_process(petal, timestamp, 0, chip->output_lock);
    }
}

// DATASHEET VIOLATION 1
// target value that we ideally wanna see from an idle petal.
// this chip was designed for some sort  of (plastic) case
// between electrode and finger so our signal is coming in
// super hot and we need the extended headroom. datasheet
// suggests to set this to halfpoint (32k).
static const int32_t _calib_target = 6000;
// this is what we assume one step in the chip's AFE
// parameter does to the output reading. this value is used
// in an iterative process so it doesn't have to be super
// precise but it helps to be in the ballpark. we thiink
// the real value is more like 970 but not sure how linear
// this is. anyways, calibration works reasonably well with
// this, let's never touch it again :D
static const int32_t _calib_incr_cap = 1000;

#if defined(CONFIG_FLOW3R_HW_GEN_P3)
static const pad_mapping_t _map_top[12] = {
    { 0, petal_pad_tip },  // 0
    { 0, petal_pad_ccw },  // 1
    { 0, petal_pad_cw },   // 2
    { 8, petal_pad_cw },   // 3
    { 8, petal_pad_ccw },  // 4
    { 8, petal_pad_tip },  // 5
    { 4, petal_pad_tip },  // 6
    { 4, petal_pad_ccw },  // 7
    { 4, petal_pad_cw },   // 8
    { 6, petal_pad_cw },   // 9
    { 6, petal_pad_ccw },  // 10
    { 6, petal_pad_tip },  // 11
};

static const pad_mapping_t _map_bot[13] = {
    { 9, petal_pad_base },  // 0
    { 9, petal_pad_tip },   // 1

    { 7, petal_pad_base },  // 2
    { 7, petal_pad_tip },   // 3

    { 5, petal_pad_base },  // 4
    { 5, petal_pad_tip },   // 5

    { 3, petal_pad_tip },   // 6
    { 3, petal_pad_base },  // 7

    { 1, petal_pad_tip },   // 8
    { 1, petal_pad_base },  // 9

    { 2, petal_pad_tip },  // 10
    { 2, petal_pad_cw },   // 11
    { 2, petal_pad_ccw },  // 12
};

static gpio_num_t _interrupt_gpio_top = GPIO_NUM_15;
static gpio_num_t _interrupt_gpio_bot = GPIO_NUM_15;
static bool _interrupt_shared = true;
#elif defined(CONFIG_FLOW3R_HW_GEN_P4) || defined(CONFIG_FLOW3R_HW_GEN_C23)
static const pad_mapping_t _map_top[12] = {
    { 0, petal_pad_ccw },   // 0
    { 0, petal_pad_base },  // 1
    { 0, petal_pad_cw },    // 2
    { 8, petal_pad_cw },    // 3
    { 8, petal_pad_base },  // 4
    { 8, petal_pad_ccw },   // 5
    { 4, petal_pad_ccw },   // 6
    { 4, petal_pad_base },  // 7
    { 4, petal_pad_cw },    // 8
    { 6, petal_pad_ccw },   // 9
    { 6, petal_pad_base },  // 10
    { 6, petal_pad_cw },    // 11
};
static const pad_mapping_t _map_bot[13] = {
    { 9, petal_pad_base },  // 0
    { 9, petal_pad_tip },   // 1

    { 7, petal_pad_base },  // 2
    { 7, petal_pad_tip },   // 3

    { 5, petal_pad_base },  // 4
    { 5, petal_pad_tip },   // 5

    { 3, petal_pad_tip },   // 6
    { 3, petal_pad_base },  // 7

    { 1, petal_pad_tip },   // 8
    { 1, petal_pad_base },  // 9

    { 2, petal_pad_ccw },   // 10
    { 2, petal_pad_cw },    // 11
    { 2, petal_pad_base },  // 12
};
#if defined(CONFIG_FLOW3R_HW_GEN_P4)
static gpio_num_t _interrupt_gpio_top = GPIO_NUM_15;
static gpio_num_t _interrupt_gpio_bot = GPIO_NUM_15;
static bool _interrupt_shared = true;
#else
static gpio_num_t _interrupt_gpio_top = GPIO_NUM_15;
static gpio_num_t _interrupt_gpio_bot = GPIO_NUM_16;
static bool _interrupt_shared = false;
#endif
#else
#error "captouch not implemented for this badge generation"
#endif

static uint8_t get_petal_pad_ch(uint8_t petal, petal_kind_t kind) {
    // returns the channel but not the chip. defaults to channel 0.
    // pretty bad function but does the trick for now.
    static uint8_t cache[40] = {
        0,
    };
    int index = kind + 4 * petal;
    if (cache[index]) return cache[index] - 1;
    int len;
    len = sizeof(_map_bot) / sizeof(pad_mapping_t);
    for (int i = 0; i < len; i++) {
        if (_map_bot[i].petal_number == petal && _map_bot[i].pad_kind == kind) {
            cache[index] = i + 1;
            return i;
        }
    }
    len = sizeof(_map_top) / sizeof(pad_mapping_t);
    for (int i = 0; i < len; i++) {
        if (_map_top[i].petal_number == petal && _map_top[i].pad_kind == kind) {
            cache[index] = i + 1;
            return i;
        }
    }
    return 0;  // should never be reached
}

void _write_petal_mode_seq(uint16_t **write_head, uint8_t petal,
                           flow3r_bsp_captouch_petal_mode_t mode) {
    if (petal & 1) {
        switch (mode) {
            case PETAL_MODE_OFF:  // c is a clown language
                break;
            case PETAL_MODE_0D:
                **write_head = 1 << get_petal_pad_ch(petal, petal_pad_base);
                **write_head |= 1 << get_petal_pad_ch(petal, petal_pad_tip);
                *write_head += 1;
                break;
            case PETAL_MODE_1D:
            case PETAL_MODE_2D:
                **write_head = 1 << get_petal_pad_ch(petal, petal_pad_base);
                *write_head += 1;  // the * is its little clown nose
                **write_head = 1 << get_petal_pad_ch(petal, petal_pad_tip);
                *write_head += 1;
                break;
        }
    } else {
        switch (mode) {
            case PETAL_MODE_OFF:
                break;
            case PETAL_MODE_0D:
                **write_head = 1 << get_petal_pad_ch(petal, petal_pad_base);
                **write_head |= 1 << get_petal_pad_ch(petal, petal_pad_cw);
                **write_head |= 1 << get_petal_pad_ch(petal, petal_pad_ccw);
                *write_head += 1;
                break;  // it's good for when you want to
            case PETAL_MODE_1D:
                **write_head = 1 << get_petal_pad_ch(petal, petal_pad_base);
                *write_head += 1;
                **write_head = 1 << get_petal_pad_ch(petal, petal_pad_cw);
                **write_head |= 1 << get_petal_pad_ch(petal, petal_pad_ccw);
                *write_head += 1;
                break;
            case PETAL_MODE_2D:  // be silly
                **write_head = 1 << get_petal_pad_ch(petal, petal_pad_base);
                *write_head += 1;
                **write_head = 1 << get_petal_pad_ch(petal, petal_pad_cw);
                *write_head += 1;
                **write_head = 1 << get_petal_pad_ch(petal, petal_pad_ccw);
                *write_head += 1;
                break;
        }
    }
}

// hardcoding this one.
// first and last must be >= 7 bc i2c golf reasons.
// keeping petal 2 away from the swap helps w noise and/or our ocd a bit.
static const uint16_t _cursed_seq[] = { 1 << 8, 1 << 0,  1 << 1,  1 << 2,
                                        1 << 3, 1 << 10, 1 << 11, 1 << 12,
                                        1 << 4, 1 << 5,  1 << 6,  1 << 7,
                                        1 << 9 };

static int _petal_modes_to_sequence(ad7147_chip_t *chip) {
    uint16_t *write_head = chip->sequence;
    for (int j = 0; j < chip->num_petals; j++) {  // go thru bot chip petals
        int petal = chip->petals[j];
        _write_petal_mode_seq(&write_head, petal, chip->modes[petal]);
    }
    int len = write_head - chip->sequence;
    assert(len < 14 || len >= 0);
    memset(write_head, 0, (13 - len) * sizeof(uint16_t));

    chip->cursed_active = len == 13;
    if (chip->cursed_active) {
        memcpy(chip->sequence, _cursed_seq, 13 * sizeof(uint16_t));
    }
#ifdef FLOW3R_BSP_CAPTOUCH_DEBUG_SEQUENCING
    write_head = chip->sequence;
    printf("%s: applying %d-sequence: ", chip->name, len);
    while ((write_head - chip->sequence) < 13)
        printf("%d ", (int)(*write_head)), write_head++;
    if (chip->cursed_active) printf(" (cursed sequence)");
    printf("\n");
#endif
    return len;
}

static ad7147_sequence_stage_t _cursed_swap_stage;

static inline int _mode_num_channels(flow3r_bsp_captouch_petal_mode_t mode) {
    switch (mode) {
        case PETAL_MODE_0D:
            return 1;
        case PETAL_MODE_1D:
            return 2;
        case PETAL_MODE_2D:
            return 3;
        default:
            return 0;
    }
}

static int _get_calib_data_index(ad7147_chip_t *chip, int i, int *petal_ret,
                                 int *index_ret) {
    assert(chip == &_top || chip == &_bot);
    bool top = chip == &_top;
    const pad_mapping_t *map = top ? _map_top : _map_bot;
    int step = chip->sequence[i];
    for (int j = 0; j < 13; j++) {
        if (step & (1 << j)) {
            int petal = map[j].petal_number;
            int calib_index = _mode_num_channels(chip->modes[petal]) - 1;
            if (calib_index < 0) continue;
            if (calib_index) {
                int offset = map[j].pad_kind;
                if (petal & 1) {
                    if (offset == 3) offset = 1;
                } else {  // treat tip and base as same bc bw compat
                    if (offset == 3) offset = 0;
                }
                if (calib_index == 1 && offset > 1) offset = 1;
                if (calib_index == 2) calib_index = 3;
                calib_index += offset;
            }
            *petal_ret = petal;
            *index_ret = calib_index;
            return true;
        }
    }
    return false;
}

void get_stage_hw_config(ad7147_chip_t *chip, ad7147_sequence_t *seq_out,
                         uint8_t i, uint16_t channel_mask) {
    int offset = chip->stages[i].afe_offset;
    offset = offset > (MAX_AFE) ? (MAX_AFE) : offset;
    seq_out->stages[i].channel_mask = channel_mask;
    // DATASHEET VIOLATION 2
    // the datasheet recommends to connect captouch pads to the internal bias
    // while they are not being measured. here's why we're not doing that:
    //
    // there was a strong cross talk issue on the bottom chip petals that
    // varied with grounding setup (e.g., plugging in a usb cable and putting
    // the other end in ur pocket changed behavior). we suspected that this
    // might be due to a capacitive interaction between device ground, chip bias
    // and chip shield driver. our wild theories were rewarded, and while
    // introducing a bit of noise (well within budget, mind you) this resolved
    // the issue.
    //
    // a few months later, a rare edge case was brought to our attention: when
    // many channels on the top chip petals were saturated, similar cross talk
    // could be generated. we applied the fix to the top chip too, and sure
    // enough it resolved the issue. which makes sense except for fact that the
    // top chip petals don't couple to a shield driver.
    //
    // we currently have no fully sensical mental model for why this works but
    // that's okay.

    // datasheet recommendation-ish (13-seq has hardcoded bits that violate
    // this, change manually!)
    // seq_out->stages[i].idle_to_bias = true;
    // experiment A: only top pads
    // seq_out->stages[i].idle_to_bias = !(chip->is_bot && (channel < 10));
    // experiment B: only top chip pads
    // seq_out->stages[i].idle_to_bias = !chip->is_bot;
    // experiment C: none (winner)
    seq_out->stages[i].idle_to_bias = false;
    if (offset <= MAX_NEG_AFE) {
        seq_out->stages[i].neg_afe_offset = offset;
        seq_out->stages[i].pos_afe_offset = 0;
    } else {
        seq_out->stages[i].neg_afe_offset = MAX_NEG_AFE;
        seq_out->stages[i].pos_afe_offset = offset - MAX_NEG_AFE;
    }
}

static void _notify_from_isr(uint32_t mask, TaskHandle_t handle) {
    if (handle == NULL) return;
    BaseType_t xHigherPriorityTaskWoken;
    xTaskNotifyFromISR(handle, mask, eSetBits, &xHigherPriorityTaskWoken);
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

static void _notify(uint32_t mask, TaskHandle_t handle) {
    if (handle == NULL) return;
    xTaskNotify(handle, mask, eSetBits);
    portYIELD();
}

static void _notify_both(uint32_t mask) {
    _notify(mask, _top.task);
    _notify(mask, _bot.task);
}

// bad hack, see below
static esp_err_t _cursed_sequence_request(ad7147_chip_t *chip) {
    uint16_t *seq = chip->sequence;
    ad7147_sequence_t seq_out;
    int petal, calib_index;
    if (_get_calib_data_index(chip, 12, &petal, &calib_index)) {
        chip->stages[12].amb = _chip_calibration_data[petal].amb[calib_index];
        chip->stages[12].afe_offset =
            _chip_calibration_data[petal].afe[calib_index];
        chip->stages[0].amb = _chip_calibration_data[petal].amb[calib_index];
        chip->stages[0].afe_offset =
            _chip_calibration_data[petal].afe[calib_index];
    }
    get_stage_hw_config(chip, &seq_out, 0, seq[12]);
    memcpy(&_cursed_swap_stage, &(seq_out.stages[0]),
           sizeof(ad7147_sequence_stage_t));
    return ESP_OK;
}

// This takes a sequence of stages from the chip config, gets
// the configuration data for the individual stages
// and writes it to the respective captouch chip.
// In case of the bottom chip we request a 13-sequence, so that's
// not possible, so we use a hardcoded hack to save the 13th
// sequence step configuration data for later use.
static void _sequence_request(ad7147_chip_t *chip, bool new_modes) {
    if (new_modes) {
        chip->len_sequence = _petal_modes_to_sequence(chip);
#ifdef FLOW3R_BSP_CAPTOUCH_DEBUG_PROFILING
    } else {
        uint16_t prev_seq[13];
        memcpy(prev_seq, chip->sequence, sizeof(chip->sequence));
        _petal_modes_to_sequence(chip);
        if (memcmp(prev_seq, chip->sequence, sizeof(chip->sequence))) {
            printf("sequence mismatch!\n");
        }
#endif
    }
    int len = chip->len_sequence;
    uint16_t *seq = chip->sequence;

    if (chip->cursed_active) {
        _cursed_sequence_request(chip);
        len = 12;
    }

    ad7147_sequence_t seq_out = {
        .len = len,
        .interrupt_stage = 0,
    };
    for (int i = 0; i < len; i++) {
        int petal, calib_index;
        assert(_get_calib_data_index(chip, i, &petal, &calib_index));
        chip->stages[i].amb = _chip_calibration_data[petal].amb[calib_index];
        chip->stages[i].afe_offset =
            _chip_calibration_data[petal].afe[calib_index];
    }
    for (size_t i = 0; i < seq_out.len; i++) {
        get_stage_hw_config(chip, &seq_out, i, seq[i]);
    }

    esp_err_t ret;
    if ((ret = ad7147_hw_configure_stages(&chip->dev, &seq_out)) != ESP_OK) {
        ESP_LOGE(TAG, "%s: requesting next sequence failed: %s", chip->name,
                 esp_err_to_name(ret));
    }
    if (!len) {
        petals_clear(chip, esp_timer_get_time());
    }
}

static bool _stage_afe_tweak(ad7147_chip_t *chip, size_t cix) {
    int32_t cur = chip->stages[cix].amb;
    int32_t target = _calib_target;
    int32_t diff = (cur - target) / _calib_incr_cap;
    if (diff < 1 && diff > -1) {
        return false;
    }
    int32_t offset = chip->stages[cix].afe_offset;
    if (offset <= 0 && diff < 0) {
        return false;
    }
    if (offset >= (MAX_AFE) && diff > 0) {
        return false;
    }
    offset += diff;
    if (offset < 0) {
        offset = 0;
    }
    if (offset > (MAX_AFE)) {
        offset = (MAX_AFE);
    }
    chip->stages[cix].afe_offset = offset;
    return true;
}

// Takes data from captouch chip readout, looks up which petal pad
// it belongs to according to the chip map and puts it there.
// May need to take lock for raw_petals access depending on circumstances
// (atm just for bottom chip).
// Should maybe get a proper name someday. Sorry. Only used by
// raw_data_to_petal_pads atm, maybe it should just absorb it, it's
// not that big.
static void _on_chip_data(ad7147_chip_t *chip, uint16_t *values, int len) {
    assert(chip == &_top || chip == &_bot);
    bool top = chip == &_top;
    const pad_mapping_t *map = top ? _map_top : _map_bot;

    for (int i = 0; i < len; i++) {
        int step = chip->sequence[i];
        int num_fields = 0;
        int fields[3];
        for (int j = 0; j < (top ? 12 : 13); j++) {
            if (step & (1 << j)) {
                fields[num_fields] = j;
                num_fields++;
                if (num_fields == 3) break;
            }
        }
        if (!num_fields) continue;
        int value = values[i] / num_fields;
        for (int k = 0; k < num_fields; k++) {
            int petal = map[fields[k]].petal_number;
            int pad = map[fields[k]].pad_kind;
            if (petal >= 10 || petal < 0 || pad >= 4 || pad < 0) {
                ESP_LOGE(TAG, "out of bounds at petal %d pad %d stage %d\n",
                         petal, pad, i);
            } else {
                raw_petals[petal][pad] = value;
            }
        }
    }
}

// lazy copypasta code :3
static void _on_chip_calib_data(ad7147_chip_t *chip, uint16_t *amb,
                                uint16_t *afe, size_t len) {
    for (int i = 0; i < len; i++) {
        int petal, calib_index;
        assert(_get_calib_data_index(chip, i, &petal, &calib_index));
        _chip_calibration_data[petal].amb[calib_index] = amb[i];
        _chip_calibration_data[petal].afe[calib_index] = afe[i];
    }
}

static void raw_data_to_calib(ad7147_chip_t *chip, uint16_t *data, size_t len) {
    if (chip->calibration_cycles > 0) {
        // We're doing a calibration cycle on our channels. Instead of
        // writing the data to channel->cdc, write it to channel->amb_meas.
        int cycle = chip->calibration_cycles - 1;
        if (cycle < _AD7147_CALIB_CYCLES) {  // throw away first few points
            for (int i = 0; i < len; i++) {
                chip->stages[i].amb_meas[cycle] = data[i];
            }
        }
    }

    // Deal with active calibration.
    chip->calibration_cycles--;
    if (chip->calibration_cycles <= 0) {
        // Calibration measurements done. Calculate average amb data for
        // each channel.
        uint16_t amb[13];
        uint16_t afe[13];
        uint16_t rerun = 0;
        for (size_t i = 0; i < len; i++) {
            uint32_t avg = 0;
            for (uint8_t j = 0; j < _AD7147_CALIB_CYCLES; j++) {
                avg += chip->stages[i].amb_meas[j];
            }
            chip->stages[i].amb = avg / _AD7147_CALIB_CYCLES;
            // Can we tweak the AFE to get a better measurement?
            if (_stage_afe_tweak(chip, i)) {
                rerun |= (1 << i);
            }
            amb[i] = chip->stages[i].amb;
            afe[i] = chip->stages[i].afe_offset;
        }
        _on_chip_calib_data(chip, amb, afe, len);
        if (rerun != 0) {
            chip->calibration_cycles = _AD7147_CALIB_CYCLES + 5;
            _notify(NOTIF_INTERNAL_CALIB_CHANGE, chip->task);
#ifdef FLOW3R_BSP_CAPTOUCH_DEBUG_CALIBRATION
            printf("%s calibration stage %d: repeating ", chip->name,
                   chip->calibration_stage);
            for (int i = 0; i < 13; i++) {
                if (rerun & 1 << i) {
                    printf("%d, ", i);
                }
            }
            printf(" (%d, %d)\n", (int)rerun, (int)len);
            for (int i = 0; i < 13; i++) {
                if (rerun & 1 << i) {
                    printf("  %s stage %d: amb %d, afe %d\n", chip->name, i,
                           (int)chip->stages[i].amb,
                           (int)chip->stages[i].afe_offset);
                }
            }
#endif
        } else {
#ifdef FLOW3R_BSP_CAPTOUCH_DEBUG_CALIBRATION
            printf("%s calibration stage %d: done\n", chip->name,
                   chip->calibration_stage);
            for (int i = 0; i < len; i++) {
                printf("  %s stage %d: amb %d, afe %d\n", chip->name, i,
                       (int)chip->stages[i].amb,
                       (int)chip->stages[i].afe_offset);
            }
#endif

            chip->calibration_stage_active = false;

            ad7147_chip_t *other = chip == &_top ? &_bot : &_top;
            _notify(NOTIF_INTERNAL_CALIB_NEXT_STAGE, other->task);

            ESP_LOGI(TAG, "%s: calibration stage done.", chip->name);
        }
    }
}

static void raw_data_to_petal_pads(ad7147_chip_t *chip, uint16_t *data,
                                   size_t len) {
    // Normal measurement, apply to channel->cdc.
    for (size_t i = 0; i < len; i++) {
        chip->stages[i].cdc = data[i];
    }
    // Submit data to higher level for processing.
    uint16_t val[13];
    for (size_t i = 0; i < len; i++) {
        int32_t cdc = chip->stages[i].cdc;
        int32_t amb = chip->stages[i].amb;
        int32_t diff = cdc - amb;
        val[i] = diff < 0 ? 0 : (diff > 65535 ? 65535 : diff);
    }
    _on_chip_data(chip, val, len);
}

// (sunshine emoji) tell the captouch task that top chip data is available :D
static void _top_isr(void *data) {
    _notify_from_isr(NOTIF_GPIO, _top_task_handle);
}

// (dark cloud emoji) tell the cursed task that it is time for THE PROCEDURE
static void _bot_isr(void *data) {
    _notify_from_isr(NOTIF_GPIO, _bot_task_handle);
}

typedef struct {
    uint16_t buffer[13];
    uint16_t buffer_len;
    uint16_t reject;
    // normal process only
    uint16_t previously_filled;
    uint16_t interrupt_stage;
    // cursed process only
    uint16_t step;
} _chip_process_data_t;

static void _chip_process_reset(_chip_process_data_t *process_data) {
    process_data->previously_filled = 0;
    process_data->interrupt_stage = 0;
    process_data->step = 0;
    process_data->buffer_len = 0;
    process_data->reject = 2;
}

#ifdef FLOW3R_BSP_CAPTOUCH_DEBUG_CURSED_PROFILING
static int64_t _cursed_step_time = 0;
static uint32_t _cursed_step_to_step_time[4];
static uint32_t _cursed_step_execution_time[4];
#endif

// THE PROCEDURE
// we need to reconfigure the bottom chip on the fly to read all 13 channels.
// this here is pretty much a shim between the bottom chip isr and the main
// captouch task that manages the i2c traffic to the bottom chip as well as
// poking the main captouch task when data is ready.
// this is kinda sensitive and overall weird so we keep our lil crappy profiler
// hooked up for now, we're sure we'll need it in the future again.
static bool _cursed_chip_process(ad7147_chip_t *chip, uint32_t *timestamp,
                                 _chip_process_data_t *process_data) {
    ad7147_hw_t *device = &chip->dev;
    uint16_t st = 0;
#ifdef FLOW3R_BSP_CAPTOUCH_DEBUG_CURSED_PROFILING
    int64_t time = esp_timer_get_time();
#endif
    // this boi clears the interrupt flag of the captouch chip so it must
    // run _every time_ we receive an interrupt. &st tells us which stages
    // have been completed since the last call.
    if (!ad7147_hw_get_and_clear_completed(device, &st)) {
        return false;
    }
    // let's pretend for fun that channel0 is read by stage0 and so forth.
    // makes describing this a lot easier.
    // also i2c traffic functions are highly golfed, pls don't tear them apart
    // it saves like 1% cpu so be cool~
    switch (process_data->step) {
        case 0:
            // read all 12 "regular" channels into the array
            if (!ad7147_hw_get_cdc_data(device, process_data->buffer, 12)) {
                return false;
            }
            process_data->buffer_len = 12;
            // reconfigures stage0 to channel 13 config, set up sequencer
            // to loop only stage0 (<-actually a lie but dw abt it~)
            if (!ad7147_hw_modulate_stage0_and_reset(device,
                                                     &_cursed_swap_stage)) {
                return false;
            }
            // clear stray freertos interrupts, then clear hw interrupts.
            // must be done in that order.
            xTaskNotifyStateClear(NULL);
            if (!ad7147_hw_get_and_clear_completed(device, &st)) {
                return false;
            }
            break;
        // case 1: the data right after reconfiguration might be junk
        //         so we wait for another cycle.
        case 2:
            // grab data for channel 13
            if (!ad7147_hw_get_cdc_data(device, &(process_data->buffer[12]),
                                        1)) {
                return false;
            }
            *timestamp = esp_timer_get_time();
            // reconfigure stage0 to channel 0 config, set up sequencer
            // to loop all stages
            if (!ad7147_hw_modulate_stage0_and_reset(device, NULL)) {
                return false;
            }
            // clear stray freertos interrupts, then clear hw interrupts.
            // must be done in that order.
            xTaskNotifyStateClear(NULL);
            if (!ad7147_hw_get_and_clear_completed(device, &st)) {
                return false;
            }
            if (process_data->buffer_len != 12) {
                process_data->step = 0;
                return false;
            }
            process_data->buffer_len = 13;
            break;
            // case 3:
            //  "whoa whoa whoa if you do the same here as with case 1
            //  you'd wait for the whole 12-sequence without any good
            //  reason!"
            // observant, but there's a hidden trick! both case2->case3 and
            // case 3->case0 have the sequencer configure to 12 steps but
            // consider: case 2 resets to stage0 (only stage we can reset to,
            // remember?), and the next interrupt is triggered once that is
            // complete! that is the magic of ISR trigger reconf laziness!
    }
#ifdef FLOW3R_BSP_CAPTOUCH_DEBUG_CURSED_PROFILING
    _cursed_step_to_step_time[process_data->step] = time - _cursed_step_time;
    _cursed_step_time = esp_timer_get_time();
    _cursed_step_execution_time[process_data->step] = _cursed_step_time - time;
#endif
    process_data->step = (process_data->step + 1) % 4;
    return !process_data->step;
}

static bool _chip_process(ad7147_chip_t *chip, uint32_t *timestamp,
                          _chip_process_data_t *process_data) {
    if (chip->cursed_active)
        return _cursed_chip_process(chip, timestamp, process_data);
    ad7147_hw_t *device = &chip->dev;
    // Read complete status register. This acknowledges interrupts.
    uint16_t st = 0;
    if (!ad7147_hw_get_and_clear_completed(device, &st)) {
        return false;
    }

    // Nothing to do if no stages are expected to be read.
    if (device->num_stages < 1) {
        return false;
    }

    int count = device->num_stages;
    process_data->previously_filled |= st;
    if (process_data->previously_filled < (1 << count) - 1) {
        return false;
    }
    if (!ad7147_hw_get_cdc_data(device, process_data->buffer, count)) {
        return false;
    }
    *timestamp = esp_timer_get_time();
    // so here's another curveball: we can't really lock on the output register
    // of the captouch driver, it just keeps writing to it, so we get FUTURE
    // DATA, whoa! this is actually bad because you may receive the same NO MORE
    // FUTURE BUT NOW PRESENT DATA in the next round, because if you do any sort
    // of velocity calculation this will bite u ^w^.
    // we'd love to use the reset trick from the cursed sequence here, but turns
    // out the jank behaves slightly different and procudes twitches on the
    // 11th/last channel. so we're chasing st instead by moving around the
    // interrupt stage. fun :3. this is actually faster than the reset sequence.
    // and yeeeah for some applications future data is fine, and then we're
    // sacrificing precious cycles here. but it's typically less than a ms. it's
    // okay. it's the better default. also it's silly code :3
    if (!ad7147_hw_get_and_clear_completed(device, &st)) return false;
    st = st & ((1 << count) - 1);  // sanitize
    if (st) {
        int new_interrupt_stage = -1;
        for (int i = 0; i < count; i++) {
            int stage = (count + process_data->interrupt_stage - i) % count;
            if (st & 1 << stage) {
                new_interrupt_stage = stage;
                break;
            }
        }
        if (new_interrupt_stage < 0) {
            ESP_LOGE(TAG, "bad stage vector");
        }
        // there is a question to be asked if we gain anything by not setting
        // the interrupt to "slice apart" petals. due to random timing between
        // isr receive and us grabbing the data it's a bit unpredictable, but
        // we'd definitely catch much less ~positional data temporal
        // dyslocation~ phenomena :3. but it'd be slower. and probably not super
        // duper reliable. we think we wanna be lazy here and argue that we care
        // most about the minimum data quality, which does not change from
        // adding this. we get some datarate++ in return. yay datarate!
        if (new_interrupt_stage != process_data->interrupt_stage) {
            if (!ad7147_hw_override_interrupt_stage(
                    device, process_data->interrupt_stage))
                return false;
            process_data->interrupt_stage = new_interrupt_stage;
        }
        if (!ad7147_hw_get_and_clear_completed(device, &st)) return false;
        if (st >= (1 << count) - 1) _notify(NOTIF_GPIO, chip->task);
    }
    process_data->buffer_len = count;
    process_data->previously_filled = st;
    return true;
}

static bool _chip_process_kickstart(ad7147_chip_t *chip) {
    ad7147_hw_t *device = &chip->dev;
    uint16_t st = 0;
    if (!ad7147_hw_get_and_clear_completed(device, &st)) {
        ESP_LOGE(TAG, "kickstart failed");
        return false;
    }
    return true;
}

#ifdef FLOW3R_BSP_CAPTOUCH_DEBUG_PROFILING
typedef struct {
    uint32_t timestamps[100];
    uint32_t index;
    ad7147_chip_t *chip;
} _profiler_t;

#ifdef FLOW3R_BSP_CAPTOUCH_DEBUG_CURSED_PROFILING
static void _cursed_profiler_run() {
    for (uint16_t i = 0; i < 4; i++) {
        uint16_t k = (i + 3) % 4;
        printf("last bot step %u to step %u time: %luus\n", k, i,
               _cursed_step_to_step_time[i]);
    }
    for (uint16_t i = 0; i < 4; i++) {
        printf("last bot step %u execution time: %luus\n", i,
               _cursed_step_execution_time[i]);
    }
}
#endif

static void _profiler_run(_profiler_t *self, uint32_t timestamp) {
    if (self->index < 100) {
        self->timestamps[self->index] = timestamp;
        self->index++;
    } else {
        uint32_t avg = self->timestamps[99] - self->timestamps[3];
        avg /= (99 - 3);
        uint32_t min = UINT32_MAX;
        uint32_t max = 0;
        for (int i = 3; i < 99; i++) {
            uint32_t time = self->timestamps[i + 1] - self->timestamps[i];
            if (time > max) max = time;
            if (time < min) min = time;
        }
        if (chip->is_bot) {
            printf(
                "cycle time bot [us]:                       |  %5lu  %5lu  "
                "%5lu\n",
                min, avg, max);
#ifdef FLOW3R_BSP_CAPTOUCH_DEBUG_CURSED_PROFILING
            if (chip->cursed_active) {
                _cursed_profiler_run();
            }
#endif
        } else {
            printf("cycle time top [us]:  %5lu  %5lu  %5lu  |\n", min, avg,
                   max);
        }
        self->index = 0;
    }
}
#endif

static void _calibration_init_next_stage(ad7147_chip_t *chip,
                                         _chip_process_data_t *process_data) {
    chip->calibration_proceed = false;
    int stage = chip->calibration_stage - 1;
    if (stage > 0) {
#ifdef FLOW3R_BSP_CAPTOUCH_DEBUG_CALIBRATION
        printf("%s: calibration stage %d\n", chip->name, stage);
#endif
        chip->calibration_active = true;
        flow3r_bsp_captouch_petal_mode_t modes[10];
        for (int i = 0; i < 10; i++) {
            int limit = 3 - (i & 1);
            modes[i] = stage > limit ? limit : stage;
        }
        chip->calibration_stage = stage;
        chip->calibration_stage_active = true;
        chip->calibration_cycles = _AD7147_CALIB_CYCLES + 5;

        memcpy(chip->modes, modes, sizeof(chip->modes));
        _sequence_request(chip, true);
        _chip_process_reset(process_data);
        _chip_process_kickstart(chip);
    } else {
        if (stage < 0) {
            // restore previous calibration
            _notify(NOTIF_CALIB_CHANGE, chip->task);
#ifdef FLOW3R_BSP_CAPTOUCH_DEBUG_CALIBRATION
            printf("%s: calibration stopped\n", chip->name);
#endif
        } else {
            // need to hold the lock for checking notifs
            xSemaphoreTake(calibration_lock, portMAX_DELAY);
            uint32_t notif;
            // it's okay, we're not clearing any and make it pending later with
            // NOTIF_MODE_CHANGE
            if ((xTaskNotifyWait(0, 0, &notif, 0) == pdTRUE) &&
                (notif & NOTIF_CALIB_CHANGE)) {
#ifdef FLOW3R_BSP_CAPTOUCH_DEBUG_CALIBRATION
                printf(
                    "%s: calibration data ignored because of pending user "
                    "data\n",
                    chip->name);
#endif
            } else {
                memcpy(&calibration_data, &_chip_calibration_data,
                       sizeof(ad7147_petal_calib_t) * 10);
#ifdef FLOW3R_BSP_CAPTOUCH_DEBUG_CALIBRATION
                printf("%s: calibration done\n", chip->name);
#endif
            }
            xSemaphoreGive(calibration_lock);
        }

        chip->calibration_stage = 0;
        chip->calibration_stage_active = false;
        chip->calibration_cycles = 0;
        chip->calibration_active = false;
        _chip_process_reset(process_data);
        _notify(NOTIF_MODE_CHANGE, chip->task);
    }
}

static void _task(ad7147_chip_t *chip) {
    assert(chip == &_bot || chip == &_top);
    uint32_t timestamp = 0;
    _chip_process_data_t process_data;
    _chip_process_reset(&process_data);
#ifdef FLOW3R_BSP_CAPTOUCH_DEBUG_PROFILING
    _profiler_t profiler = { index = 0, chip = chip };
#endif
    for (;;) {
        uint32_t notif;
        if (xTaskNotifyWait(0, NOTIF_CLEAR_MASK, &notif, portMAX_DELAY) ==
            pdFALSE) {
            ESP_LOGE(TAG, "%s: Notification receive failed", chip->name);
            continue;
        }
        if (notif & NOTIF_CALIB_CHANGE) {
            xSemaphoreTake(calibration_lock, portMAX_DELAY);
            memcpy(&_chip_calibration_data, &calibration_data,
                   sizeof(ad7147_petal_calib_t) * 10);
            xSemaphoreGive(calibration_lock);
        }
        if (notif & (NOTIF_CALIB_STOP | NOTIF_CALIB_CHANGE)) {
            if (chip->calibration_active) {
                chip->calibration_stage = -1;
                _calibration_init_next_stage(chip, &process_data);
            }
            // drop NOTIF_CALIB_START if _STOP or _CHANGE have been received in
            // the same notif
        } else if (notif & NOTIF_CALIB_START) {
            chip->calibration_active = true;
            // we just wanna bonk the lock to make sure nobody is holding it
            xSemaphoreBonk(calibration_lock, portMAX_DELAY);
            petals_clear(chip, esp_timer_get_time());
#ifdef FLOW3R_BSP_CAPTOUCH_DEBUG_CALIBRATION
            printf("%s: starting calibration stage\n", chip->name);
#endif
            chip->calibration_stage = 4;
            _calibration_init_next_stage(chip, &process_data);
            // drop _NEXT_STAGE if (see above)
        } else if (notif & NOTIF_INTERNAL_CALIB_NEXT_STAGE) {
            chip->calibration_proceed = true;
        }
        if (chip->calibration_proceed && (!chip->calibration_stage_active)) {
            _calibration_init_next_stage(chip, &process_data);
        }
        if (notif & NOTIF_GPIO) {
            bool new_data = _chip_process(chip, &timestamp, &process_data);
            if (new_data) {
                if (process_data.reject > 0) {
                    process_data.reject -= 1;
                    new_data = false;
                }
            }
            if (new_data) {
                if (chip->calibration_stage_active) {
                    raw_data_to_calib(chip, process_data.buffer,
                                      process_data.buffer_len);
                }
                if (!chip->calibration_active) {
                    raw_data_to_petal_pads(chip, process_data.buffer,
                                           process_data.buffer_len);
                    petals_process(chip, timestamp);
#ifdef FLOW3R_BSP_CAPTOUCH_DEBUG_PROFILING
                    // not running this during calibration bc it's annoying
                    _profiler_run(&profiler, timestamp);
#endif
                }
                process_data.buffer_len = 0;
            }
        }
        if (notif & (NOTIF_MODE_CHANGE | NOTIF_CALIB_CHANGE |
                     NOTIF_INTERNAL_CALIB_CHANGE)) {
            bool new_modes = notif & NOTIF_MODE_CHANGE;
            if (new_modes) {
                xSemaphoreTake(chip->user_lock, portMAX_DELAY);
                new_modes =
                    memcmp(chip->modes, chip->user_modes, sizeof(chip->modes));
                if (new_modes) {
                    memcpy(chip->modes, chip->user_modes, sizeof(chip->modes));
                }
                xSemaphoreGive(chip->user_lock);
            }
            _sequence_request(chip, new_modes);
            _chip_process_reset(&process_data);
            _chip_process_kickstart(chip);
        }
    }
}

static void _bot_task(void *data) {
    (void)data;
    _task(&_bot);
}

static void _top_task(void *data) {
    (void)data;
    _task(&_top);
}

void flow3r_bsp_ad7147_get_petal_modes(flow3r_bsp_captouch_petal_mode_t *data) {
    memcpy(data, _petal_modes, sizeof(_petal_modes));
}

static void _set_petal_modes(flow3r_bsp_captouch_petal_mode_t *data) {
    for (int i = 0; i < 2; i++) {
        ad7147_chip_t *chip = i ? &_bot : &_top;
        xSemaphoreTake(chip->user_lock, portMAX_DELAY);
        memcpy(chip->user_modes, data, sizeof(chip->user_modes));
        xSemaphoreGive(chip->user_lock);
        _notify(NOTIF_MODE_CHANGE, chip->task);
    }
}

void flow3r_bsp_ad7147_set_petal_modes(flow3r_bsp_captouch_petal_mode_t *data) {
    memcpy(_petal_modes, data, sizeof(_petal_modes));
    _set_petal_modes(_petal_modes);
}

void flow3r_bsp_ad7147_calibrate() {
    // the notification would set them as soon as it is processed
    // but we do want those flags earlier
    // would be nice if we could just read the notification vector
    _bot.calibration_active = true;
    _top.calibration_active = true;
    _notify_both(NOTIF_CALIB_START);
}

int flow3r_bsp_ad7147_calibrating() {
    if (_bot.calibration_active || _top.calibration_active) {
        return _bot.calibration_stage > _top.calibration_stage
                   ? _bot.calibration_stage
                   : _top.calibration_stage;
    }
    return 0;
}

bool flow3r_bsp_ad7147_get_calibration_data(int32_t *data) {
    if (_top.calibration_active || _bot.calibration_active) return false;
    xSemaphoreTake(calibration_lock, portMAX_DELAY);
    for (int petal = 0; petal < 10; petal++) {
        for (int calib_index = 0; calib_index < 6; calib_index++) {
            int data_index = petal * 12 + calib_index * 2;
            if ((petal & 1) && calib_index >= 3) {
                data[data_index] = 0;
                data[data_index + 1] = 0;
            } else {
                data[data_index] = calibration_data[petal].amb[calib_index];
                data[data_index + 1] = calibration_data[petal].afe[calib_index];
            }
        }
    }
    xSemaphoreGive(calibration_lock);
    return true;
}

static uint16_t amb_limit(int32_t data) {
    return data > 65535 ? 65535 : (data < 0 ? 0 : data);
}

static uint8_t afe_limit(int32_t data) {
    return data > (MAX_AFE) ? (MAX_AFE) : (data < 0 ? 0 : data);
}

void flow3r_bsp_ad7147_set_calibration_data(int32_t *data) {
    xSemaphoreTake(calibration_lock, portMAX_DELAY);
    for (int petal = 0; petal < 10; petal++) {
        for (int calib_index = 0; calib_index < 6; calib_index++) {
            int data_index = petal * 12 + calib_index * 2;
            if (!((petal & 1) && calib_index >= 3)) {
                calibration_data[petal].amb[calib_index] =
                    amb_limit(data[data_index]);
                calibration_data[petal].afe[calib_index] =
                    afe_limit(data[data_index + 1]);
            }
        }
    }
    // we must notify while holding the lock so if the calibration has
    // not stopped yet it will not write its results to calibration_data
    _notify_both(NOTIF_CALIB_CHANGE);
    xSemaphoreGive(calibration_lock);
}

void flow3r_bsp_ad7147_get(flow3r_bsp_captouch_data_t *dest) {
    xSemaphoreTakeCaptouchOutput(portMAX_DELAY);
    memcpy(dest, &captouch_data, sizeof(captouch_data));
    xSemaphoreGiveCaptouchOutput();
}

void flow3r_bsp_ad7147_refresh_events() {
    xSemaphoreTakeCaptouchOutput(portMAX_DELAY);
    for (uint8_t i = 0; i < 10; i++) {
        captouch_data.petals[i].press_event = latches[i].press_event_new;
        latches[i].fresh = true;
    }
    xSemaphoreGiveCaptouchOutput();
}

esp_err_t flow3r_bsp_ad7147_chip_init(ad7147_chip_t *chip) {
    esp_err_t ret;
    for (size_t i = 0; i < 13; i++) {
        chip->stages[i].amb = 0;
    }
    if ((ret = ad7147_hw_init(&chip->dev)) != ESP_OK) {
        return ret;
    }
    return ESP_OK;
}

esp_err_t _gpio_interrupt_setup(gpio_num_t num, gpio_isr_t isr) {
    esp_err_t ret;

    gpio_config_t io_conf = {
        .intr_type = GPIO_INTR_NEGEDGE,
        .mode = GPIO_MODE_INPUT,
        .pull_up_en = true,
        .pin_bit_mask = (1 << num),
    };
    if ((ret = gpio_config(&io_conf)) != ESP_OK) {
        return ret;
    }
    if ((ret = gpio_isr_handler_add(num, isr, NULL)) != ESP_OK) {
        return ret;
    }
    return ESP_OK;
}

void assertSemaphoreCreateMutex(SemaphoreHandle_t *target) {
    assert(*target == NULL);
    *target = xSemaphoreCreateMutex();
    assert(*target != NULL);
}

esp_err_t flow3r_bsp_ad7147_init() {
    assertSemaphoreCreateMutex(&calibration_lock);

    for (int i = 0; i < 2; i++) {
        ad7147_chip_t *chip = i ? &_top : &_bot;
        assertSemaphoreCreateMutex(&chip->output_lock);
        assertSemaphoreCreateMutex(&chip->user_lock);
        chip->dev.dev_config.decimation = 0b10;
        memcpy(chip->user_modes, _petal_modes, sizeof(chip->user_modes));
    }

    esp_err_t ret;

    for (int i = 0; i < 10; i++) {
        captouch_data.petals[i].index = i;
    }

    _top.dev.addr = flow3r_i2c_addresses.touch_top;
    _bot.dev.addr = flow3r_i2c_addresses.touch_bottom;

    xTaskCreate(&_top_task, "captouch-top", 4096, NULL,
                configMAX_PRIORITIES - 2, &_top_task_handle);
    xTaskCreate(&_bot_task, "captouch-bot", 4096, NULL,
                configMAX_PRIORITIES - 2, &_bot_task_handle);

    _top.task = _top_task_handle;
    _bot.task = _bot_task_handle;

    if ((ret = flow3r_bsp_ad7147_chip_init(&_top)) != ESP_OK) {
        return ret;
    }
    if ((ret = flow3r_bsp_ad7147_chip_init(&_bot)) != ESP_OK) {
        return ret;
    }

    if ((ret = gpio_install_isr_service(ESP_INTR_FLAG_SHARED |
                                        ESP_INTR_FLAG_LOWMED)) != ESP_OK) {
        ESP_LOGE(TAG, "Failed to install GPIO ISR service");
        return ret;
    }
    if ((ret = _gpio_interrupt_setup(_interrupt_gpio_bot, _bot_isr)) !=
        ESP_OK) {
        ESP_LOGE(TAG, "Failed to add bottom captouch ISR");
        return ret;
    }
    if (!_interrupt_shared) {
        // On badges with shared interrupts, only install the 'bot' ISR as a
        // shared ISR.
        if ((ret = _gpio_interrupt_setup(_interrupt_gpio_top, _top_isr)) !=
            ESP_OK) {
            ESP_LOGE(TAG, "Failed to add top captouch ISR");
            return ret;
        }
    }

    flow3r_bsp_ad7147_calibrate();
    ESP_LOGI(TAG, "Captouch initialized");
    return ESP_OK;
}

// roughly matches the behavior of the legacy api. someday we should have more
// meaningful output units.

#define TOP_PETAL_THRESHOLD 8000
#define BOTTOM_PETAL_THRESHOLD 12000
#define PETAL_HYSTERESIS 1000

#define PETAL_TOP_GAIN 5300
#define PETAL_TOP_2_GAIN 6000
#define PETAL_BOT_GAIN 6500

static inline int16_t clip_to_int16(int32_t num) {
    return (num > 32767 ? 32767 : (num < -32768 ? -32768 : num));
}

static int32_t top_petal_filter[10];

static int32_t petal_pos_gain[10] = {
    PETAL_TOP_GAIN, PETAL_BOT_GAIN, PETAL_TOP_2_GAIN, PETAL_BOT_GAIN,
    PETAL_TOP_GAIN, PETAL_BOT_GAIN, PETAL_TOP_GAIN,   PETAL_BOT_GAIN,
    PETAL_TOP_GAIN, PETAL_BOT_GAIN,
};

static inline void pos_calc_top(int index, int mode, int *rad, int *phi,
                                int *raw_sum, uint16_t *raw_petal) {
    int32_t cw = raw_petal[petal_pad_cw];
    int32_t ccw = raw_petal[petal_pad_ccw];
    int32_t base = raw_petal[petal_pad_base];
    int32_t tip = ((cw + ccw) * 3) >> 2;
    *raw_sum = base + ccw + cw;
    if (mode > 1) {
        int rad_calc = tip - base;
        rad_calc *= petal_pos_gain[index];
        rad_calc /= ((tip + base) >> 2) + 1;
        if (mode > 2) {
            int phi_calc = cw - ccw;
            phi_calc *= petal_pos_gain[index];
            phi_calc /= ((cw + ccw) >> 2) + 1;
            phi_calc = (phi_calc * 10) / 6;

            int phi_sq = (phi_calc * phi_calc) >> 15;
            int phi_qb = (phi_sq * phi_calc) >> 15;
            int blend = (rad_calc + (18 << 10)) >> 3;
            phi_calc = (phi_calc * blend + phi_qb * ((1 << 12) - blend)) >> 12;

            phi_calc = (phi_calc * 6) / 10;

            *phi = phi_calc;
        } else {
            *phi = 0;
        }
        rad_calc -= 4096;
        rad_calc += rad_calc >> 3;
        *rad = rad_calc;
    } else {
        *rad = 0;
    }
}

static inline void pos_calc_bot(int index, int mode, int *rad, int *raw_sum,
                                uint16_t *raw_petal) {
    int32_t tip = raw_petal[petal_pad_tip];
    int32_t base = raw_petal[petal_pad_base];
    *raw_sum = base + tip;
    if (mode > 1) {
        base += ((base * 3) >> 2);  // tiny gain correction
        int rad_calc = tip - base;
        rad_calc *= petal_pos_gain[index];
        rad_calc /= ((tip + base) >> 2) + 1;

        rad_calc = 16384 - rad_calc;

        int rad_calc_gain = rad_calc > 0 ? rad_calc : 0;
        rad_calc_gain = (rad_calc_gain + 32768) >> 5;
        rad_calc_gain = (rad_calc_gain * rad_calc_gain) >> 12;
        rad_calc = (rad_calc * rad_calc_gain) >> 10;

        rad_calc = 16384 - rad_calc;

        *rad = rad_calc;
    } else {
        *rad = 0;
    }
}

#ifdef FLOW3R_BSP_CAPTOUCH_SECONDARY_OUTPUT
static int32_t top_petal_filter2[10];

static inline void pos_calc_top2(int index, int mode, int *rad, int *phi,
                                 int *raw_sum, uint16_t *raw_petal) {
    int32_t cw = raw_petal[petal_pad_cw];
    int32_t ccw = raw_petal[petal_pad_ccw];
    int32_t base = raw_petal[petal_pad_base];
    int32_t tip = (ccw + cw) >> 1;
    *raw_sum = base + ccw + cw;
    if (mode > 1) {
        int rad_calc = tip - base;
        rad_calc *= petal_pos_gain[index];
        rad_calc /= ((tip + base) >> 2) + 1;
        *rad = rad_calc;
        if (mode > 2) {
            int phi_calc = cw - ccw;
            phi_calc *= petal_pos_gain[index];
            phi_calc /= ((cw + ccw) >> 2) + 1;
            *phi = phi_calc;
        } else {
            *phi = 0;
        }
    } else {
        *rad = 0;
    }
}

static inline void pos_calc_bot2(int index, int mode, int *rad, int *raw_sum,
                                 uint16_t *raw_petal) {
    int32_t tip = raw_petal[petal_pad_tip];
    int32_t base = raw_petal[petal_pad_base];
    *raw_sum = base + tip;
    if (mode > 1) {
        base += ((base * 3) >> 2);  // tiny gain correction
        int rad_calc = tip - base;
        rad_calc *= petal_pos_gain[index];
        rad_calc /= ((tip + base) >> 2) + 1;
        *rad = rad_calc;
    } else {
        *rad = 0;
    }
}
#endif

static void petal_process(int index, uint32_t timestamp, int mode,
                          SemaphoreHandle_t lock) {
    // lock only locks multiple readers and one writer.
    // for multiple writers you need to lock around this function.
    flow3r_bsp_captouch_petal_data_t *petal = &(captouch_data.petals[index]);
    if (!mode) {
        if (!petal->mode) return;
        if (lock) xSemaphoreTake(lock, portMAX_DELAY);
        petal->mode = 0;
        petal->pressed = false;
        petal->coverage = 0;
        petal->rad = 0;
        petal->phi = 0;
#ifdef FLOW3R_BSP_CAPTOUCH_SECONDARY_OUTPUT
        petal->rad2 = 0;
        petal->phi2 = 0;
#endif
        if (lock) xSemaphoreGive(lock);
        return;
    }
    bool top = (index % 2) == 0;

    int rad = 0;
    int phi = 0;
    int raw_sum = 0;

    if (top) {
        pos_calc_top(index, mode, &rad, &phi, &raw_sum, raw_petals[index]);
#if defined(CONFIG_FLOW3R_HW_GEN_P3)
        rad = -rad;
#endif
        phi = clip_to_int16(phi);
    } else {
        pos_calc_bot(index, mode, &rad, &raw_sum, raw_petals[index]);
    }
    rad = clip_to_int16(rad);

    raw_sum = (raw_sum * 1024) /
              (top ? (TOP_PETAL_THRESHOLD) : (BOTTOM_PETAL_THRESHOLD));

    bool pressed_prev = petal->pressed;
    bool pressed = raw_sum > (pressed_prev ? 896 : 1024);
    bool reset_filters = pressed && (!pressed_prev) && (mode > 1);

    if ((!latches[index].press_event_new) || latches[index].fresh) {
        latches[index].press_event_new = pressed;
        latches[index].fresh = false;
    }
    raw_sum = raw_sum > 65535 ? 65535 : raw_sum;

#ifdef FLOW3R_BSP_CAPTOUCH_SECONDARY_OUTPUT
    int rad2 = rad;
    int phi2 = phi;
    int raw_sum2 = 0;
    if (top) {
        pos_calc_top2(index, mode, &rad2, &phi2, &raw_sum2, raw_petals[index]);
        phi2 = clip_to_int16(phi2);
    } else {
        pos_calc_bot2(index, mode, &rad2, &raw_sum2, raw_petals[index]);
    }
    rad2 = clip_to_int16(rad2);
#endif

#ifdef FLOW3R_BSP_CAPTOUCH_DEBUG_PETAL
    static int print_petal_counter = 0;
    int base = raw_petals[index][petal_pad_base];
    int tip = raw_petals[index][petal_pad_tip];
    int cw = raw_petals[index][petal_pad_cw];
    int ccw = raw_petals[index][petal_pad_ccw];
    bool print_data = pressed;
    bool print_data_prev = pressed_prev;
    if (index == (FLOW3R_BSP_CAPTOUCH_DEBUG_PETAL)) {
        if (print_data) {
            if (!print_data_prev)
                printf("data = [\n    # cw, ccw/tip, base, rad, phi\n");
            if (!print_petal_counter) {
                if (top) {
                    printf("    (%d, %d, ", cw, ccw);
                } else {
                    printf("    (0, %d, ", tip);
                }
                printf("%d, %d, %d", base, rad, phi);
#ifdef FLOW3R_BSP_CAPTOUCH_SECONDARY_OUTPUT
                printf(", %d, %d", rad2, phi2);
#endif
                printf("),\n");
            }
            print_petal_counter++;
            print_petal_counter %= 6;
        } else if (print_data_prev) {
            printf("]\n");
            print_petal_counter = 0;
        }
    }
#endif

    if (_on_data) _on_data(index, mode, pressed, timestamp, rad, phi, raw_sum);

    if (mode > 1) {
        // filter settings
        const int32_t f_div_pow = 4;
        const int32_t f_mult_old = 9;
        const int32_t f_mult_new = (1 << f_div_pow) - f_mult_old;

        if (reset_filters) {
            if (top) {
                top_petal_filter[index] = rad;
                top_petal_filter[index + 1] = phi;
            }
        } else {
            if (top) {
                top_petal_filter[index] =
                    (f_mult_old * top_petal_filter[index] + f_mult_new * rad) >>
                    f_div_pow;
                top_petal_filter[index + 1] =
                    (f_mult_old * top_petal_filter[index + 1] +
                     f_mult_new * phi) >>
                    f_div_pow;
                rad = (f_mult_old * petal->rad +
                       f_mult_new * top_petal_filter[index]) >>
                      f_div_pow;
                phi = (f_mult_old * petal->phi +
                       f_mult_new * top_petal_filter[index + 1]) >>
                      f_div_pow;
            } else {
                rad = (f_mult_old * petal->rad + f_mult_new * rad) >> f_div_pow;
            }
        }

#ifdef FLOW3R_BSP_CAPTOUCH_SECONDARY_OUTPUT
        if (reset_filters) {
            if (top) {
                top_petal_filter2[index] = rad2;
                top_petal_filter2[index + 1] = phi2;
            }
        } else {
            if (top) {
                top_petal_filter2[index] =
                    (f_mult_old * top_petal_filter2[index] +
                     f_mult_new * rad2) >>
                    f_div_pow;
                top_petal_filter2[index + 1] =
                    (f_mult_old * top_petal_filter2[index + 1] +
                     f_mult_new * phi2) >>
                    f_div_pow;
                rad2 = (f_mult_old * petal->rad2 +
                        f_mult_new * top_petal_filter2[index]) >>
                       f_div_pow;
                phi2 = (f_mult_old * petal->phi2 +
                        f_mult_new * top_petal_filter2[index + 1]) >>
                       f_div_pow;
            } else {
                rad2 =
                    (f_mult_old * petal->rad2 + f_mult_new * rad2) >> f_div_pow;
            }
        }
#endif
    }
    if (lock) xSemaphoreTake(lock, portMAX_DELAY);
    petal->mode = mode;
    petal->pressed = pressed;
    petal->coverage = raw_sum;
    petal->rad = rad;
    petal->phi = phi;
#ifdef FLOW3R_BSP_CAPTOUCH_SECONDARY_OUTPUT
    petal->rad2 = rad2;
    petal->phi2 = phi2;
#endif
    if (lock) xSemaphoreGive(lock);
}
