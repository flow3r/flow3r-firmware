#include "py/builtin.h"
#include "py/objstr.h"
#include "py/parsenum.h"
#include "py/runtime.h"
#include "py/smallint.h"

#include "esp_timer.h"
#include "flow3r_bsp_captouch.h"

#include <math.h>
#include <string.h>

// :/
typedef struct _mp_obj_complex_t {
    mp_obj_base_t base;
    mp_float_t real;
    mp_float_t imag;
} mp_obj_complex_t;

#define LIST_MIN_ALLOC 4

// this looks like the most useless optimization, this is not about caching tho:
// we wanna output data to micropython in floats but!! if we do the conversion
// in the cap touch driver task it can no longer jump between cores and that
// would make us very sad
// so we put the floating point mafh in the micropython task :D
// also we n-e-v-e-r get to use unions and they're so cool :3
typedef union {
    float post;
    int32_t pre;
} processable;

typedef struct {
    processable rad;
    processable phi;
    processable coverage;
    mp_uint_t timestamp;
    uint8_t petal : 4;
    uint8_t mode : 4;
    bool pressed : 1;
    bool processed : 1;
} log_frame_data_t;

typedef struct _log_frame_data_ll_t {
    log_frame_data_t frame;
    struct _log_frame_data_ll_t *next;
} log_frame_data_ll_t;

typedef struct {
    mp_obj_base_t base;
    log_frame_data_t data;
} mp_captouch_petal_log_frame_t;

const mp_obj_type_t mp_type_captouch_petal_log_frame;

static bool calib_trusted = false;

// argument: the fastest we can go is like 0.75ms per frame if each chip only
// scans a single pad. we wanna avoid overflowing between thinks and garbage
// collection can be like 50ms (overflowing is somewhat graceish tho we're just
// dropping data) so a total worst case buffer length of 75ms seems reasonable
#define LOG_MAX_LEN 100

typedef struct {
    log_frame_data_ll_t *start;  // pointer to first (earliest) element in ll
    log_frame_data_ll_t *end;    // pointer to last (latest) element in ll
    int len;                     // number of elements in log
    SemaphoreHandle_t lock;      // lock for log

    // when we kick the latch with refresh_events we buffer the log slice for
    // the next captouch_read here:
    log_frame_data_ll_t *latch_buffer_start;
    int latch_buffer_len;
    SemaphoreHandle_t latch_buffer_lock;
} petal_log_buffer_t;

static uint32_t refresh_time;
static petal_log_buffer_t petal_log[10] = {
    0,
};
static bool petal_log_initialized = false;
static bool petal_log_active[10];

static void _add_entry_to_log(uint8_t petal, uint8_t mode, bool pressed,
                              uint32_t timestamp, int32_t rad, int32_t phi,
                              int32_t coverage) {
    if (petal >= 10) return;
    if (!petal_log_active[petal]) return;
    // TODO: can't m_new_obj here naively bc different task, fails in
    // gc_alloc/mp_thread_get_state there's gotta be a lock, else _threads
    // wouldn't work, will look into this sometime soon.
    log_frame_data_ll_t *frame_ll = malloc(sizeof(log_frame_data_ll_t));
    if (frame_ll == NULL) {
        printf("captouch frame init error");
        return;
    }
    frame_ll->next = NULL;

    log_frame_data_t *frame = &(frame_ll->frame);
    frame->mode = mode;
    frame->petal = petal;
    frame->processed = false;
    frame->pressed = pressed;
    frame->timestamp = timestamp & (MICROPY_PY_UTIME_TICKS_PERIOD - 1);
    frame->rad.pre = rad;
    frame->phi.pre = phi;
    frame->coverage.pre = coverage;

    // insert into linked list
    petal_log_buffer_t *log = &(petal_log[petal]);
    log_frame_data_ll_t *runaway = NULL;
    xSemaphoreTake(log->lock, portMAX_DELAY);

    if (log->len >= LOG_MAX_LEN) {
        runaway = log->start;
        log->start = log->start->next;
        log->len--;
    }

    if (log->len == 0) {
        assert(log->start == NULL);
        assert(log->end == NULL);
        log->start = frame_ll;
        log->end = frame_ll;
    } else {
        log->end->next = frame_ll;
    }
    log->len++;
    log->end = frame_ll;

    xSemaphoreGive(log->lock);
    free(runaway);
}

static void _kick_log_latch(uint8_t petal) {
    petal_log_buffer_t *log = &(petal_log[petal]);
    if (log->lock == NULL) return;

    xSemaphoreTake(log->lock, portMAX_DELAY);
    int len = log->len;
    log_frame_data_ll_t *start = log->start;
    log->len = 0;
    log->start = NULL;
    log->end = NULL;
    xSemaphoreGive(log->lock);

    xSemaphoreTake(log->latch_buffer_lock, portMAX_DELAY);
    log_frame_data_ll_t *runaway = log->latch_buffer_start;
    log->latch_buffer_len = len;
    log->latch_buffer_start = start;
    xSemaphoreGive(log->latch_buffer_lock);
    while (runaway) {
        log_frame_data_ll_t *runaway_next = runaway->next;
        free(runaway);
        runaway = runaway_next;
    }
}

static mp_obj_t _get_list_from_log(uint8_t petal) {
    petal_log_buffer_t *log = &(petal_log[petal]);

    mp_obj_list_t *o = m_new_obj(mp_obj_list_t);
    if (log->lock == NULL) {
        mp_obj_list_init(o, 0);
        return MP_OBJ_FROM_PTR(o);
    }

    xSemaphoreTake(log->latch_buffer_lock, portMAX_DELAY);

    const int len = log->latch_buffer_len;  // len 0 ok
    log_frame_data_ll_t *frame_ll = log->latch_buffer_start;

    mp_obj_list_init(o, len);
    log_frame_data_ll_t *del[len];
    mp_captouch_petal_log_frame_t *frames[len];

    for (int i = 0; i < len; i++) {
        assert(frame_ll);
        frames[i] = m_new(mp_captouch_petal_log_frame_t, 1);
        frames[i]->base.type = &mp_type_captouch_petal_log_frame;
        memcpy(&(frames[i]->data), &(frame_ll->frame),
               sizeof(log_frame_data_t));
        del[i] = frame_ll;
        frame_ll = frame_ll->next;
    }
    assert(!frame_ll);

    log->latch_buffer_start = NULL;
    log->latch_buffer_len = 0;
    xSemaphoreGive(log->latch_buffer_lock);

    for (int i = 0; i < len; i++) {
        o->items[i] = MP_OBJ_FROM_PTR(frames[i]);
        free(del[i]);
    }
    return MP_OBJ_FROM_PTR(o);
}

static void _initialize_log() {
    if (petal_log_initialized) return;
    for (size_t i = 0; i < 10; i++) {
        petal_log_buffer_t *log = &(petal_log[i]);
        assert(log->lock == NULL);
        assert(log->latch_buffer_lock == NULL);
        log->lock = xSemaphoreCreateMutex();
        assert(log->lock != NULL);
        log->latch_buffer_lock = xSemaphoreCreateMutex();
        assert(log->latch_buffer_lock != NULL);
    }
    flow3r_bsp_captouch_set_data_callback(_add_entry_to_log);
    petal_log_initialized = true;
}

typedef struct {
    mp_obj_base_t base;
    mp_obj_t captouch;
    mp_obj_t log;
    size_t ix;
} mp_captouch_petal_state_t;

const mp_obj_type_t captouch_petal_state_type;

typedef struct {
    mp_obj_base_t base;
    mp_obj_t petals;
    uint32_t timestamp;
    flow3r_bsp_captouch_data_t underlying;
} mp_captouch_state_t;

const mp_obj_type_t captouch_state_type;

static inline void log_frame_process_data(log_frame_data_t *data) {
    if (!data->processed) {
        data->rad.post = data->rad.pre / FLOW3R_BSP_CAPTOUCH_RAD_UNIT;
        data->phi.post = data->phi.pre / FLOW3R_BSP_CAPTOUCH_PHI_UNIT;
        data->coverage.post =
            data->coverage.pre / FLOW3R_BSP_CAPTOUCH_COVERAGE_UNIT;
        data->processed = true;
    }
}

STATIC void mp_captouch_petal_log_frame_attr(mp_obj_t self_in, qstr attr,
                                             mp_obj_t *dest) {
    mp_captouch_petal_log_frame_t *self = MP_OBJ_TO_PTR(self_in);
    if (dest[0] != MP_OBJ_NULL) {
        return;
    }
    log_frame_data_t *data = &self->data;
    log_frame_process_data(data);

    switch (attr) {
        case MP_QSTR_raw_pos:
            dest[0] = mp_obj_new_complex(data->rad.post, data->phi.post);
            break;
        case MP_QSTR_pos:
            if (data->pressed && data->mode > 1) {
                dest[0] = mp_obj_new_complex(data->rad.post, data->phi.post);
            } else {
                dest[0] = mp_const_none;
            }
            break;
        case MP_QSTR_raw_cap:
            dest[0] = mp_obj_new_float(data->coverage.post);
            break;
        case MP_QSTR_pressed:
            dest[0] = mp_obj_new_bool(data->pressed);
            break;
        case MP_QSTR_mode:
            dest[0] = mp_obj_new_int(data->mode);
            break;
        case MP_QSTR_ticks_us:
            dest[0] = MP_OBJ_NEW_SMALL_INT(data->timestamp);
            break;
    }
}

STATIC mp_obj_t mp_captouch_petal_log_frame_make_new(
    const mp_obj_type_t *type_in, size_t n_args, size_t n_kw,
    const mp_obj_t *args) {
    mp_arg_check_num(n_args, n_kw, 2, 4, false);
    // args: pressed, position, timestamp (optional, fallback: present time),
    // coverage (optional, fallback: 5 if pressed else 0)

    mp_captouch_petal_log_frame_t *self =
        m_new_obj(mp_captouch_petal_log_frame_t);
    self->base.type = type_in;
    log_frame_data_t *data = &self->data;
    data->processed = true;

    data->pressed = mp_obj_is_true(args[0]);

    if (mp_obj_is_type(args[1], &mp_type_complex)) {
        mp_obj_complex_t *position = MP_OBJ_TO_PTR(args[1]);
        data->rad.post = position->real;
        data->phi.post = position->imag;
    } else {
        data->rad.post = mp_obj_get_float(args[1]);
        data->phi.post = 0;
    }

    if (n_args > 2) {
        data->timestamp = MP_OBJ_SMALL_INT_VALUE(args[2]);
    } else {
        data->timestamp =
            esp_timer_get_time() & (MICROPY_PY_UTIME_TICKS_PERIOD - 1);
    }

    if (n_args > 3) {
        data->coverage.post = mp_obj_get_float(args[3]);
    } else {
        data->coverage.post = data->pressed ? 5. : 0.;
    }

    return MP_OBJ_FROM_PTR(self);
}

STATIC void mp_captouch_petal_state_attr(mp_obj_t self_in, qstr attr,
                                         mp_obj_t *dest) {
    mp_captouch_petal_state_t *self = MP_OBJ_TO_PTR(self_in);
    if (dest[0] != MP_OBJ_NULL) {
        return;
    }

    mp_captouch_state_t *captouch = MP_OBJ_TO_PTR(self->captouch);
    flow3r_bsp_captouch_petal_data_t *state =
        &captouch->underlying.petals[self->ix];

    bool top = (self->ix % 2) == 0;

    switch (attr) {
        case MP_QSTR_log: {
            mp_obj_list_t *log = MP_OBJ_TO_PTR(self->log);
            dest[0] = mp_obj_new_tuple(log->len, log->items);
        } break;
        case MP_QSTR_top:
            dest[0] = mp_obj_new_bool(top);
            break;
        case MP_QSTR_bottom:
            dest[0] = mp_obj_new_bool(!top);
            break;
        case MP_QSTR_pressed:
            dest[0] = mp_obj_new_bool(state->press_event);
            break;
        case MP_QSTR_raw_cap:
            dest[0] = mp_obj_new_float(state->coverage /
                                       FLOW3R_BSP_CAPTOUCH_COVERAGE_UNIT);
            break;
        case MP_QSTR_raw_pos:
            dest[0] =
                mp_obj_new_complex(state->rad / FLOW3R_BSP_CAPTOUCH_RAD_UNIT,
                                   state->phi / FLOW3R_BSP_CAPTOUCH_PHI_UNIT);
            break;
        case MP_QSTR_pos:
            if (state->press_event && state->mode > 1) {
                dest[0] = mp_obj_new_complex(
                    state->rad / FLOW3R_BSP_CAPTOUCH_RAD_UNIT,
                    state->phi / FLOW3R_BSP_CAPTOUCH_PHI_UNIT);
            } else {
                dest[0] = mp_const_none;
            }
            break;
#ifdef FLOW3R_BSP_CAPTOUCH_SECONDARY_OUTPUT
        case MP_QSTR__pos2:
            if (state->press_event && state->mode > 1) {
                dest[0] = mp_obj_new_complex(
                    state->rad2 / FLOW3R_BSP_CAPTOUCH_RAD_UNIT,
                    state->phi2 / FLOW3R_BSP_CAPTOUCH_PHI_UNIT);
            } else {
                dest[0] = mp_const_none;
            }
            break;
#endif
        case MP_QSTR_pressure:
            // legacy
            if (state->press_event) {
                dest[0] = mp_obj_new_int(state->coverage);
            } else {
                dest[0] = mp_obj_new_int(0);
            }
            break;
        case MP_QSTR_position:
            // legacy
            {
                int32_t ret[2];
                ret[0] = state->rad;
                ret[1] = state->phi;
                mp_obj_t items[2];
                // bitshift better than division here due to uneven spacing when
                // crossing 0 fix manually when changing RAD_UNIT/PHI_UNIT
                if (top) {
                    ret[0] = (int32_t)((35000 * ret[0]) >> 14);
                    ret[1] = (int32_t)((35000 * ret[1]) >> 14);
                } else {
                    ret[0] = (int32_t)(((25000 * ret[0]) >> 14) + 5000);
                    ret[1] = 0;
                }
                items[0] = mp_obj_new_int(ret[0]);
                items[1] = mp_obj_new_int(ret[1]);
                dest[0] = mp_obj_new_tuple(2, items);
            }
            break;
    }
}

STATIC void mp_captouch_state_attr(mp_obj_t self_in, qstr attr,
                                   mp_obj_t *dest) {
    mp_captouch_state_t *self = MP_OBJ_TO_PTR(self_in);
    if (dest[0] != MP_OBJ_NULL) {
        return;
    }

    switch (attr) {
        case MP_QSTR_petals:
            dest[0] = self->petals;
            break;
        case MP_QSTR_ticks_us:
            dest[0] = MP_OBJ_NEW_SMALL_INT(self->timestamp);
            break;
    }
}

MP_DEFINE_CONST_OBJ_TYPE(mp_type_captouch_petal_log_frame,
                         MP_QSTR_PetalLogFrame, MP_TYPE_FLAG_NONE, attr,
                         mp_captouch_petal_log_frame_attr, make_new,
                         mp_captouch_petal_log_frame_make_new);

MP_DEFINE_CONST_OBJ_TYPE(captouch_petal_state_type, MP_QSTR_CaptouchPetalState,
                         MP_TYPE_FLAG_NONE, attr, mp_captouch_petal_state_attr);

MP_DEFINE_CONST_OBJ_TYPE(captouch_state_type, MP_QSTR_CaptouchState,
                         MP_TYPE_FLAG_NONE, attr, mp_captouch_state_attr);

STATIC mp_obj_t
mp_captouch_state_new(const flow3r_bsp_captouch_data_t *underlying) {
    mp_captouch_state_t *captouch = m_new_obj(mp_captouch_state_t);
    captouch->timestamp = refresh_time;
    captouch->base.type = &captouch_state_type;
    memcpy(&captouch->underlying, underlying,
           sizeof(flow3r_bsp_captouch_data_t));

    mp_obj_t petals[10];
    for (int i = 0; i < 10; i++) {
        mp_captouch_petal_state_t *petal = m_new_obj(mp_captouch_petal_state_t);
        petal->base.type = &captouch_petal_state_type;
        petal->captouch = MP_OBJ_FROM_PTR(captouch);
        petal->ix = i;
        petal->log = _get_list_from_log(i);
        petals[i] = MP_OBJ_FROM_PTR(petal);
    }
    captouch->petals = mp_obj_new_tuple(10, petals);

    return MP_OBJ_FROM_PTR(captouch);
}

STATIC mp_obj_t mp_captouch_read(void) {
    flow3r_bsp_captouch_data_t dat;
    flow3r_bsp_captouch_get(&dat);
    return mp_captouch_state_new(&dat);
}

STATIC MP_DEFINE_CONST_FUN_OBJ_0(mp_captouch_read_obj, mp_captouch_read);

STATIC mp_obj_t mp_captouch_refresh_events(void) {
    for (int i = 0; i < 10; i++) {
        _kick_log_latch(i);
    }
    refresh_time = esp_timer_get_time() & (MICROPY_PY_UTIME_TICKS_PERIOD - 1);
    flow3r_bsp_captouch_refresh_events();
    return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_0(mp_captouch_refresh_events_obj,
                                 mp_captouch_refresh_events);

STATIC mp_obj_t mp_captouch_calibration_get_trusted(void) {
    return mp_obj_new_bool(calib_trusted);
}
STATIC MP_DEFINE_CONST_FUN_OBJ_0(mp_captouch_calibration_get_trusted_obj,
                                 mp_captouch_calibration_get_trusted);

STATIC mp_obj_t mp_captouch_calibration_set_trusted(mp_obj_t mp_data) {
    calib_trusted = mp_obj_is_true(mp_data);
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(mp_captouch_calibration_set_trusted_obj,
                                 mp_captouch_calibration_set_trusted);

STATIC mp_obj_t mp_captouch_calibration_active(void) {
    return mp_obj_new_int(flow3r_bsp_captouch_calibrating());
}
STATIC MP_DEFINE_CONST_FUN_OBJ_0(mp_captouch_calibration_active_obj,
                                 mp_captouch_calibration_active);

STATIC mp_obj_t mp_captouch_calibration_request(void) {
    flow3r_bsp_captouch_calibration_request();
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_0(mp_captouch_calibration_request_obj,
                                 mp_captouch_calibration_request);

static int get_calibration_index(int petal, int num_dimensions,
                                 int dimension_index) {
    int index = petal * 6;
    index += ((num_dimensions + 1) * num_dimensions) / 2;
    index += dimension_index;
    index *= 2;
    if (index >= 120)
        mp_raise_ValueError(
            MP_ERROR_TEXT("target index out of range (our fault)"));
    return index;
}

STATIC mp_obj_t mp_captouch_calibration_get_data(void) {
    int32_t data[120];
    if (!flow3r_bsp_captouch_get_calibration_data(data)) return mp_const_none;

    mp_obj_t petal_dicts[10];
    mp_obj_t items[3];
    mp_obj_t strings[4] = { mp_obj_new_str("0d", 2), mp_obj_new_str("1d", 2),
                            mp_obj_new_str("2d", 2),
                            mp_obj_new_str("petal", 5) };

    for (int petal = 0; petal < 10; petal++) {
        petal_dicts[petal] =
            mp_obj_dict_make_new(&mp_type_ordereddict, 0, 0, 0);
        mp_obj_dict_store(petal_dicts[petal], strings[3],
                          mp_obj_new_int(petal));
        for (int ndim = 0; ndim < (3 - (petal & 1)); ndim++) {
            for (int dim = 0; dim <= ndim; dim++) {
                int index = get_calibration_index(petal, ndim, dim);
                char buf[25];  // max len of 32bit int decimal is 11 chars
                int trunc = snprintf(buf, 25, "%d/%d", (int)data[index],
                                     (int)data[index + 1]);
                assert(trunc < 25);
                items[dim] = mp_obj_new_str(buf, trunc);
            }
            mp_obj_dict_store(petal_dicts[petal], strings[ndim],
                              mp_obj_new_tuple(ndim + 1, items));
        }
    }
    mp_obj_t ret = mp_obj_dict_make_new(&mp_type_ordereddict, 0, 0, 0);
    mp_obj_dict_store(ret, mp_obj_new_str("ad7147", 6),
                      mp_obj_new_tuple(10, petal_dicts));
    return ret;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_0(mp_captouch_calibration_get_data_obj,
                                 mp_captouch_calibration_get_data);

static bool parse_calibration_dict(mp_obj_t mp_data, int32_t *data) {
    if (!mp_obj_is_dict_or_ordereddict(mp_data)) return false;
    mp_map_elem_t *elem;
    mp_obj_t len;
    mp_obj_dict_t *calib_dict = MP_OBJ_TO_PTR(mp_data);
    elem = mp_map_lookup(&calib_dict->map, mp_obj_new_str("ad7147", 6),
                         MP_MAP_LOOKUP);
    if (!elem) return false;
    mp_obj_t bias_tuple = elem->value;
    len = mp_obj_len_maybe(bias_tuple);
    if ((len == MP_OBJ_NULL) || (mp_obj_get_int(len) != 10)) return false;
    mp_obj_t strings[4] = { mp_obj_new_str("0d", 2), mp_obj_new_str("1d", 2),
                            mp_obj_new_str("2d", 2),
                            mp_obj_new_str("petal", 5) };

    uint16_t petals_scanned = 0;
    for (int index = 0; index < 10; index++) {
        mp_obj_t petal_obj =
            mp_obj_subscr(bias_tuple, mp_obj_new_int(index), MP_OBJ_SENTINEL);
        if (!mp_obj_is_dict_or_ordereddict(petal_obj)) return false;
        mp_obj_dict_t *petal_dict = MP_OBJ_TO_PTR(petal_obj);
        elem = mp_map_lookup(&petal_dict->map, strings[3], MP_MAP_LOOKUP);
        int petal = elem ? mp_obj_get_int(elem->value) : index;
        if (petal > 9 || petal < 0)
            mp_raise_ValueError(MP_ERROR_TEXT("petal index out of range"));
        if (petals_scanned & 1 << petal)
            mp_raise_ValueError(MP_ERROR_TEXT("petal index duplicate"));
        petals_scanned |= 1 << petal;

        for (int ndim = 0; ndim < (3 - (petal & 1)); ndim++) {
            elem =
                mp_map_lookup(&petal_dict->map, strings[ndim], MP_MAP_LOOKUP);
            if (!elem) return false;
            mp_obj_t calib_tuple = elem->value;
            len = mp_obj_len_maybe(calib_tuple);
            if ((len == MP_OBJ_NULL) || (mp_obj_get_int(len) != ndim + 1))
                return false;
            for (int dim = 0; dim <= ndim; dim++) {
                int index = get_calibration_index(petal, ndim, dim);
                mp_obj_t calib_str[2] = { mp_obj_subscr(calib_tuple,
                                                        mp_obj_new_int(dim),
                                                        MP_OBJ_SENTINEL),
                                          mp_obj_new_str("/", 1) };
                mp_obj_t calib_pt = mp_obj_str_split(2, calib_str);
                for (int data_index = 0; data_index < 2; data_index++) {
                    size_t l;
                    const char *s = mp_obj_str_get_data(
                        mp_obj_subscr(calib_pt, mp_obj_new_int(data_index),
                                      MP_OBJ_SENTINEL),
                        &l);
                    data[index + data_index] =
                        mp_obj_get_int(mp_parse_num_integer(s, l, 0, NULL));
                }
                if (data[index] > UINT16_MAX || data[index] < 0 ||
                    data[index + 1] > 126 || data[index + 1] < 0) {
                    mp_raise_ValueError(
                        MP_ERROR_TEXT("calibration data out of range"));
                    return false;  // never reached bc __attribute__((noreturn))
                                   // but leaving it to make clear it doesn't
                                   // return true
                }
            }
        }
    }
    return true;
}

STATIC mp_obj_t mp_captouch_calibration_set_data(mp_obj_t mp_data) {
    int32_t data[120];
    if (parse_calibration_dict(mp_data, data)) {
        flow3r_bsp_captouch_set_calibration_data(data);
        return mp_const_none;
    }
    mp_raise_TypeError(MP_ERROR_TEXT("calibration data malformed"));
}

STATIC MP_DEFINE_CONST_FUN_OBJ_1(mp_captouch_calibration_set_data_obj,
                                 mp_captouch_calibration_set_data);

STATIC const MP_DEFINE_STR_OBJ(mp_captouch_calibration_path_obj,
                               "/flash/captouch_calib.json");

STATIC const mp_rom_map_elem_t sys_globals_table[] = {
    { MP_ROM_QSTR(MP_QSTR_read), MP_ROM_PTR(&mp_captouch_read_obj) },
    { MP_ROM_QSTR(MP_QSTR_refresh_events),
      MP_ROM_PTR(&mp_captouch_refresh_events_obj) },
    { MP_ROM_QSTR(MP_QSTR_calibration_path),
      MP_ROM_PTR(&mp_captouch_calibration_path_obj) },
    { MP_ROM_QSTR(MP_QSTR_calibration_request),
      MP_ROM_PTR(&mp_captouch_calibration_request_obj) },
    { MP_ROM_QSTR(MP_QSTR_calibration_active),
      MP_ROM_PTR(&mp_captouch_calibration_active_obj) },
    { MP_ROM_QSTR(MP_QSTR_calibration_set_trusted),
      MP_ROM_PTR(&mp_captouch_calibration_set_trusted_obj) },
    { MP_ROM_QSTR(MP_QSTR_calibration_get_trusted),
      MP_ROM_PTR(&mp_captouch_calibration_get_trusted_obj) },
    { MP_ROM_QSTR(MP_QSTR_calibration_set_data),
      MP_ROM_PTR(&mp_captouch_calibration_set_data_obj) },
    { MP_ROM_QSTR(MP_QSTR_calibration_get_data),
      MP_ROM_PTR(&mp_captouch_calibration_get_data_obj) },
};

STATIC MP_DEFINE_CONST_DICT(sys_globals, sys_globals_table);

const mp_obj_module_t mp_module_sys_captouch_user_cmodule = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t *)&sys_globals,
};

MP_REGISTER_MODULE(MP_QSTR_sys_captouch, mp_module_sys_captouch_user_cmodule);

typedef struct {
    mp_obj_base_t base;
    int8_t index;
    int8_t mode;
    int8_t logging;
} captouch_petal_config_obj_t;

typedef struct {
    mp_obj_base_t base;
    mp_obj_t petals;
} captouch_config_obj_t;

static int _check_petal_mode(int mode, int petal) {
    if (petal & 1) {
        if ((mode < 0) || (mode > 2)) {
            mp_raise_ValueError(MP_ERROR_TEXT(
                "petal mode not allowed (allowed modes: 0, 1, 2)"));
        }
    } else if (petal == 4 || petal == 6) {
        if ((mode < 0) || (mode > 3)) {
            mp_raise_ValueError(MP_ERROR_TEXT(
                "petal mode not allowed (allowed modes: 0, 1, 2, 3)"));
        }
    } else {
        if ((mode != 0) && (mode != 3)) {
            mp_raise_ValueError(
                MP_ERROR_TEXT("petal mode not allowed (allowed modes: 0, 3)"));
        }
    }
    return mode;
}

static int _limit_petal_mode(int mode, int petal) {
    if (petal & 1) {
        return mode > 2 ? 2 : (mode < 0 ? 0 : mode);
    } else if (petal == 4 || petal == 6) {
        return mode > 3 ? 3 : (mode < 0 ? 0 : mode);
    } else {
        return mode > 0 ? 3 : 0;
    }
}

STATIC void captouch_petal_config_attr(mp_obj_t self_in, qstr attr,
                                       mp_obj_t *dest) {
    captouch_petal_config_obj_t *self = MP_OBJ_TO_PTR(self_in);
    if (dest[0] != MP_OBJ_NULL) {
        if (attr == MP_QSTR_mode) {
            self->mode =
                _check_petal_mode(mp_obj_get_int(dest[1]), self->index);
            dest[0] = MP_OBJ_NULL;
        } else if (attr == MP_QSTR_logging) {
            self->logging = mp_obj_is_true(dest[1]);
            dest[0] = MP_OBJ_NULL;
        }
    } else {
        if (attr == MP_QSTR_mode) {
            dest[0] = mp_obj_new_int(self->mode);
        } else if (attr == MP_QSTR_logging) {
            dest[0] = self->logging ? mp_const_true : mp_const_false;
        } else {
            dest[1] = MP_OBJ_SENTINEL;
        }
    }
}

STATIC mp_obj_t captouch_petal_config_set_default(mp_obj_t self_in) {
    captouch_petal_config_obj_t *self = MP_OBJ_TO_PTR(self_in);
    self->mode = _limit_petal_mode(3, self->index);
    self->logging = false;
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_1(captouch_petal_config_set_default_obj,
                          captouch_petal_config_set_default);

STATIC mp_obj_t captouch_petal_config_clear(mp_obj_t self_in) {
    captouch_petal_config_obj_t *self = MP_OBJ_TO_PTR(self_in);
    self->mode = 0;
    self->logging = false;
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_1(captouch_petal_config_clear_obj,
                          captouch_petal_config_clear);

STATIC mp_obj_t captouch_petal_config_set_min_mode(mp_obj_t self_in,
                                                   mp_obj_t mp_mode) {
    captouch_petal_config_obj_t *self = MP_OBJ_TO_PTR(self_in);
    int mode = _limit_petal_mode(mp_obj_get_int(mp_mode), self->index);
    self->mode = mode > self->mode ? mode : self->mode;
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_2(captouch_petal_config_set_min_mode_obj,
                          captouch_petal_config_set_min_mode);

STATIC const mp_rom_map_elem_t captouch_petal_config_locals_dict_table[] = {
    { MP_ROM_QSTR(MP_QSTR_set_default),
      MP_ROM_PTR(&captouch_petal_config_set_default_obj) },
    { MP_ROM_QSTR(MP_QSTR_clear),
      MP_ROM_PTR(&captouch_petal_config_clear_obj) },
    { MP_ROM_QSTR(MP_QSTR_set_min_mode),
      MP_ROM_PTR(&captouch_petal_config_set_min_mode_obj) },
};

STATIC MP_DEFINE_CONST_DICT(captouch_petal_config_locals_dict,
                            captouch_petal_config_locals_dict_table);

MP_DEFINE_CONST_OBJ_TYPE(mp_type_captouch_petal_config, MP_QSTR_PetalConfig,
                         MP_TYPE_FLAG_NONE, locals_dict,
                         &captouch_petal_config_locals_dict, attr,
                         captouch_petal_config_attr);

static mp_obj_t captouch_config_new(int mode, mp_obj_type_t *type) {
    captouch_config_obj_t *self = m_new_obj(captouch_config_obj_t);
    self->base.type = type;
    mp_obj_t petals[10];

    flow3r_bsp_captouch_petal_mode_t mode_data[10];
    if (mode > 1) flow3r_bsp_captouch_get_petal_modes(mode_data);
    for (int petal = 0; petal < 10; petal++) {
        captouch_petal_config_obj_t *petalconf =
            m_new_obj(captouch_petal_config_obj_t);
        petalconf->base.type = &mp_type_captouch_petal_config;
        petalconf->index = petal;
        if (mode > 1) {
            petalconf->logging = petal_log_active[petal];
            petalconf->mode = mode_data[petal];
        } else {
            petalconf->logging = false;
            petalconf->mode = mode ? 3 - (petal & 1) : 0;
        }
        petals[petal] = MP_OBJ_FROM_PTR(petalconf);
    }
    self->petals = mp_obj_new_tuple(10, petals);
    return MP_OBJ_FROM_PTR(self);
}

STATIC mp_obj_t captouch_config_empty(mp_obj_t type_in) {
    return captouch_config_new(0, MP_OBJ_TO_PTR(type_in));
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(captouch_config_empty_fun_obj,
                                 captouch_config_empty);
STATIC MP_DEFINE_CONST_CLASSMETHOD_OBJ(
    captouch_config_empty_obj, MP_ROM_PTR(&captouch_config_empty_fun_obj));

STATIC mp_obj_t captouch_config_default(mp_obj_t type_in) {
    return captouch_config_new(1, MP_OBJ_TO_PTR(type_in));
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(captouch_config_default_fun_obj,
                                 captouch_config_default);
STATIC MP_DEFINE_CONST_CLASSMETHOD_OBJ(
    captouch_config_default_obj, MP_ROM_PTR(&captouch_config_default_fun_obj));

STATIC mp_obj_t captouch_config_current(mp_obj_t type_in) {
    return captouch_config_new(2, MP_OBJ_TO_PTR(type_in));
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(captouch_config_current_fun_obj,
                                 captouch_config_current);
STATIC MP_DEFINE_CONST_CLASSMETHOD_OBJ(
    captouch_config_current_obj, MP_ROM_PTR(&captouch_config_current_fun_obj));

STATIC void captouch_config_attr(mp_obj_t self_in, qstr attr, mp_obj_t *dest) {
    if (dest[0] != MP_OBJ_NULL) {
        return;
    }
    if (attr == MP_QSTR_petals) {
        captouch_config_obj_t *self = MP_OBJ_TO_PTR(self_in);
        dest[0] = self->petals;
    } else {
        dest[1] = MP_OBJ_SENTINEL;
    }
}

static void captouch_config_verify(captouch_config_obj_t *self) {
    for (int petal = 0; petal < 10; petal++) {
        mp_obj_t petalconf_obj =
            mp_obj_subscr(self->petals, mp_obj_new_int(petal), MP_OBJ_SENTINEL);
        if (!mp_obj_is_type(petalconf_obj, &mp_type_captouch_petal_config))
            mp_raise_TypeError(MP_ERROR_TEXT("petal data malformed"));
    }
}

STATIC mp_obj_t captouch_config_apply(mp_obj_t self_in) {
    _initialize_log();
    captouch_config_obj_t *self = MP_OBJ_TO_PTR(self_in);
    captouch_config_verify(self);
    flow3r_bsp_captouch_petal_mode_t mode_data[10];
    bool logging_data[10];
    for (int petal = 0; petal < 10; petal++) {
        captouch_petal_config_obj_t *petalconf = MP_OBJ_TO_PTR(mp_obj_subscr(
            self->petals, mp_obj_new_int(petal), MP_OBJ_SENTINEL));
        mode_data[petal] = _limit_petal_mode(petalconf->mode, petal);
        logging_data[petal] = petalconf->logging;
    }
    flow3r_bsp_captouch_set_petal_modes(mode_data);
    memcpy(petal_log_active, logging_data, sizeof(petal_log_active));
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_1(captouch_config_apply_obj, captouch_config_apply);

STATIC mp_obj_t captouch_config_apply_default(mp_obj_t self_in) {
    flow3r_bsp_captouch_petal_mode_t mode_data[10];
    bool logging_data[10];
    for (int petal = 0; petal < 10; petal++) {
        mode_data[petal] = _limit_petal_mode(3, petal);
        logging_data[petal] = false;
    }
    flow3r_bsp_captouch_set_petal_modes(mode_data);
    memcpy(petal_log_active, logging_data, sizeof(petal_log_active));
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_1(captouch_config_apply_default_obj,
                          captouch_config_apply_default);

// dirty hack: struct not exposed, internal to vendor/py/objcomplex.c but if we
// wanna write to ROM this is probably for the better
typedef struct {
    mp_obj_base_t base;
    mp_float_t real;
    mp_float_t imag;
} mp_obj_complex_copy_t;

STATIC const mp_obj_complex_copy_t mp_const_petal_rotors[10] = {
    { { &mp_type_complex }, (mp_float_t)0., (mp_float_t)-1. },
    { { &mp_type_complex },
      (mp_float_t)0.5877852522924731,
      (mp_float_t)-0.8090169943749475 },
    { { &mp_type_complex },
      (mp_float_t)0.9510565162951535,
      (mp_float_t)-0.3090169943749474 },
    { { &mp_type_complex },
      (mp_float_t)0.9510565162951535,
      (mp_float_t)0.3090169943749474 },
    { { &mp_type_complex },
      (mp_float_t)0.5877852522924731,
      (mp_float_t)0.8090169943749475 },
    { { &mp_type_complex }, (mp_float_t)0., (mp_float_t)1. },
    { { &mp_type_complex },
      (mp_float_t)-0.5877852522924731,
      (mp_float_t)0.8090169943749475 },
    { { &mp_type_complex },
      (mp_float_t)-0.9510565162951535,
      (mp_float_t)0.3090169943749474 },
    { { &mp_type_complex },
      (mp_float_t)-0.9510565162951535,
      (mp_float_t)-0.3090169943749474 },
    { { &mp_type_complex },
      (mp_float_t)-0.5877852522924731,
      (mp_float_t)-0.8090169943749475 },
};

STATIC const mp_rom_obj_tuple_t mp_petal_rotors_tuple_obj = {
    { &mp_type_tuple },
    10,
    {
        MP_ROM_PTR(&mp_const_petal_rotors[0]),
        MP_ROM_PTR(&mp_const_petal_rotors[1]),
        MP_ROM_PTR(&mp_const_petal_rotors[2]),
        MP_ROM_PTR(&mp_const_petal_rotors[3]),
        MP_ROM_PTR(&mp_const_petal_rotors[4]),
        MP_ROM_PTR(&mp_const_petal_rotors[5]),
        MP_ROM_PTR(&mp_const_petal_rotors[6]),
        MP_ROM_PTR(&mp_const_petal_rotors[7]),
        MP_ROM_PTR(&mp_const_petal_rotors[8]),
        MP_ROM_PTR(&mp_const_petal_rotors[9]),
    },
};

// same as with mp_obj_complex_copy_t
typedef struct {
    mp_obj_base_t base;
    mp_float_t value;
} mp_obj_float_copy_t;

STATIC const mp_obj_float_copy_t mp_const_petal_angles[10] = {
    { { &mp_type_float }, (mp_float_t)-1.570796 },
    { { &mp_type_float }, (mp_float_t)-0.9424778 },
    { { &mp_type_float }, (mp_float_t)-0.3141593 },
    { { &mp_type_float }, (mp_float_t)0.3141593 },
    { { &mp_type_float }, (mp_float_t)0.9424778 },
    { { &mp_type_float }, (mp_float_t)1.570796 },
    { { &mp_type_float }, (mp_float_t)2.199115 },
    { { &mp_type_float }, (mp_float_t)2.827433 },
    { { &mp_type_float }, (mp_float_t)-2.827433 },
    { { &mp_type_float }, (mp_float_t)-2.199115 },
};

STATIC const mp_rom_obj_tuple_t mp_petal_angles_tuple_obj = {
    { &mp_type_tuple },
    10,
    {
        MP_ROM_PTR(&mp_const_petal_angles[0]),
        MP_ROM_PTR(&mp_const_petal_angles[1]),
        MP_ROM_PTR(&mp_const_petal_angles[2]),
        MP_ROM_PTR(&mp_const_petal_angles[3]),
        MP_ROM_PTR(&mp_const_petal_angles[4]),
        MP_ROM_PTR(&mp_const_petal_angles[5]),
        MP_ROM_PTR(&mp_const_petal_angles[6]),
        MP_ROM_PTR(&mp_const_petal_angles[7]),
        MP_ROM_PTR(&mp_const_petal_angles[8]),
        MP_ROM_PTR(&mp_const_petal_angles[9]),
    },
};

STATIC const mp_rom_map_elem_t captouch_config_locals_dict_table[] = {
    { MP_ROM_QSTR(MP_QSTR_empty), MP_ROM_PTR(&captouch_config_empty_obj) },
    { MP_ROM_QSTR(MP_QSTR_default), MP_ROM_PTR(&captouch_config_default_obj) },
    { MP_ROM_QSTR(MP_QSTR_current), MP_ROM_PTR(&captouch_config_current_obj) },
    { MP_ROM_QSTR(MP_QSTR_apply), MP_ROM_PTR(&captouch_config_apply_obj) },
    { MP_ROM_QSTR(MP_QSTR_apply_default),
      MP_ROM_PTR(&captouch_config_apply_default_obj) },
};

STATIC MP_DEFINE_CONST_DICT(captouch_config_locals_dict,
                            captouch_config_locals_dict_table);

MP_DEFINE_CONST_OBJ_TYPE(mp_type_captouch_config, MP_QSTR_Config,
                         MP_TYPE_FLAG_NONE, locals_dict,
                         &captouch_config_locals_dict, attr,
                         captouch_config_attr);

typedef struct {
    mp_obj_base_t base;
    mp_obj_t frames;
    int max_len;
} captouch_petal_log_obj_t;

#define PREALLOC_LIMIT 128

static inline mp_int_t ticks_diff_inner(mp_uint_t end, mp_uint_t start) {
    return (((end - start + MICROPY_PY_UTIME_TICKS_PERIOD / 2) &
             (MICROPY_PY_UTIME_TICKS_PERIOD - 1)) -
            MICROPY_PY_UTIME_TICKS_PERIOD / 2);
}

static int list_multidel_nodealloc(mp_obj_list_t *list, int num_del,
                                   bool from_newest) {
    if (num_del <= 0) return 0;
    num_del = num_del > list->len ? list->len : num_del;
    list->len -= num_del;
    if (!from_newest)
        memmove(list->items, list->items + num_del,
                (list->len) * sizeof(mp_obj_t));
    for (int i = 0; i < num_del; i++) {
        list->items[list->len + i] = MP_OBJ_NULL;
    }
    mp_seq_clear(list->items, list->len + 1, list->alloc, sizeof(*list->items));
    return num_del;
}

static mp_captouch_petal_log_frame_t *mp_obj_to_frame_ptr(mp_obj_t frame_obj) {
    if (!mp_obj_is_type(frame_obj, &mp_type_captouch_petal_log_frame)) {
        mp_raise_TypeError(MP_ERROR_TEXT("frame must be PetalLogFrame"));
    }
    return MP_OBJ_TO_PTR(frame_obj);
}

static void log_crop(mp_obj_list_t *list, int min_frames, float min_time_ms,
                     bool from_newest) {
    int len = list->len;
    if (!len) return;
    min_frames = min_frames < 0 ? 0 : min_frames;

    if (min_time_ms > 0.) {
        mp_uint_t min_time_us = min_time_ms * 1000;
        mp_captouch_petal_log_frame_t *frame =
            mp_obj_to_frame_ptr(list->items[from_newest ? len - 1 : 0]);
        mp_uint_t ref_timestamp = frame->data.timestamp;

        for (int i = min_frames + 1; i < len; i++) {
            int index = from_newest ? len - 1 - i : i;
            frame = mp_obj_to_frame_ptr(list->items[index]);
            mp_uint_t start =
                from_newest ? frame->data.timestamp : ref_timestamp;
            mp_uint_t end = from_newest ? ref_timestamp : frame->data.timestamp;
            if (ticks_diff_inner(end, start) >= min_time_us) {
                min_frames = i - 1;
                break;
            }
        }
    }
    list_multidel_nodealloc(list, min_frames, from_newest);
}

static inline void get_slice_index(mp_obj_t mp_index, int len, int *ret) {
    if (mp_index == mp_const_none) return;
    int index = mp_obj_get_int(mp_index);
    if (index < 0) index += len;
    if (index < 0) {
        index = 0;
    } else if (index > len) {
        index = len;
    }
    *ret = index;
}

STATIC mp_obj_t captouch_petal_log_average(size_t n_args,
                                           const mp_obj_t *args) {
    captouch_petal_log_obj_t *self = MP_OBJ_TO_PTR(args[0]);
    mp_obj_list_t *frames = MP_OBJ_TO_PTR(self->frames);
    int start = 0;
    if (n_args > 1) get_slice_index(args[1], frames->len, &start);
    int stop = frames->len;
    if (n_args > 2) get_slice_index(args[2], frames->len, &stop);

    int len = stop - start;
    if (len < 1) return mp_const_none;

    float sum_real = 0;
    float sum_imag = 0;
    for (int i = start; i < stop; i++) {
        mp_captouch_petal_log_frame_t *frame =
            mp_obj_to_frame_ptr(frames->items[i]);
        log_frame_data_t *data = &frame->data;
        log_frame_process_data(data);
        sum_real += data->rad.post;
        sum_imag += data->phi.post;
    }
    return mp_obj_new_complex(sum_real / len, sum_imag / len);
}
MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(captouch_petal_log_average_obj, 1, 3,
                                    captouch_petal_log_average);

STATIC mp_obj_t captouch_petal_log_slope_per_ms(size_t n_args,
                                                const mp_obj_t *args) {
    captouch_petal_log_obj_t *self = MP_OBJ_TO_PTR(args[0]);
    mp_obj_list_t *frames = MP_OBJ_TO_PTR(self->frames);
    int start = 0;
    if (n_args > 1) get_slice_index(args[1], frames->len, &start);
    int stop = frames->len;
    if (n_args > 2) get_slice_index(args[2], frames->len, &stop);

    int len = stop - start;
    if (len < 2) return mp_const_none;

    mp_uint_t timestamp_start;
    float sum_real = 0;
    float sum_imag = 0;
    float sum_t_real = 0;
    float sum_t_imag = 0;
    float sum_t_sq = 0;
    float sum_t = 0;
    for (int i = 0; i < len; i++) {
        mp_captouch_petal_log_frame_t *frame =
            mp_obj_to_frame_ptr(frames->items[i + start]);
        log_frame_data_t *data = &frame->data;
        log_frame_process_data(data);
        sum_real += data->rad.post;
        sum_imag += data->phi.post;
        if (!i) {
            timestamp_start = data->timestamp;
            continue;
        } else {
            float timestamp =
                ticks_diff_inner(data->timestamp, timestamp_start);
            sum_t_real += data->rad.post * timestamp;
            sum_t_imag += data->phi.post * timestamp;
            sum_t_sq += timestamp * timestamp;
            sum_t += timestamp;
        }
    }
    float div = len * sum_t_sq - sum_t * sum_t;
    if (!div) return mp_const_none;
    float k_real = (len * sum_t_real - sum_real * sum_t) * 1000 / div;
    float k_imag = (len * sum_t_imag - sum_imag * sum_t) * 1000 / div;
    return mp_obj_new_complex(k_real, k_imag);
}
MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(captouch_petal_log_slope_per_ms_obj, 1, 3,
                                    captouch_petal_log_slope_per_ms);

STATIC mp_obj_t captouch_petal_log_length(mp_obj_t self_in) {
    captouch_petal_log_obj_t *self = MP_OBJ_TO_PTR(self_in);
    mp_obj_list_t *frames = MP_OBJ_TO_PTR(self->frames);
    return mp_obj_new_int(frames->len);
}
MP_DEFINE_CONST_FUN_OBJ_1(captouch_petal_log_length_obj,
                          captouch_petal_log_length);

STATIC mp_obj_t captouch_petal_log_length_ms(size_t n_args,
                                             const mp_obj_t *args) {
    captouch_petal_log_obj_t *self = MP_OBJ_TO_PTR(args[0]);
    mp_obj_list_t *frames = MP_OBJ_TO_PTR(self->frames);
    int start = 0;
    if (n_args > 1) get_slice_index(args[1], frames->len, &start);
    int stop = frames->len;
    if (n_args > 2) get_slice_index(args[2], frames->len, &stop);
    if (stop - start < 2) return mp_obj_new_float(0.);
    mp_captouch_petal_log_frame_t *oldest =
        mp_obj_to_frame_ptr(frames->items[start]);
    mp_captouch_petal_log_frame_t *newest =
        mp_obj_to_frame_ptr(frames->items[stop - 1]);
    float ret =
        ticks_diff_inner(newest->data.timestamp, oldest->data.timestamp);
    return mp_obj_new_float(ret / 1000);
}
MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(captouch_petal_log_length_ms_obj, 1, 3,
                                    captouch_petal_log_length_ms);

STATIC mp_obj_t captouch_petal_log_index_offset_ms(mp_obj_t self_in,
                                                   mp_obj_t mp_index,
                                                   mp_obj_t mp_min_offset_ms) {
    captouch_petal_log_obj_t *self = MP_OBJ_TO_PTR(self_in);
    mp_obj_list_t *frames = MP_OBJ_TO_PTR(self->frames);
    int ref_index = mp_obj_get_int(mp_index);
    if (ref_index < 0) ref_index += frames->len;
    if (ref_index >= frames->len || ref_index < 0)
        mp_raise_msg(&mp_type_IndexError, MP_ERROR_TEXT("index out of range"));
    float min_offset_ms = mp_obj_get_float(mp_min_offset_ms);
    mp_uint_t ref_timestamp =
        mp_obj_to_frame_ptr(frames->items[ref_index])->data.timestamp;

    if (!min_offset_ms) return mp_index;
    if (min_offset_ms > 0) {
        mp_int_t min_offset_us = min_offset_ms * 1000.;
        // TODO: Make a smarter search that estimates based on length_ms/length
        // and then expands linearily in whichever direction
        for (int test_index = ref_index + 1; test_index < frames->len;
             test_index++) {
            mp_uint_t test_timestamp =
                mp_obj_to_frame_ptr(frames->items[test_index])->data.timestamp;
            if (ticks_diff_inner(test_timestamp, ref_timestamp) > min_offset_us)
                return mp_obj_new_int(test_index);
        }
    } else {
        mp_int_t min_offset_us = -min_offset_ms * 1000.;
        for (int test_index = ref_index - 1; test_index >= 0; test_index--) {
            mp_uint_t test_timestamp =
                mp_obj_to_frame_ptr(frames->items[test_index])->data.timestamp;
            if (ticks_diff_inner(ref_timestamp, test_timestamp) > min_offset_us)
                return mp_obj_new_int(test_index);
        }
    }
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_3(captouch_petal_log_index_offset_ms_obj,
                          captouch_petal_log_index_offset_ms);

STATIC mp_obj_t captouch_petal_log_crop(mp_obj_t self_in, mp_obj_t mp_index) {
    if (mp_index == mp_const_none) return mp_obj_new_int(0);
    captouch_petal_log_obj_t *self = MP_OBJ_TO_PTR(self_in);
    mp_obj_list_t *frames = MP_OBJ_TO_PTR(self->frames);
    int num_del = mp_obj_get_int(mp_index);
    if (num_del < 0) num_del += frames->len;
    num_del = list_multidel_nodealloc(frames, num_del, false);
    return mp_obj_new_int(num_del);
}
MP_DEFINE_CONST_FUN_OBJ_2(captouch_petal_log_crop_obj, captouch_petal_log_crop);

STATIC mp_obj_t captouch_petal_log_clear(mp_obj_t self_in) {
    captouch_petal_log_obj_t *self = MP_OBJ_TO_PTR(self_in);
    mp_obj_list_t *frames = MP_OBJ_TO_PTR(self->frames);
    frames->len = 0;
    frames->items =
        m_renew(mp_obj_t, frames->items, frames->alloc, LIST_MIN_ALLOC);
    frames->alloc = LIST_MIN_ALLOC;
    mp_seq_clear(frames->items, 0, frames->alloc, sizeof(*frames->items));
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(captouch_petal_log_clear_obj,
                                 captouch_petal_log_clear);

STATIC mp_obj_t captouch_petal_log_append(mp_obj_t self_in,
                                          mp_obj_t frame_obj) {
    if (!mp_obj_is_type(frame_obj, &mp_type_captouch_petal_log_frame)) {
        mp_raise_TypeError(MP_ERROR_TEXT("frame must be PetalLogFrame"));
    }
    captouch_petal_log_obj_t *self = MP_OBJ_TO_PTR(self_in);
    mp_obj_list_t *frames = MP_OBJ_TO_PTR(self->frames);
    if (frames->len >= frames->alloc) {
        int new_alloc = frames->alloc * 2;
        if (self->max_len > new_alloc) new_alloc = self->max_len;
        frames->items =
            m_renew(mp_obj_t, frames->items, frames->alloc, new_alloc);
        frames->alloc = new_alloc;
        mp_seq_clear(frames->items, frames->len + 1, frames->alloc,
                     sizeof(*frames->items));
    }
    frames->items[frames->len++] = frame_obj;
    if (frames->len > self->max_len) {
        self->max_len =
            frames->len > (PREALLOC_LIMIT) ? (PREALLOC_LIMIT) : frames->len;
    }
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_2(captouch_petal_log_append_obj,
                                 captouch_petal_log_append);

STATIC void captouch_petal_log_attr(mp_obj_t self_in, qstr attr,
                                    mp_obj_t *dest) {
    captouch_petal_log_obj_t *self = MP_OBJ_TO_PTR(self_in);
    if (dest[0] != MP_OBJ_NULL) {
        if (attr == MP_QSTR_frames) {
            if (!mp_obj_is_type(dest[1], &mp_type_list))
                mp_raise_TypeError(MP_ERROR_TEXT("frames must be a list"));
            self->frames = dest[1];
            dest[0] = MP_OBJ_NULL;
        }
    } else {
        if (attr == MP_QSTR_frames) {
            dest[0] = self->frames;
        } else {
            dest[1] = MP_OBJ_SENTINEL;
        }
    }
}

STATIC mp_obj_t captouch_petal_log_make_new(const mp_obj_type_t *type_in,
                                            size_t n_args, size_t n_kw,
                                            const mp_obj_t *args) {
    mp_arg_check_num(n_args, n_kw, 0, 1, false);
    captouch_petal_log_obj_t *self = m_new_obj(captouch_petal_log_obj_t);
    self->base.type = type_in;
    if (n_args) {
        if (!mp_obj_is_type(args[0], &mp_type_list))
            mp_raise_TypeError(MP_ERROR_TEXT("frames must be a list"));
        self->frames = args[0];
        mp_obj_list_t *list = MP_OBJ_TO_PTR(self->frames);
        self->max_len = list->len;
    } else {
        self->frames = mp_obj_new_list(0, NULL);
        self->max_len = LIST_MIN_ALLOC;
    }
    return MP_OBJ_FROM_PTR(self);
}

STATIC const mp_rom_map_elem_t captouch_petal_log_locals_dict_table[] = {
    { MP_ROM_QSTR(MP_QSTR_average),
      MP_ROM_PTR(&captouch_petal_log_average_obj) },
    { MP_ROM_QSTR(MP_QSTR_slope_per_ms),
      MP_ROM_PTR(&captouch_petal_log_slope_per_ms_obj) },
    { MP_ROM_QSTR(MP_QSTR_length), MP_ROM_PTR(&captouch_petal_log_length_obj) },
    { MP_ROM_QSTR(MP_QSTR_length_ms),
      MP_ROM_PTR(&captouch_petal_log_length_ms_obj) },
    { MP_ROM_QSTR(MP_QSTR_clear), MP_ROM_PTR(&captouch_petal_log_clear_obj) },
    { MP_ROM_QSTR(MP_QSTR_crop), MP_ROM_PTR(&captouch_petal_log_crop_obj) },
    { MP_ROM_QSTR(MP_QSTR_append), MP_ROM_PTR(&captouch_petal_log_append_obj) },
    { MP_ROM_QSTR(MP_QSTR_index_offset_ms),
      MP_ROM_PTR(&captouch_petal_log_index_offset_ms_obj) },
};

STATIC MP_DEFINE_CONST_DICT(captouch_petal_log_locals_dict,
                            captouch_petal_log_locals_dict_table);

MP_DEFINE_CONST_OBJ_TYPE(mp_type_captouch_petal_log, MP_QSTR_PetalLog,
                         MP_TYPE_FLAG_NONE, locals_dict,
                         &captouch_petal_log_locals_dict, attr,
                         captouch_petal_log_attr, make_new,
                         captouch_petal_log_make_new);

STATIC const mp_rom_map_elem_t globals_table[] = {
    { MP_ROM_QSTR(MP_QSTR_Config), MP_ROM_PTR(&mp_type_captouch_config) },
    { MP_ROM_QSTR(MP_QSTR_PetalLog), MP_ROM_PTR(&mp_type_captouch_petal_log) },
    { MP_ROM_QSTR(MP_QSTR_PetalLogFrame),
      MP_ROM_PTR(&mp_type_captouch_petal_log_frame) },
    { MP_ROM_QSTR(MP_QSTR_PETAL_ROTORS),
      MP_ROM_PTR(&mp_petal_rotors_tuple_obj) },
    { MP_ROM_QSTR(MP_QSTR_PETAL_ANGLES),
      MP_ROM_PTR(&mp_petal_angles_tuple_obj) },
#ifndef FLOW3R_V2
    { MP_ROM_QSTR(MP_QSTR_read), MP_ROM_PTR(&mp_captouch_read_obj) },
#endif
};

STATIC MP_DEFINE_CONST_DICT(globals, globals_table);

const mp_obj_module_t mp_module_captouch_user_cmodule = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t *)&globals,
};

MP_REGISTER_MODULE(MP_QSTR_captouch, mp_module_captouch_user_cmodule);
