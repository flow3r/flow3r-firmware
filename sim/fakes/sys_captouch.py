def calibration_get_data():
    return [0] * 120


def calibration_set_data(val):
    pass


_trusted = False


def refresh_events():
    pass


def calibration_set_trusted(val):
    global _trusted
    _trusted = bool(val)


def calibration_get_trusted():
    return _trusted


_modes = [3, 2] * 5


def config_set_petal_modes(val):
    global _modes
    _modes = val


def config_get_petal_modes():
    return _modes


_logging = [False] * 10


def config_set_logging(val):
    global _logging
    _logging = val


def config_get_logging():
    return _logging
