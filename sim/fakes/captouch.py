from typing import List
import math, cmath

PETAL_ROTORS = [cmath.rect(1, math.tau * (x - 2.5)) for x in range(10)]


class PetalLogFrame:
    def __init__(self, ix: int, pressed: bool):
        self.mode = 2 if ix % 2 else 3
        self.pos = 0j if pressed else None
        self.raw_cap = 4 if pressed else 0
        self.raw_pos = 0j
        self.ticks_us = 0


class PetalLog:
    def __init__(self):
        self.clear()

    def append(self, frame: PetalLogFrame):
        self.frames.append(frame)
        self.length += 1
        self.length_ms += 10

    def clear(self):
        self.frames = []
        self.length = 0
        self.length_ms = 0

    def index_offset_ms(self, index: int, min_offset_ms: float):
        if index < 0:
            index += self.length
        if index >= self.length:
            index = self.length - 1
        index += int(min_offset_ms / 10)
        if index < 0:
            index = 0
        if index >= self.length:
            index = self.length - 1
        return index

    def average(start: int = None, stop: int = None):
        return 0

    def slope_per_ms(start: int = None, stop: int = None):
        return 0


class CaptouchPetalState:
    def __init__(self, ix: int, pressed: bool):
        self.pressed = pressed
        self._ix = ix
        self.position = (0, 0)
        self.pos = 0j if pressed else None
        self.raw_cap = 4 if pressed else 0
        self.raw_pos = 0j
        self.log = [CaptouchPetalLogFrame(ix, pressed)]

    @property
    def top(self) -> bool:
        return self._ix % 2 == 0

    @property
    def bottom(self) -> bool:
        return not self.top


_modes = [3 - (x % 2) for x in range(10)]
_logging = [False for x in range(10)]


class PetalConfig:
    mode = 0
    logging = False

    def set_min_mode(self, mode):
        self.mode = max(self.mode, mode)


class Config:
    def __init__(self, _token=None):
        if _token is None:
            raise ValueError("don't initialize directly")
        self.petals = [PetalConfig() for x in range(10)]
        for x in range(10):
            if _token == 1:
                self.petals[x].mode = 3 - (x % 2)
            if _token == 2:
                self.petals[x].mode = _modes[x]

    def apply(self):
        for x in range(10):
            _modes[x] = self.petals[x].mode
            _logging[x] = self.petals[x].logging

    def apply_default(self):
        for x in range(10):
            _modes[x] = 3 - (x % 2)
            _logging[x] = False

    @classmethod
    def current(cls):
        return cls(_token=2)

    @classmethod
    def default(cls):
        return cls(_token=1)

    @classmethod
    def empty(cls):
        return cls(_token=0)


class CaptouchState:
    def __init__(self, petals: List[CaptouchPetalState]):
        self._petals = petals

    @property
    def petals(self) -> List[CaptouchPetalState]:
        return self._petals


def read() -> CaptouchState:
    import _sim

    _sim._sim.process_events()
    _sim._sim.render_gui_lazy()
    petals = _sim._sim.petals

    res = []
    for petal in range(10):
        top = petal % 2 == 0
        if top:
            ccw = petals.state_for_petal_pad(petal, 1)
            cw = petals.state_for_petal_pad(petal, 2)
            base = petals.state_for_petal_pad(petal, 3)
            pressed = cw or ccw or base
            res.append(CaptouchPetalState(petal, pressed))
        else:
            tip = petals.state_for_petal_pad(petal, 0)
            base = petals.state_for_petal_pad(petal, 3)
            pressed = tip or base
            res.append(CaptouchPetalState(petal, pressed))
    return CaptouchState(res)


def calibration_active() -> bool:
    return False


def calibration_request() -> None:
    return


def refresh_events() -> None:
    return
