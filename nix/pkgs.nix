let
  sources = import ./sources.nix;
  nixpkgs = import sources.nixpkgs {
    overlays = [
      (import ./overlay)
    ];
  };

in with nixpkgs; rec {
  # nixpkgs passthrough
  inherit (nixpkgs) pkgs lib;

  sphinx-multiversion =
     python3Packages.buildPythonPackage rec {
      pname = "sphinx-multiversion";
      version = "0.2.4";
      src = fetchFromGitHub {
        owner = "dequis";
        repo = "sphinx-multiversion";
        rev = "fd3f0a0a1ef90781deac2de72c5d5c102c7f66da";
        sha256 = "sha256-jW0jvLlIK1LDxPvTkeAsQnZ6JpBVQWA6IjqPKaSl8lM=";
      };

      doCheck = false;

      propagatedBuildInputs = [
        python3Packages.sphinx
      ];
    };

  # All packages require to build/lint the project.
  fwbuild = [
    gcc-xtensa-esp32s3-elf-bin
    esp-idf
    esp-llvm
    esptool
    run-clang-tidy

    git wget gnumake
    flex bison gperf pkgconfig

    cmake ninja

    python3Packages.sphinx
    python3Packages.sphinx_rtd_theme
    python3Packages.black
    mypy
  ];
  fwdev = fwbuild ++ [
    openocd-esp32-bin
    python3Packages.pygame
    python3Packages.wasmer
    python3Packages.wasmer-compiler-cranelift
    python3Packages.pymad
    python3Packages.requests
    emscripten
    ncurses5
    esp-gdb
    mpremote
  ];
}
