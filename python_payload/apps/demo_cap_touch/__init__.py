from st3m.application import Application
from st3m.goose import List
from st3m.input import InputState
from st3m.ui.view import ViewManager
from ctx import Context

import captouch
import cmath
import math


class Dot:
    size = None
    pos = 0j
    filled = True

    def draw(self, ctx: Context):
        if self.size is None or self.pos is None or self.filled is None:
            return
        ctx.save()
        ctx.translate(self.pos.real, self.pos.imag)
        ctx.rotate(cmath.phase(self.pos))
        ctx.move_to(-self.size / 2, -self.size / 2)
        ctx.rel_line_to(self.size, self.size / 2)
        ctx.rel_line_to(-self.size, self.size / 2)
        ctx.close_path()
        ctx.fill() if self.filled else ctx.stroke()
        ctx.restore()


class CapTouchDemo(Application):
    def on_enter(self, vm: ViewManager):
        super().on_enter(vm)
        self.dots: List[Dot] = [Dot() for x in range(10)]

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        for i in range(10):
            petal = ins.captouch.petals[i]
            dot = self.dots[i]
            pos = 0j if petal.pos is None else petal.pos * 30
            dot.pos = (pos + 70) * captouch.PETAL_ROTORS[i]
            dot.size = 5 + 8 * math.sqrt(petal.raw_cap)
            dot.filled = not petal.pressed

    def draw(self, ctx: Context) -> None:
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        for i in range(10):
            ctx.rgb(*((0.0, 0.8, 0.8) if i % 2 else (1.0, 0.0, 1.0)))
            self.dots[i].draw(ctx)


if __name__ == "__main__":
    import st3m.run

    st3m.run.run_app(CapTouchDemo)
