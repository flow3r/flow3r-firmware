from st3m.goose import Enum
from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from st3m.ui.interactions import ScrollController
from st3m.ui import colours
from st3m.ui.view import ViewManager
from st3m.utils import sd_card_unreliable
import st3m.wifi
from ctx import Context
import network
from .applist import AppList
from .background import Flow3rView
from .record import RecordView
from .manual import ManualInputView


class ViewState(Enum):
    CONTENT = 1
    NO_INTERNET = 2
    BAD_SDCARD = 3


class Gr33nhouseApp(Application):
    items = ["Browse apps", "Record flow3r seed", "Enter flow3r seed"]

    background: Flow3rView
    state: ViewState

    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx=app_ctx)

        self.background = Flow3rView()
        self._sc = ScrollController()
        self._sc.set_item_count(3)
        self.acceptSdCard = False

    def on_enter(self, vm):
        super().on_enter(vm)
        self.update_state()

    def update_state(self):
        if not self.acceptSdCard and sd_card_unreliable():
            self.state = ViewState.BAD_SDCARD
        elif not st3m.wifi.is_connected():
            self.state = ViewState.NO_INTERNET
        else:
            self.state = ViewState.CONTENT

    def on_exit(self) -> bool:
        # request thinks after on_exit
        return True

    def draw(self, ctx: Context) -> None:
        ctx.rgb(*colours.BLACK)
        ctx.rectangle(
            -120.0,
            -120.0,
            240.0,
            240.0,
        ).fill()

        ctx.rgb(*colours.WHITE)
        ctx.font = "Camp Font 3"
        ctx.font_size = 24
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE

        if self.state == ViewState.BAD_SDCARD:
            ctx.font_size = 18
            ctx.move_to(0, -15)
            ctx.text("Unreliable SD card detected!")
            ctx.move_to(0, 5)
            ctx.text("Please replace it.")

            ctx.gray(0.75)
            ctx.move_to(0, 40)
            ctx.font_size = 16
            ctx.text("Press the app button to")
            ctx.move_to(0, 55)
            ctx.text("continue anyway.")

        elif self.state == ViewState.NO_INTERNET:
            ctx.font_size = 24
            ctx.move_to(0, -15)
            ctx.text("Connecting..." if st3m.wifi.is_connecting() else "No internet")

            ctx.gray(0.75)
            ctx.move_to(0, 40)
            ctx.font_size = 16
            ctx.text("Press the app button to")
            ctx.move_to(0, 55)
            ctx.text("enter Wi-Fi settings.")

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self._sc.think(ins, delta_ms)

        self.background.think(ins, delta_ms)

        if self.state == ViewState.BAD_SDCARD:
            if self.input.buttons.app.middle.pressed:
                self.state = ViewState.CONTENT
                self.acceptSdCard = True
        elif self.state == ViewState.NO_INTERNET:
            if self.input.buttons.app.middle.pressed:
                st3m.wifi.run_wifi_settings(self.vm)
        else:
            self.vm.replace(AppList())

        self.update_state()
