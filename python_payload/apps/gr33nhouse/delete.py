from st3m.input import InputState
from st3m.goose import Optional, List
from st3m.ui import colours
from st3m.utils import sd_card_plugged
from st3m.ui.view import BaseView
from ctx import Context
import os

from st3m import application_settings


class DeleteView(BaseView):
    def __init__(self, app) -> None:
        super().__init__()
        self._app = app
        self._delete = 0

    def draw(self, ctx: Context) -> None:
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        ctx.gray(1)
        ctx.move_to(0, 0)
        ctx.font = "Camp Font 3"
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 24
        ctx.text(self._app.name)

        ctx.move_to(0, -40)

        ctx.font_size = 20
        ctx.text("Delete app?")

        for x in range(2):
            x = 1 - x
            ctx.translate(0, 35)
            ctx.move_to(0, 0)
            ctx.gray(1)
            if x == self._delete:
                ctx.rectangle(-120, -15, 240, 30).fill()
                ctx.gray(0)
            ctx.text("yes" if x else "no")

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)  # Let BaseView do its thing

        self._delete += self.input.buttons.app.right.pressed
        self._delete -= self.input.buttons.app.left.pressed
        self._delete %= 2

        if self.input.buttons.app.middle.pressed:
            if self._delete:
                application_settings.delete_app(self._app.installed_path)
                self._app.update_installed_version(None)
            self.vm.pop()
