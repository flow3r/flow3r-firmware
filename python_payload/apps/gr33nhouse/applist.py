from st3m.goose import Optional, Enum, Any
from st3m.input import InputState
from st3m.ui import colours
from st3m.ui.view import BaseView, ViewManager
from st3m.ui.interactions import ScrollController
from ctx import Context
from math import sin
import urequests
import time
import os
import json
from .background import Flow3rView
from .confirmation import ConfirmationView
from .background import ColorTheme, broken_col, update_col, installed_col, color_themes
from .manual import ManualInputView


class ViewState(Enum):
    INITIAL = 1
    LOADING = 2
    ERROR = 3
    LOADED = 4


class AppDB:
    class App:
        class Version:
            def __init__(self, version, url_dict=None):
                # None indicates unknown value for most fields

                # version of the app as in its flow3r.toml
                self.version = version
                # urls
                if url_dict is not None:
                    self.zip_url = url_dict["downloadUrl"]
                    self.tar_url = url_dict["tarDownloadUrl"]
                # if this version is installed
                self.installed = None
                # if this version is considered an update
                self.update = None

                # this version of the app was tested by flow3r team
                self.tested = False
                # which version was tested
                self.tested_version = None
                # test result: is app broken?
                self.broken = None

                # true if app is patch by flow3r team
                self.patch = None
                # which original version this patch forks
                self.patch_base_version = None

        def __init__(self, raw_app):
            self.raw_app = raw_app
            self.processed = False

        def process(self, toml_cache):
            if self.processed:
                return
            raw_app = self.raw_app
            self.category = raw_app.get("menu")
            for field in ["name", "author", "description", "stars", "featured"]:
                setattr(self, field, raw_app.get(field))
            slug_bits = raw_app.get("repoUrl").split("/")
            self.slug = "-".join([slug_bits[-2], slug_bits[-1].lower()])

            # get latest original version
            orig = self.Version(self.raw_app["version"], url_dict=self.raw_app)
            self.available_versions = [orig]
            self.orig = orig

            # check what version is installed
            installed = None
            self.installed_version = None
            self.installed = False
            self.installed_path = None
            for app_dir in ["/sd/apps/", "/sys/flash/apps/"]:
                path = app_dir + self.slug
                app_installed = toml_cache.get(path)
                if not app_installed:
                    continue
                if not os.path.exists(path):
                    print(f"app in database but not in filesystem: {path}")
                    continue
                try:
                    installed = self.Version(app_installed["metadata"]["version"])
                except:
                    print("parsing installed version in toml.cache failed")
                installed.patch = "patch_source" in app_installed
                self.installed_path = path
                self.installed_version = installed
                self.installed = True
                break

            # check for flow3r team status flags/patched version
            patch = None
            self.broken = None
            if (status := raw_app.get("status")) is not None:
                if (tested_version := status.get("tested_version")) is not None:
                    orig.tested_version = tested_version
                    orig.tested = orig.tested_version == orig.version
                if (broken := status.get("broken")) is not None:
                    # if we specify a tested version, a new version shouldn't
                    # be flagged as broken.
                    if orig.tested_version is None or orig.tested:
                        orig.broken = broken
                if (app_patch := status.get("patch")) is not None:
                    version = app_patch.get("version")
                    patch = self.Version(version, url_dict=app_patch)
                    patch.patch = True
                    patch.patch_base_version = orig.tested_version
                    # order of list is display order, so if the original
                    # isn't broken we wanna recommend it frist, else
                    # we default to the patched version
                    self.broken = False
                    if orig.broken:
                        self.available_versions.insert(0, patch)
                    else:
                        self.available_versions.append(patch)
                else:
                    self.broken = orig.broken
            self.patch = patch

            # check for style
            self.colors = ColorTheme.from_style(raw_app["style"], self.category)

            # check if updates are available
            self.processed = True

            self._update_versions()

        def _update_versions(self):
            installed = self.installed_version
            patch = self.patch
            orig = self.orig

            orig.installed = False
            orig.update = False
            if patch:
                patch.installed = False
                patch.update = False

            if not installed:
                self.installed = False
                self.update_available = None
                return

            if patch is None:
                orig.installed = orig.version == installed.version
                if orig.version > installed.version:
                    self.update_available = True
            else:
                if installed.patch:
                    orig.installed = False
                    patch.installed = patch.version == installed.version

                    # option 1: new version of original came out
                    orig.update = orig.version > patch.patch_base_version
                    # option 2: new version of patch came out
                    patch.update = patch.version > installed.version
                else:
                    orig.installed = orig.version == installed.version
                    patch.installed = False

                    # option 1: new version of original came out
                    orig.update = orig.version > installed.version
                    # option 2: patch for installed version came out
                    patch.update = patch.patch_base_version == installed.version

            self.installed = True
            self.update_available = any(
                [version.update for version in self.available_versions]
            )

        def update_installed_version(self, version):
            if False:
                print(f"updating {self.slug}:")
                print(f"  previous at {self.installed_path}:")
                for v in self.available_versions:
                    print(
                        f'    {"patch" if v.patch else "orig"} v{v.version} {"installed" if v.installed else ""}'
                    )
                if self.installed_version is not None:
                    v = self.installed_version
                    print(
                        f'    installed: {"patch" if v.patch else "orig"} v{v.version}'
                    )
            self.installed_version = version
            self._update_versions()
            if False:
                print(f"  now at {self.installed_path}:")
                for v in self.available_versions:
                    print(
                        f'    {"patch" if v.patch else "orig"} v{v.version} {"installed" if v.installed else ""}'
                    )
                if self.installed_version is not None:
                    v = self.installed_version
                    print(
                        f'    installed: {"patch" if v.patch else "orig"} v{v.version}'
                    )

    class Category:
        def __init__(self, name, db):
            self.name = name
            self._db = db
            self.apps = []

        def add_app(self, app):
            if self.name != app.category:
                print("app seems to be in wrong category")
            self.apps.append(app)

        def scan_all(self):
            db = self._db
            for x in range(db._process_index, len(db._raw_apps)):
                raw_app = db._unprocessed_apps[x]
                if raw_app.get("menu") == self.name:
                    db.add_raw_app(raw_app)

            db.applist_sort(self.apps)

    @staticmethod
    def applist_sort_key(app):
        if app.update_available:
            return -1
        elif app.installed:
            return 1
        return 0

    @classmethod
    def applist_sort(cls, apps):
        apps.sort(key=cls.applist_sort_key)

    def __init__(self, raw_apps):
        self._raw_apps = raw_apps
        self._process_index = 0
        self.apps = []
        try:
            with open("/sd/apps/toml_cache.json") as f:
                self._toml_cache = json.load(f)
        except:
            self._toml_cache = {}
        self._done = False
        self.categories = {}

    def scan_incremental(self, increment=8):
        if self._done:
            return
        while True:
            if self._process_index >= len(self._raw_apps):
                self.applist_sort(self.apps)
                self._done = True
                print("Database finished")
                return
            raw_app = self._raw_apps[self._process_index]
            self._process_index += 1
            self.add_raw_app(raw_app)
            if increment is not None:
                increment -= 1
                if not increment:
                    break

    def scan_all_category_names(self):
        for x in range(self._process_index, len(self._raw_apps)):
            raw_app = self._raw_apps[x]
            if (cat := raw_app.get("menu")) not in self.categories:
                self.categories[cat] = None

    def scan_all(self):
        self.scan_incremental(None)

    def add_raw_app(self, raw_app):
        app = self.App(raw_app)
        app.process(self._toml_cache)
        self.apps.append(app)

        # write into category list
        cat = app.category
        # may exist as key but contain None, see scan_all_categories
        if (category := self.categories.get(cat)) is None:
            category = self.Category(cat, self)
            self.categories[cat] = category
        category.add_app(app)


class AppSubList(BaseView):
    _scroll_pos: float = 0.0

    background: Flow3rView

    def __init__(self, apps, colors, hide_tags=tuple(), app_filter=None) -> None:
        super().__init__()
        self.background = Flow3rView(colors)
        self.colors = colors
        self._sc = ScrollController()
        self.hide_tags = tuple(hide_tags)
        self.app_filter = app_filter
        if self.app_filter:
            self.unfiltered_apps = apps
        else:
            self.apps = apps
            self._sc.set_item_count(len(apps))

    def on_enter(self, vm):
        super().on_enter(vm)
        if self.app_filter:
            self.apps = [x for x in self.unfiltered_apps if self.app_filter(x)]
            self._sc.set_item_count(len(self.apps))

    def on_exit(self) -> bool:
        # request thinks after on_exit
        return True

    def draw(self, ctx: Context) -> None:
        ctx.move_to(0, 0)

        self.background.draw(ctx)

        ctx.save()
        ctx.rgb(*self.colors.text_bg)
        ctx.rectangle(
            -120.0,
            -15.0,
            240.0,
            30.0,
        ).fill()

        ctx.translate(0, -30 * self._sc.current_position())

        offset = 0

        ctx.font = "Camp Font 3"
        ctx.font_size = 24
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE

        ctx.move_to(0, 0)
        for idx, app in enumerate(self.apps):
            target = idx == self._sc.target_position()
            if target:
                ctx.rgb(*self.colors.text_fg)
            else:
                ctx.rgb(*self.colors.fg)

            if abs(self._sc.current_position() - idx) <= 5:
                xpos = 0.0
                if target and (width := ctx.text_width(app.name)) > 220:
                    xpos = sin(self._scroll_pos) * (width - 220) / 2
                ctx.move_to(xpos, offset)
                ctx.text(app.name)

                if target:
                    text = None
                    col = (0, 0, 0)
                    if app.update_available:
                        col = update_col
                        text = "update"
                    elif app.installed:
                        col = installed_col
                        text = "installed"
                    elif app.broken:
                        col = broken_col
                        text = "broken"

                    if text and not (text in self.hide_tags):
                        ctx.save()
                        ctx.rgb(*col)
                        ctx.font_size = 16
                        ctx.text_align = ctx.LEFT
                        ctx.rel_move_to(0, -5)
                        ctx.text(text)
                        ctx.restore()

            offset += 30
        if not self.apps:
            ctx.rgb(*self.colors.text_fg)
            ctx.text("(empty)")

        ctx.restore()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self._sc.think(ins, delta_ms)

        self.background.think(ins, delta_ms)
        self._scroll_pos += delta_ms / 1000

        if not self.is_active():
            return

        if self.input.buttons.app.left.pressed or self.input.buttons.app.left.repeated:
            self._sc.scroll_left()
            self._scroll_pos = 0.0
        elif (
            self.input.buttons.app.right.pressed
            or self.input.buttons.app.right.repeated
        ):
            self._sc.scroll_right()
            self._scroll_pos = 0.0
        elif self.input.buttons.app.middle.pressed:
            if self.vm is None:
                raise RuntimeError("vm is None")
            if self.apps:
                app = self.apps[self._sc.target_position()]
                self.vm.push(ConfirmationView(app))


class AppList(BaseView):
    _scroll_pos: float = 0.0
    _state: ViewState = ViewState.INITIAL

    items: list[Any] = ["All"]
    category_order: list[Any] = ["Badge", "Music", "Games", "Media", "Apps", "Demos"]

    background: Flow3rView

    def __init__(self) -> None:
        super().__init__()
        self.background = Flow3rView(ColorTheme.get("SolidBlack"))
        self._sc = ScrollController()
        self._sc.set_item_count(len(self.items))
        self.category_prev = ""

    def draw(self, ctx):
        ctx.move_to(0, 0)

        if self._state == ViewState.INITIAL or self._state == ViewState.LOADING:
            ctx.rgb(*colours.BLACK)
            ctx.rectangle(
                -120.0,
                -120.0,
                240.0,
                240.0,
            ).fill()

            ctx.save()
            ctx.rgb(*colours.WHITE)
            ctx.font = "Camp Font 3"
            ctx.font_size = 24
            ctx.text_align = ctx.CENTER
            ctx.text_baseline = ctx.MIDDLE
            ctx.text("Collecting seeds...")
            ctx.restore()
            return

        elif self._state == ViewState.ERROR:
            ctx.rgb(*colours.BLACK)
            ctx.rectangle(
                -120.0,
                -120.0,
                240.0,
                240.0,
            ).fill()

            ctx.save()
            ctx.rgb(*colours.WHITE)
            ctx.gray(1.0)
            ctx.font = "Camp Font 3"
            ctx.font_size = 24
            ctx.text_align = ctx.CENTER
            ctx.text_baseline = ctx.MIDDLE
            ctx.text("Something went wrong")
            ctx.restore()
            return

        elif self._state == ViewState.LOADED:
            ctx.move_to(0, 0)

            self.background.draw(ctx)

            ctx.save()
            ctx.gray(1.0)
            ctx.rectangle(
                -120.0,
                -15.0,
                240.0,
                30.0,
            ).fill()

            ctx.translate(0, -30 * self._sc.current_position())
            offset = 0

            ctx.font = "Camp Font 3"
            ctx.font_size = 24
            ctx.text_align = ctx.CENTER
            ctx.text_baseline = ctx.MIDDLE

            ctx.move_to(0, 0)
            for idx, item in enumerate(self.items):
                target = idx == self._sc.target_position()
                if target:
                    ctx.gray(0.0)
                else:
                    ctx.gray(1.0)

                if abs(self._sc.current_position() - idx) <= 5:
                    xpos = 0.0
                    if target and (width := ctx.text_width(item)) > 220:
                        xpos = sin(self._scroll_pos) * (width - 220) / 2
                    ctx.move_to(xpos, offset)
                    ctx.text(item)
                offset += 30

            ctx.restore()
        else:
            raise RuntimeError(f"Invalid view state {self._state}")

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self._sc.think(ins, delta_ms)

        if self._state == ViewState.INITIAL:
            if self.vm.transitioning:
                return
            try:
                self._state = ViewState.LOADING
                print("Loading app list...")
                res = urequests.get("https://flow3r.garden/api/apps.json")
                raw_apps = res.json()["apps"]
                if raw_apps == None:
                    print(f"Invalid JSON or no apps: {res.json()}")
                    self._state = ViewState.ERROR
                    return

                print("Initializing database...")
                self.db = AppDB(raw_apps)
                self.apps = self.db.apps

                self.db.scan_all_category_names()
                categories = list(self.db.categories.keys())

                def sortkey(obj):
                    try:
                        return self.category_order.index(obj)
                    except ValueError:
                        return len(self.category_order)

                categories.sort(key=sortkey)
                print("Found categories:", categories)
                self.items = ["All"] + categories + ["Updates", "Installed", "Seeds"]
                self._sc.set_item_count(len(self.items))

                self._state = ViewState.LOADED
            except Exception as e:
                print(f"Load failed: {e}")
                self._state = ViewState.ERROR
            return
        elif self._state == ViewState.LOADING:
            raise RuntimeError(f"Invalid view state {self._state}")
        elif self._state == ViewState.ERROR:
            return

        self.background.think(ins, delta_ms)
        self._scroll_pos += delta_ms / 1000

        if not self.is_active():
            return

        self.db.scan_incremental()

        if self.input.buttons.app.left.pressed or self.input.buttons.app.left.repeated:
            self._sc.scroll_left()
            self._scroll_pos = 0.0
        elif (
            self.input.buttons.app.right.pressed
            or self.input.buttons.app.right.repeated
        ):
            self._sc.scroll_right()
            self._scroll_pos = 0.0
        elif self.input.buttons.app.middle.pressed:
            if self.vm is None:
                raise RuntimeError("vm is None")
            category = self.items[self._sc.target_position()]
            colors = ColorTheme.get(category)
            hide_tags = []
            apps = None
            app_filter = None
            if category == "All":
                self.db.scan_all()
                apps = self.apps
            elif category == "Updates":
                self.db.scan_all()
                app_filter = lambda x: x.update_available
                apps = self.apps
            elif category == "Installed":
                self.db.scan_all()
                app_filter = lambda x: x.installed
                apps = self.apps
                hide_tags = ["installed"]
            elif category == "Featured":
                self.db.scan_all()
                app_filter = lambda x: x.featured
                apps = self.apps
            elif category == "Seeds":
                self.vm.push(ManualInputView(colors=colors))
            else:
                category = self.db.categories[category]
                category.scan_all()
                apps = category.apps
            if apps is not None:
                self.vm.push(AppSubList(apps, colors, hide_tags, app_filter))

        category = self.items[self._sc.target_position()]
        if category != self.category_prev:
            self.category_prev = category
            self.background.update_colors(ColorTheme.get(category))
