from st3m.input import InputState
from st3m.ui import colours
from st3m.ui.view import BaseView, ViewManager
from ctx import Context
from .background import Flow3rView
from .background import broken_col, update_col, installed_col
from .download import DownloadView
from .delete import DeleteView
from st3m.utils import wrap_text
import math


class Button:
    def __init__(self, pos, size):
        self.len = list(size)
        self.mid = list(pos)
        self.min = [pos[x] - size[x] / 2 for x in range(2)]
        self.shift = 0

        self.top_tags = list()
        self.bot_tags = list()
        self.text = "back"

    def draw(self, ctx, highlight=False):
        ctx.font_size = 18
        ctx.gray(1)
        if not highlight:
            ctx.move_to(*self.mid)
            ctx.text(self.text)
            return

        ctx.rectangle(*self.min, *self.len).fill()
        ctx.fill()

        ctx.save()
        ctx.gray(0)
        ctx.move_to(self.shift + self.mid[0], self.mid[1])
        ctx.text_align = ctx.LEFT
        ctx.text(self.text)
        ctx.font_size = 14
        ctx.rel_move_to(0, -6)
        x_start = ctx.x
        for text, col in self.top_tags:
            ctx.rgb(*col)
            ctx.text(" " + text)
        x_stop = ctx.x
        ctx.rel_move_to(x_start - x_stop, 12)
        for text, col in self.bot_tags:
            ctx.rgb(*col)
            ctx.text(" " + text)
        x_stop = max(x_stop, ctx.x)
        ctx.restore()

        if abs(x_asym := self.shift + x_stop) > 0.1:
            self.shift -= x_asym / 2
            self.draw(ctx, True)


class InstallButton(Button):
    def __init__(self, pos, size, version):
        super().__init__(pos, size)

        self.version = version
        self.text = "install"
        if version.patch:
            self.text += " patch"
        tags = self.top_tags
        if version.broken:
            tags.append(("broken", broken_col))
            tags = self.bot_tags
        if version.update:
            tags.append(("update", update_col))
            tags = self.bot_tags
        if version.installed:
            tags.append(("installed", installed_col))
            tags = self.bot_tags


class DeleteButton(Button):
    def __init__(self, pos, size):
        super().__init__(pos, size)
        self.text = "delete"


class ScrollBlock:
    def __init__(self, raw_text):
        self.num_lines = 2
        self.line_height = 18
        self.start = -87
        self.width = 175
        self.grad_len = 0
        self.end = self.start + self.num_lines * self.line_height

        self.raw_text = raw_text
        self.speed = 0
        self.pos = self.start + self.line_height / 2
        self.grad = self.grad_len / (self.end - self.start + 2 * self.grad_len)
        self.clip_min = self.start - self.grad_len
        self.clip_max = self.end + self.grad_len

    def get_lines(self, ctx):
        self.lines = wrap_text(self.raw_text, self.width, ctx)
        if not self.lines:
            return

        if len(self.lines) > self.num_lines:
            self.speed = 1 / 160
            self.lines.append("")
        else:
            self.pos += (self.num_lines - len(self.lines)) * self.line_height / 2

        self.overflow_pos = len(self.lines) * self.line_height

    def draw(self, ctx):
        if not hasattr(self, "lines"):
            self.get_lines(ctx)
        if not self.lines:
            return

        ctx.save()
        ctx.rectangle(
            -self.width / 2, self.clip_min, self.width, self.clip_max - self.clip_min
        ).clip()
        if not self.speed:
            for x, line in enumerate(self.lines):
                pos = x * self.line_height + self.pos
                ctx.move_to(0, pos)
                ctx.text(line)
        else:
            self.pos %= self.overflow_pos
            x = int(
                (self.clip_min - self.line_height / 2 - self.pos) // self.line_height
            )
            while True:
                line = self.lines[x % len(self.lines)]
                pos = x * self.line_height + self.pos
                if pos > self.clip_max + self.line_height / 2:
                    break
                if pos > self.end - self.line_height / 2:
                    ctx.linear_gradient(0, self.clip_min, 0, self.clip_max)
                    ctx.add_stop(0, (1.0, 1.0, 1.0), 1)
                    ctx.add_stop(1 - self.grad, (1.0, 1.0, 1.0), 1)
                    ctx.add_stop(1, (1.0, 1.0, 1.0), 0)
                elif pos < self.start + self.line_height / 2:
                    ctx.linear_gradient(0, self.clip_min, 0, self.clip_max)
                    ctx.add_stop(0, (1.0, 1.0, 1.0), 0)
                    ctx.add_stop(self.grad, (1.0, 1.0, 1.0), 1)
                    ctx.add_stop(1, (1.0, 1.0, 1.0), 1)
                else:
                    ctx.gray(1)
                ctx.move_to(0, pos)
                ctx.text(line)
                x += 1
        ctx.restore()

    def think(self, ins, delta_ms):
        self.pos -= delta_ms * self.speed


class ConfirmationView(BaseView):
    background: Flow3rView

    def __init__(self, app) -> None:
        super().__init__()
        self.background = Flow3rView(app.colors)

        self.app = app

    def on_enter(self, vm):
        super().on_enter(vm)
        self.buttons = []
        self.button_index = 0
        x = 0
        size = [240, 25]
        for version in self.app.available_versions:
            pos = [0, 48 + 25 * x]
            self.buttons.append(InstallButton(pos, size, version))
            x += 1
        if self.app.installed:
            pos = [0, 48 + 25 * x]
            self.buttons.append(DeleteButton(pos, size))
            x += 1

        self.desc = ScrollBlock(self.app.description)

    def on_exit(self) -> bool:
        # request thinks after on_exit
        return True

    def draw(self, ctx: Context) -> None:
        ctx.move_to(0, 0)

        self.background.draw(ctx)

        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE

        ctx.font_size = 17

        self.desc.draw(ctx)

        ctx.rgb(*colours.WHITE)
        ctx.rectangle(
            -120.0,
            -35.0,
            240.0,
            40.0,
        ).fill()

        ctx.font = "Camp Font 3"
        app = self.app
        ctx.rgb(*colours.BLACK)
        ctx.font_size = 24
        ctx.move_to(0, -15)
        ctx.text(app.name)

        if app.author:
            ctx.font_size = 17
            ctx.gray(1)
            ctx.move_to(0, 21)
            ctx.text("by " + app.author)

        for x, button in enumerate(self.buttons):
            button.draw(ctx, highlight=x == self.button_index)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self.background.think(ins, delta_ms)
        self.desc.think(ins, delta_ms)

        if self.is_active():
            self.button_index += self.input.buttons.app.right.pressed
            self.button_index -= self.input.buttons.app.left.pressed
            self.button_index %= len(self.buttons)
            if self.input.buttons.app.middle.pressed:
                button = self.buttons[self.button_index]
                if isinstance(button, InstallButton):
                    print("Installing", self.app.name, "from", button.version.tar_url)
                    self.vm.push(
                        DownloadView(
                            self.app,
                            button.version,
                        )
                    )
                elif isinstance(button, DeleteButton):
                    self.vm.push(
                        DeleteView(
                            self.app,
                        )
                    )
                else:
                    self.vm.pop()
