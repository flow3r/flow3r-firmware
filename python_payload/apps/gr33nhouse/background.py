import random
from st3m.input import InputState

from st3m.ui.view import BaseView
from ctx import Context

seed_cols = [
    (0, 0, 1),
    (0, 1, 1),
    (1, 1, 0),
    (0, 1, 0),
    (1, 0, 1),
]

broken_col = (0, 0.4, 1)
update_col = (0.9, 0.7, 0)
installed_col = (0.5, 0.5, 0.5)


def color_blend(lo, hi, blend):
    if blend >= 1:
        return hi
    elif blend <= 0:
        return lo
    num_x = 3
    if len(lo) > 3 or len(hi) > 3:
        lo = list(lo) + [1]
        hi = list(hi) + [1]
        num_x = 4
    return tuple([lo[x] * (1 - blend) + hi[x] * blend for x in range(num_x)])


class FlowerTheme:
    def __init__(self, fg, solid=False):
        self.fg = fg
        self.solid = solid


class ColorTheme:
    def __init__(self, bg, flowers, num_stars=8):
        self.bg = bg
        self.fg = (1, 1, 1)
        self.text_bg = (1, 1, 1)
        self.text_fg = (0, 0, 0)
        self.flowers = flowers
        self.num_stars = num_stars

    @classmethod
    def from_style(cls, style, category):
        def color_from_style(color):
            try:
                ret = []
                if color.startswith("rgb"):
                    ret = color[color.find("(") + 1 : color.find(")")].split(",")
                    ret = [float(x) for x in ret]
                if color.startswith("#"):
                    ret = [
                        int(color[x : x + 2], 16) / 255 for x in range(1, len(color), 2)
                    ]
                if 2 < len(ret) < 5:
                    return tuple(ret)
            except:
                pass

        fallback = cls.get(category)
        if style:
            bg = color_from_style(style.get("background"))
            color = color_from_style(style.get("color"))
        else:
            bg = None
            color = None
        if bg is None:
            bg = fallback.bg
        if color is None:
            color = fallback.flowers[0].fg
        flower_colors = [FlowerTheme(color)]
        return cls(bg, flower_colors)

    @classmethod
    def get(cls, key):
        if key in color_themes:
            return color_themes[key]
        else:
            return default_color_theme


color_themes = {
    "Classic": ColorTheme((0.1, 0.4, 0.3), [FlowerTheme((1.0, 0.6, 0.4, 0.4), True)]),
    "SolidBlack": ColorTheme((0, 0, 0), [FlowerTheme((0, 0, 0), True)]),
    "Updates": ColorTheme((0.9, 0.8, 0), [FlowerTheme((0.9, 0.6, 0))]),
    "Installed": ColorTheme(installed_col, [FlowerTheme((0, 0.8, 0.8))]),
    "Music": ColorTheme((1, 0.4, 0.7), [FlowerTheme((1.0, 0.8, 0))]),
    "Games": ColorTheme((0.9, 0.5, 0.1), [FlowerTheme((0.5, 0.3, 0.1))]),
    "Badge": ColorTheme((0.3, 0.4, 0.7), [FlowerTheme((0, 0.8, 0.8))]),
    "Apps": ColorTheme((0.7, 0, 0.7), [FlowerTheme((0, 0, 0))]),
    "Demos": ColorTheme((0, 0, 0), [FlowerTheme((0.5, 0.5, 0.5))]),
    "Media": ColorTheme((0, 0, 0), [FlowerTheme((0.5, 0.5, 0.5))]),
    "All": ColorTheme((0.18, 0.81, 0.36), [FlowerTheme((0, 0.3, 0))]),
    "Seeds": ColorTheme(
        (0.1, 0.1, 0.1), [FlowerTheme([x * 0.7 for x in col]) for col in seed_cols]
    ),
    "Featured": ColorTheme(
        (0.2, 0.0, 0.3),
        [FlowerTheme((0.9, 0.6, 0)), FlowerTheme((1.0, 0.4, 0.7), True)],
    ),
}

default_color_theme = color_themes["Classic"]


class Flow3rView(BaseView):
    def __init__(self, colors=None) -> None:
        super().__init__()
        self.colors = default_color_theme if colors is None else colors

        self.flowers = []
        for i in range(self.colors.num_stars):
            flower_color = random.choice(self.colors.flowers)
            flower = Flower(
                ((random.getrandbits(16) - 32767) / 32767.0) * 200,
                ((random.getrandbits(16)) / 65535.0) * 240 - 120,
                ((random.getrandbits(16)) / 65535.0) * 400 + 25,
                colors=flower_color,
            )
            flower.fg_col_prev = flower.fg_col
            flower.fg_col_target = flower.fg_col
            self.flowers.append(flower)
        self.bg_col = self.colors.bg
        self.bg_col_prev = self.bg_col
        self.bg_col_target = self.bg_col
        self.color_blend = None

    def update_colors(self, colors):
        self.colors = colors
        self.bg_col_prev = self.bg_col
        self.bg_col_target = self.colors.bg
        for f in self.flowers:
            flower_color = random.choice(self.colors.flowers)
            f.fg_col_prev = f.fg_col
            f.fg_col_target = flower_color.fg
        self.color_blend = 0

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        for f in self.flowers:
            f.y += (10 * delta_ms / 1000.0) * 200 / f.z
            if f.y > 300:
                f.y = -300
            f.rot += float(delta_ms) * f.rot_speed
        self.flowers = sorted(self.flowers, key=lambda f: -f.z)

        if self.color_blend is not None:
            self.color_blend += delta_ms / 160

    def draw(self, ctx: Context) -> None:
        ctx.save()
        ctx.rectangle(-120, -120, 240, 240)
        if self.color_blend is not None:
            self.bg_col = color_blend(
                self.bg_col_prev, self.bg_col_target, self.color_blend
            )
            for f in self.flowers:
                f.fg_col = color_blend(f.fg_col_prev, f.fg_col_target, self.color_blend)
            if self.color_blend >= 1:
                self.color_blend = None

        ctx.rgb(*self.bg_col)
        ctx.fill()

        for f in self.flowers:
            f.draw(ctx)
        ctx.restore()


class Flower:
    def __init__(self, x: float, y: float, z: float, colors=None) -> None:
        self.fg_col = (1.0, 0.6, 0.4, 0.4)
        self.solid = True
        if colors is not None:
            self.fg_col = colors.fg
            self.solid = colors.solid
        self.x = x
        self.y = y
        self.z = z
        self.rot = 0.0
        self.rot_speed = (((random.getrandbits(16) - 32767) / 32767.0) - 0.5) / 800

    def draw(self, ctx: Context) -> None:
        ctx.save()
        ctx.line_width = 4
        ctx.translate(-78 + self.x, -70 + self.y)
        ctx.translate(50, 40)
        ctx.rotate(self.rot)
        ctx.translate(-50, -40)
        ctx.scale(100 / self.z, 100.0 / self.z)

        if len(self.fg_col) == 4:
            ctx.rgba(*self.fg_col)
        else:
            ctx.rgb(*self.fg_col)

        ctx.move_to(76.221727, 3.9788409).curve_to(
            94.027758, 31.627675, 91.038918, 37.561293, 94.653428, 48.340473
        ).rel_curve_to(
            25.783102, -3.90214, 30.783332, -1.52811, 47.230192, 4.252451
        ).rel_curve_to(
            -11.30184, 19.609496, -21.35729, 20.701768, -35.31018, 32.087063
        ).rel_curve_to(
            5.56219, 12.080061, 12.91196, 25.953973, 9.98735, 45.917643
        ).rel_curve_to(
            -19.768963, -4.59388, -22.879866, -10.12216, -40.896842, -23.93099
        ).rel_curve_to(
            -11.463256, 10.23025, -17.377386, 18.2378, -41.515124, 25.03533
        ).rel_curve_to(
            0.05756, -29.49286, 4.71903, -31.931936, 10.342734, -46.700913
        ).curve_to(
            33.174997, 77.048676, 19.482194, 71.413009, 8.8631648, 52.420793
        ).curve_to(
            27.471602, 45.126773, 38.877997, 45.9184, 56.349456, 48.518302
        ).curve_to(
            59.03275, 31.351935, 64.893201, 16.103886, 76.221727, 3.9788409
        ).close_path()

        if self.solid:
            ctx.fill()
        else:
            ctx.stroke()

        ctx.restore()
