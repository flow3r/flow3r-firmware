from st3m.application import Application
from st3m import utils
import json
import media
import os, uos
import math, cmath, random
import leds
from st3m.ui import led_patterns

COLOR_BG = (0, 0, 0)
COLOR_FG = (0, 1, 1)
COLOR_CURSOR = (1, 1, 0)
COLOR_PLAYING = (1, 0, 1)

PETAL_A = 1
PETAL_B = 3
PETAL_C = 5
PETAL_D = 7
PETAL_E = 9

PLAYLIST_MAX_LEN = 1000


# taken from https://github.com/python/cpython/blob/main/Lib/bisect.py
# Copyright (c) 2001-2024 Python Software Foundation; All Rights Reserved
# (modified)
def insort(a, x, lo=0, hi=None, *, key=None, unique=False):
    if key is None:
        lo = bisect(a, x, lo, hi)
    else:
        lo = bisect(a, key(x), lo, hi, key=key)
    if not unique or not lo or a[lo - 1] != x:
        a.insert(lo, x)
        return lo
    return None


# taken from https://github.com/python/cpython/blob/main/Lib/bisect.py
# Copyright (c) 2001-2024 Python Software Foundation; All Rights Reserved
def bisect(a, x, lo=0, hi=None, *, key=None):
    if lo < 0:
        raise ValueError("lo must be non-negative")
    if hi is None:
        hi = len(a)
    if key is None:
        while lo < hi:
            mid = (lo + hi) // 2
            if x < a[mid]:
                hi = mid
            else:
                lo = mid + 1
    else:
        while lo < hi:
            mid = (lo + hi) // 2
            if x < key(a[mid]):
                hi = mid
            else:
                lo = mid + 1
    return lo


class Marquee:
    def __init__(self):
        self.speed = 0.015
        self.gap = 40
        self.x = 0
        self.ypos = None
        self.set_width(0)
        self.text = None
        self.set_text("")
        self._needs_redraw = True

    def set_text(self, text):
        if self.text == text:
            return
        self.text = text
        self.text_width = None
        self.x = 0
        self.full_redraw = True

    def set_width(self, ypos):
        ypos = int(ypos)
        if self.ypos == ypos:
            return
        radius = 10
        if ypos < -90 + 14 + radius:
            self.frame_width = 65 - radius
        elif ypos > 34 - radius and ypos < 34 + 14 + radius:
            self.frame_width = 106 - radius
        else:
            y = max(ypos + 12, -ypos - 2) / 120
            self.frame_width = 120 * math.sqrt(1 - y * y)
            self.frame_width = max(0, self.frame_width - 5)
        self.ypos = ypos
        self.full_redraw = True

    def think(self, delta_ms):
        if self.text_width is None:
            return
        if self.text_width / 2 < self.frame_width:
            return
        self.x += delta_ms * self.speed
        self.x %= self.text_width + self.gap
        self.full_redraw = True

    def draw(self, ctx):
        if self.text_width is None:
            self.text_width = ctx.text_width(self.text)
        if not self.full_redraw:
            return
        ctx.move_to(0, 0)
        ctx.save()
        ctx.rgb(*COLOR_BG)
        ctx.rectangle(-120, -12, 240, 14).fill()
        ctx.restore()
        ctx.save()
        if self.text_width / 2 < self.frame_width:
            ctx.text_align = ctx.CENTER
            ctx.text(self.text)
        else:
            ctx.text_align = ctx.LEFT
            ctx.rel_move_to(-self.x - self.frame_width, 0)
            ctx.text(self.text)
            ctx.rel_move_to(self.gap, 0)
            ctx.text(self.text)
        ctx.restore()
        self.full_redraw = False


def minutify(seconds):
    try:
        seconds = int(seconds)
    except ValueError:
        return ""
    minutes = seconds // 60
    seconds = seconds % 60
    return f"{minutes:02.}:{seconds:02.}"


def slugify(path):
    # strip directory
    if path.startswith("/sd/music/"):
        path = path[len("/sd/music/") :]
    # take care of non-ascii characters, bad hack
    path = repr(path)[1:-1]
    return path


class ListPageItem:
    def __init__(self, slug):
        self.ypos = None
        self.highlight = None
        self.already_drawn = 0
        self.slug = slug

    def undraw(self, ctx, ypos, highlight):
        if ypos is not None:
            ypos = int(ypos)
            if ypos < -95 or ypos > 67 + ctx.font_size:
                ypos = None
        if highlight == self.highlight and ypos == self.ypos:
            self.already_drawn += 1
            self.already_drawn = min(self.already_drawn, 3)
        else:
            self.already_drawn = 0
        if self.already_drawn > 1:
            return False
        if self.ypos is not None:
            ctx.rgb(*COLOR_BG)
            ctx.rectangle(-120, self.ypos - ctx.font_size, 240, ctx.font_size).fill()
            self.ypos = None
        if ypos is None:
            return False
        self.area = 0
        ctx.rgb(*COLOR_BG)
        ctx.rectangle(-120, ypos - ctx.font_size, 240, ctx.font_size).fill()
        self.highlight = highlight
        self.ypos = ypos
        return True

    def draw(self, ctx, draw_slug=True):
        if self.highlight > 1:
            return
        ctx.move_to(0, self.ypos - 2)  # idk why the offset
        if self.highlight == 0:
            ctx.rgb(*COLOR_FG)
        elif self.highlight == 1:
            ctx.rgb(*COLOR_PLAYING)
        else:
            ctx.rgb(*COLOR_CURSOR)
        if draw_slug:
            self.draw_slug(ctx)

    def draw_slug(self, ctx):
        ctx.text(self.slug)


class Artist(ListPageItem):
    def __init__(self, artist):
        slug = artist if artist else "<unknown artist>"
        super().__init__(slug)
        self.artist = artist

    def __eq__(self, other):
        if isinstance(other, Artist):
            return self.artist == other.artist
        return False


class Song(ListPageItem):
    def __init__(self, path, slug=None):
        super().__init__("dummy")
        if slug is not None:
            self.slug = slug
            self.path = path
            return
        path = path.strip("\n")
        self.path = utils.simplify_path(path)
        self.tags = media.get_tags(self.path)
        if self.tags is None:
            self.tags = {}
        self.title = self.tags.get("title")
        self.artist = self.tags.get("artist")
        self.album = self.tags.get("album")
        self.track_number = self.tags.get("track_number", 10000)
        self.year = self.tags.get("year", 10000)
        if self.title:
            if self.artist:
                self.slug = self.artist + " - " + self.title
            else:
                self.slug = self.title
            self.slug = repr(self.slug)[1:-1]
        else:
            if path.startswith("/sd/music/"):
                path = path[len("/sd/music/") :]
            self.slug = repr(path)[1:-1]

    def __eq__(self, other):
        if isinstance(other, Song):
            return self.path == other.path
        return False

    def copy(self):
        ret = Song(self.path, self.slug)
        ret.artist = self.artist
        ret.title = self.title
        return ret


class ListPage:
    name = "dummy"

    def __init__(self, app, itemlist):
        self.ypos = -90
        self.highlight = 0
        self.highlight_alt = None
        self.highlight_is_playing = False
        self.app = app
        self.input = app.input
        self.itemlist = itemlist
        self.undraws = []
        self.drawn_items = []
        self.step_size = 0
        self.alt_scroll = False
        self.label_state = [0] * 10
        self.highlight_ypos_limits = [-90 + 14, 67]
        self.marquee = Marquee()

    @property
    def highlighted_item(self):
        if len(self.itemlist) > self.highlight:
            return self.itemlist[self.highlight]
        return None

    def draw(self, ctx):
        incr = min(self.app.draw_ms, 50) / 250
        for x in [PETAL_B, PETAL_C, PETAL_D]:  # add more if needed
            if self.label_state[x] is not None and self.label_state[x] < 0:
                self.label_state[x] = min(self.label_state[x] + incr, 0)

        self.step_size = ctx.font_size
        if self.app.full_redraw:
            for song in self.itemlist:
                song.already_drawn = 0

        highlight_ypos = self.highlight * self.step_size + int(self.ypos)
        if self.highlight_ypos_limits[0] > highlight_ypos:
            self.ypos += self.highlight_ypos_limits[0] - highlight_ypos
        elif highlight_ypos > self.highlight_ypos_limits[1]:
            self.ypos -= highlight_ypos - self.highlight_ypos_limits[1]

        highlight_range = self.highlight_ypos_limits[1] - self.highlight_ypos_limits[0]
        highlight_bias = (
            self.highlight_ypos_limits[1] + self.highlight_ypos_limits[0]
        ) / 2
        max_items = highlight_range / self.step_size
        len_itemlist = len(self.itemlist)
        if len_itemlist > max_items:
            highlight_range /= 2
            highlight_range -= self.step_size  # * 1
            highlight_ypos -= highlight_bias
            off_by = max(abs(highlight_ypos) - highlight_range + self.step_size, 0)
            off_by = off_by if highlight_ypos > 0 else -off_by
            if off_by:
                self.ypos -= highlight_ypos * min(self.app.draw_ms / 300, 1) * 0.2
            min_ypos = -self.ypos - highlight_range
            max_ypos = (
                -self.ypos + highlight_range - (len_itemlist - 1) * self.step_size
            )
            if min_ypos < 0:
                self.ypos += min_ypos
            elif max_ypos > 0:
                self.ypos += max_ypos
        else:
            self.ypos = -0.5 * (len_itemlist - 1) * self.step_size

        ypos = int(self.ypos)

        ctx.text_align = ctx.CENTER

        index_min = (self.highlight_ypos_limits[0] - self.ypos) / self.step_size
        index_max = (self.highlight_ypos_limits[1] - self.ypos) / self.step_size
        index_min = max(0, int(index_min) - 1)
        index_max = min(len_itemlist, int(index_max) + 4)

        candidates = self.itemlist[index_min:index_max]

        self.undraws += [item for item in self.drawn_items if item not in candidates]
        while self.undraws:
            self.undraws.pop().undraw(ctx, None, 0)

        drawables = []
        self.drawn_items = []
        highlight_on_screen = False
        for index_offset, item in enumerate(candidates):
            index = index_offset + index_min
            ypos = int(self.ypos) + self.step_size * index
            highlight = 2 * (index == self.highlight) + (index == self.highlight_alt)

            needs_fresh_draw = item.undraw(ctx, ypos, highlight)
            if item.ypos is not None:  # hacky: if the thing is on screen,
                self.drawn_items.append(item)
                if needs_fresh_draw:
                    drawables.append(item)
            if highlight > 1:
                if needs_fresh_draw:
                    self.marquee.full_redraw = True
                highlight_on_screen = True
                self.marquee.set_text(item.slug)
                self.marquee.set_width(ypos)
        self.marquee.think(self.app.draw_ms)

        for item in drawables:
            item.draw(ctx)

        if highlight_on_screen:
            ctx.translate(0, self.marquee.ypos - 2)
            ctx.rgb(*COLOR_CURSOR)
            self.marquee.draw(ctx)

    def scroll(self, cw_dir, overflow):
        if not self.itemlist:
            return
        self.highlight += cw_dir
        if overflow:
            self.highlight %= len(self.itemlist)
        else:
            self.highlight = max(0, min(len(self.itemlist) - 1, self.highlight))

    def think(self, ins, delta_ms):
        if self.app.active_songlist == self.itemlist:
            self.highlight_alt = self.app.active_songlist_index
            self.highlight_is_playing = self.highlight_alt == self.highlight
        else:
            self.highlight_alt = None
            self.highlight_is_playing = False

    def draw_label(self, ctx, petal):
        if petal == PETAL_D:
            ctx.move_to(-3, -3)
            ctx.rel_line_to(6, 0).stroke()
            ctx.move_to(-3, 0)
            ctx.rel_line_to(6, 0).stroke()
            ctx.move_to(-3, 3)
            ctx.rel_line_to(6, 0).stroke()

    def insort(self, item, key=None, unique=True):
        pos = insort(self.itemlist, item, key=key, unique=unique)
        if pos is not None and pos <= self.highlight:
            self.highlight = min(self.highlight + 1, len(self.itemlist) - 1)
            self.ypos -= self.step_size
        return pos


class NotPlaylistPage(ListPage):
    def draw_label(self, ctx, petal):
        if petal == PETAL_B:
            ctx.move_to(0, -4)
            ctx.rel_line_to(0, 8).stroke()
            ctx.move_to(-4, 0)
            ctx.rel_line_to(8, 0).stroke()
            ctx.stroke()
        elif petal == PETAL_A:
            ctx.arc(0, -1, 2, 0, math.tau, 0).stroke()
            ctx.arc(0, 4, 3, -math.tau / 2, 0, 0).stroke()
        else:
            super().draw_label(ctx, petal)


class ArtistsPage(NotPlaylistPage):
    name = "artists"

    def think(self, ins, delta_ms):
        super().think(ins, delta_ms)
        self.label_state[PETAL_A] = 1
        if self.app.captouch_locked:
            return
        if self.input.captouch.petals[PETAL_B].whole.pressed:
            artist = self.highlighted_item.artist if self.highlighted_item else None
            if artist is not None:  # we don't accept unknown artist here bc spam risk
                artist_found = False
                items_added = False
                items_skipped = False
                for song in self.app.mp3_files:
                    if song.artist == artist:
                        artist_found = True
                        if len(self.app.playlist) <= PLAYLIST_MAX_LEN:
                            self.app.playlist.append(song.copy())
                            items_added = True
                        else:
                            items_skipped = True
                    elif artist_found:
                        break  # list should be sorted so that should be all
                if items_added:
                    self.app.save_request = True
                    self.label_state[PETAL_B] = None
                if items_skipped:
                    # TODO: send toast once they exist
                    pass


class MediaPage(NotPlaylistPage):
    name = "media"

    def think(self, ins, delta_ms):
        super().think(ins, delta_ms)
        if self.app.captouch_locked:
            return
        if (
            self.input.captouch.petals[PETAL_B].whole.pressed
            and self.highlighted_item is not None
        ):
            if len(self.app.playlist) <= PLAYLIST_MAX_LEN:
                self.app.playlist.append(self.highlighted_item.copy())
                self.app.save_request = True
                self.label_state[PETAL_B] = None
            else:
                # TODO: send toast once they exist
                pass


class PlaylistPage(ListPage):
    name = "playlist"

    def think(self, ins, delta_ms):
        super().think(ins, delta_ms)
        if self.app.captouch_locked:
            self.alt_scroll = False
            self.label_state[PETAL_A] = 0
            return
        if self.input.captouch.petals[PETAL_B].whole.pressed and self.itemlist:
            self.undraws += [self.itemlist.pop(self.highlight)]
            self.highlight = min(len(self.itemlist) - 1, self.highlight)
            if self.app.active_songlist is self.itemlist:
                if not self.itemlist:
                    self.app.stop_song()
                elif self.highlight_alt > self.highlight:
                    self.app.active_songlist_index -= 1
                elif self.highlight_alt == self.highlight:
                    index = min(len(self.itemlist) - 1, self.app.active_songlist_index)
                    self.app.play_song(self.itemlist, index)
            self.app.save_request = True
            self.label_state[PETAL_B] = None

        self.alt_scroll = ins.captouch.petals[PETAL_A].pressed
        self.label_state[PETAL_A] = int(self.alt_scroll)

    def move_highlighted_item(self, cw_dir, overflow):
        if not self.itemlist:
            return
        if overflow:
            highlight = (self.highlight + cw_dir) % len(self.itemlist)
        else:
            highlight = min(len(self.itemlist) - 1, max(0, self.highlight + cw_dir))
        if highlight == self.highlight:
            return
        song = self.itemlist.pop(self.highlight)
        self.highlight = highlight
        self.itemlist.insert(self.highlight, song)
        self.app.save_request = True

    def draw_label(self, ctx, petal):
        if petal == PETAL_B:
            ctx.move_to(-4, 0)
            ctx.rel_line_to(8, 0).stroke()
            ctx.stroke()
        elif petal == PETAL_A:
            ctx.move_to(-3, -1)
            ctx.rel_line_to(3, -3)
            ctx.rel_line_to(3, 3).stroke()
            ctx.move_to(3, 1)
            ctx.rel_line_to(-3, 3)
            ctx.rel_line_to(-3, -3).stroke()
        elif petal == PETAL_D:
            ctx.arc(-0.5, 1, 2, 0, math.tau, 0).stroke()
            ctx.move_to(1.5, 1).rel_line_to(0, -6).stroke()
        else:
            super().draw_label(ctx, petal)

    def scroll(self, cw_dir, overflow):
        if self.alt_scroll:
            self.move_highlighted_item(cw_dir, overflow)
            return
        super().scroll(cw_dir, overflow)


class ScrollThingy:
    def __init__(self, petal_index, inverted=False):
        self.petal_index = petal_index
        self.inverted = inverted
        self.angle = None
        self.angle_ref_count = 5
        self.angle_ref = 0
        self.events = 0
        self.raw_delay = [None] * 2
        self.raw_delay_index = 0

    def get_raw_angle(self, ins):
        if ins.captouch.petals[self.petal_index].pressed:
            _, angle = ins.captouch.petals[self.petal_index].position
            angle /= 40000
            angle *= abs(angle)
            angle = min(1, max(-1, angle))
            if self.inverted:
                return -angle
            else:
                return angle
        else:
            return None

    def think(self, ins, delta_ms):
        raw_angle = self.get_raw_angle(ins)
        if raw_angle is not None and raw_angle == self.raw_delay[self.raw_delay_index]:
            return
        self.raw_delay_index += 1
        self.raw_delay_index %= len(self.raw_delay)

        rdi = self.raw_delay_index
        self.raw_delay[rdi] = raw_angle

        rdi += 1
        rdi %= len(self.raw_delay)
        angle = self.raw_delay[rdi]
        if self.raw_delay[self.raw_delay_index] is None or angle is None:
            if self.angle is not None:
                self.angle = None
                self.angle_ref = 0
                self.angle_ref_count = 3
            return
        if self.angle_ref_count:
            self.angle_ref += angle / 3
            self.angle_ref_count -= 1
            return
        if self.angle is None:
            self.angle = self.angle_ref
        self.angle += 0.37 * (angle - self.angle)
        delta = self.angle - self.angle_ref
        if abs(delta) > 0.2:
            self.events += 1 if delta > 0 else -1
            self.angle_ref = self.angle


class App(Application):
    def get_help(self):
        ret = (
            "This is an mp3 player. It scans your SD card for files in /sd/music/ "
            "that end in '.mp3' on startup.\n\n"
            "Move your fingers on petal 8 and 2 up and down to scroll through the "
            "current list. The other top petals can be tapped for more discrete "
            "scrolling. \n\n"
            "There's two major pages, songs and playlist. You can toggle between "
            "them with petal {d}. Play back the currently highlighted song with "
            "petal {c}. Use the shoulder button to pause and go forward/backward in "
            "the playlist. \n\n"
            "On the song page, petal {b} adds the selected song to the playlist. "
            "You can also hold petal {a} to scroll through artists.\n\n"
            "On the playlist page, petal {b} removes the selected song from the "
            "playlist. You can hold petal {a} to move the selected song up and down.\n\n"
            "Holding petal {e} puts some petals in alternative modes: Tapping "
            "petal {a} scrolls through LED modes, petal {d} though repeat modes, and petal {d} "
            "tries to scroll to the currently playing song in the active page.\n\n"
            "You can lock/unlock captouch by holding down the app button.\n\n"
            "The playlist autosaves but waits for a 7 second period of no changes "
            "to keep writes low while doing many successive edits. "
            "When a save is waiting to be performed a tiny save icon is displayed. "
            "This icon also shows while the SD is scanned for files."
        )
        ret = ret.format(a=PETAL_A, b=PETAL_B, c=PETAL_C, d=PETAL_D, e=PETAL_E)
        return ret

    def __init__(self, app_ctx):
        super().__init__(app_ctx)
        self.full_redraw = True
        self.draw_ms = 0
        self.enter_done = False
        self.dirpath = "/sd/app_data/mp3_player"
        self.playlist_file = "playlist.m3u"
        self.settings_file = "settings.json"

        self.mp3_files = []
        self.artists = []
        self.playlist = []

        self.media_page = MediaPage(self, self.mp3_files)
        self.artists_page = ArtistsPage(self, self.artists)
        self.playlist_page = PlaylistPage(self, self.playlist)
        self.current_page = self.media_page

        self.scan_generator = (
            self.scandir("/sd/music") if utils.sd_card_plugged() else None
        )

        self.circer_rot = None
        self.invert_scroll_dir = False
        self.scroll_thingies = [
            ScrollThingy(2, self.invert_scroll_dir),
            ScrollThingy(8, not self.invert_scroll_dir),
        ]

        self.save_request = False
        self.save_timer = 0

        self.playing = False
        self.active_song = None
        self.active_songlist = None
        self.active_songlist_index = None
        self.song_reset_cooldown = 0

        self.app_button_release_ignore = False
        self.input.buttons.app.middle.repeat_enable(800, 300)
        self.captouch_locked = False

        self.label_state = [0] * 10

        self.playback_mode = 2
        self.orig_led_brightness = None
        self.orig_slew_rate = None
        self.led_mode = 1
        self.led_pattern_request = False

        self.marquee = Marquee()
        self.marquee.set_width(85)

        self.time_ok = False
        self.last_time_ms = -1
        self.last_led_mode = -1
        self.last_playback_mode = -1
        self.last_sd_access = False

    def play_song(self, songlist, song_index):
        if song_index >= len(songlist) or song_index < 0:
            return
        if songlist is self.artists:
            artist = songlist[song_index].artist
            songlist = self.mp3_files
            song_index = -1
            for x, song in enumerate(songlist):
                if song.artist == artist:
                    song_index = x
                    break
        song = songlist[song_index]
        media.load(song.path)
        self.playing = True
        self.time_ok = True
        self.active_songlist = songlist
        self.active_songlist_index = song_index
        self.active_song = song
        self.led_pattern_request = True
        self.full_redraw = True

    def stop_song(self):
        media.stop()
        self.active_songlist_index = None
        self.active_songlist = None
        self.active_song = None
        self.playing = False

    def scandir(self, dirname):
        if os.path.isdir(dirname):
            for entry in uos.ilistdir(dirname):
                if entry[1] == 0x8000:
                    if entry[0].endswith(".mp3"):
                        song = Song(dirname + "/" + entry[0])
                        yield song
                elif entry[1] == 0x4000:
                    inner_gen = self.scandir(dirname + "/" + entry[0])
                    for ret in inner_gen:
                        yield ret

    @property
    def playlist_path(self):
        return self.dirpath + "/" + self.playlist_file

    @property
    def settings_path(self):
        return self.dirpath + "/" + self.settings_file

    def save_data(self):
        if not os.path.exists(self.dirpath):
            utils.mkdir_recursive(self.dirpath)
        body = "\n".join([song.path for song in self.playlist])
        if utils.save_file_if_changed(self.playlist_path, body):
            print(f"saved playlist to {self.playlist_path}")
        with open(self.settings_path, "w") as f:
            f.write(
                json.dumps(
                    {"playback mode": self.playback_mode, "led mode": self.led_mode}
                )
            )
        self.save_request = False

    def load_data(self):
        if os.path.exists(self.playlist_path):
            with open(self.playlist_path, "r") as f:
                print(f"loading playlist from {self.playlist_path}")
                self.playlist.clear()
                for line in f:
                    if os.path.exists(line):
                        self.playlist.append(Song(line))
                    else:
                        print(f"playlist: couldn't find song {line}")
        if os.path.exists(self.settings_path):
            with open(self.settings_path, "r") as f:
                data = f.read()
            try:
                settings = json.loads(data)
                self.playback_mode = int(settings["playback mode"])
                self.led_mode = int(settings["led mode"])
            except (json.JSONDecodeError, KeyError, ValueError):
                pass

    def draw(self, ctx):
        if not self.enter_done or not self.mp3_files:
            self.full_redraw = True
        incr = min(self.draw_ms, 50) / 250
        for x in [PETAL_D, PETAL_C, PETAL_B, PETAL_A]:  # add more if needed
            if self.label_state[x] is not None and self.label_state[x] < 0:
                self.label_state[x] = min(self.label_state[x] + incr, 0)
        if self.full_redraw:
            ctx.rgb(*COLOR_BG)
            ctx.rectangle(-120, -120, 240, 240).fill()
        ctx.rgb(*COLOR_FG)
        ctx.font_size = 14
        ctx.text_baseline = ctx.BOTTOM
        ctx.text_align = ctx.CENTER
        ctx.line_width = 1
        ctx.save()
        ctx.rectangle(-120, -90, 240, 67 + 90).clip()
        if not self.mp3_files:
            ctx.move_to(0, 0)
            ctx.text("in /sd/music")
            if self.scan_generator:
                ctx.text("...")
                ctx.move_to(0, -14)
                ctx.text("scanning for .mp3 files")
            else:
                ctx.text(" :/")
                ctx.move_to(0, -14)
                ctx.text("no .mp3 files found")
        else:
            self.current_page.draw(ctx)
        ctx.restore()
        # order shouldn't matter as they don't overlap.
        self.draw_header(ctx)
        self.draw_footer(ctx)
        # must be last
        self.draw_circer(ctx)
        if self.captouch_locked:
            ctx.rgb(*COLOR_BG)
            ctx.rectangle(-65, -35, 130, 70).fill()
            ctx.rgb(*COLOR_FG)
            ctx.rectangle(-60, -30, 120, 60).stroke()
            ypos = -13
            ctx.move_to(0, ypos)
            ctx.text("captouch locked")
            ypos += 20
            ctx.move_to(0, ypos)
            ctx.text("hold app button")
            ypos += 14
            ctx.move_to(0, ypos)
            ctx.text("down to unlock")
        self.full_redraw = False
        self.draw_ms = 0

    def draw_header(self, ctx):
        if not self.full_redraw:
            return
        ctx.rgb(*COLOR_BG)
        ctx.rectangle(-120, -120, 240, 30).fill()
        ctx.rgb(*COLOR_FG)
        ctx.rectangle(-120, -95, 240, 1).fill()
        ctx.move_to(0, -100)
        ctx.text(self.current_page.name)

    def draw_footer(self, ctx):
        if self.full_redraw:
            ctx.rgb(*COLOR_BG)
            ctx.rectangle(-120, 67, 240, 65).fill()
            ctx.rgb(*COLOR_FG)
            ctx.rectangle(-120, 72, 240, 1).fill()

        # song name
        if self.full_redraw:
            self.marquee.full_redraw = True
        if self.active_song is not None:
            self.marquee.set_text(self.active_song.slug)
        else:
            self.marquee.set_text("")
        self.marquee.think(self.draw_ms)
        ctx.save()
        ctx.translate(0, 87)
        ctx.rgb(*COLOR_FG)
        self.marquee.draw(ctx)
        ctx.restore()

        # progress bar
        ctx.rectangle(-60 - 1, 92 - 1, 120 + 2, 5 + 2).fill()
        media_dur = media.get_duration()
        if media_dur:
            progress = media.get_position() / media_dur
            progress = min(1, max(0, progress))
        else:
            progress = 0
        ctx.rgb(*COLOR_BG)
        barlen = 120 * progress
        ctx.rectangle(-60 + barlen, 92, 120 - barlen, 5).fill()

        # time indicator
        if self.time_ok:
            time_ms = media.get_time()
        else:
            time_ms = None
        if time_ms != self.last_time_ms:
            ctx.rgb(*COLOR_BG)
            ctx.rectangle(15, 101, 100, 14).fill()
            if time_ms:
                ctx.save()
                ctx.rgb(*COLOR_FG)
                ctx.text_align = ctx.LEFT
                ctx.move_to(15, 113)
                ctx.text(minutify(media.get_time()))
                ctx.line_width = 1
                ctx.restore()
            self.last_time_ms = time_ms

        # icons
        sd_access = bool(self.save_request or self.scan_generator)
        if sd_access != self.last_sd_access or self.full_redraw:
            self.last_sd_access = sd_access
            ctx.save()
            ctx.translate(-12, 107)
            ctx.rgb(*COLOR_BG)
            ctx.rectangle(-10, -7, 12, 16).fill()
            if self.last_sd_access:
                ctx.rgb(*COLOR_FG)
                self.draw_symbol(ctx, -1)
            ctx.restore()
        if self.playback_mode != self.last_playback_mode or self.full_redraw:
            ctx.save()
            ctx.translate(-31, 107)
            ctx.rgb(*COLOR_BG)
            ctx.rectangle(-7, -7, 16, 16).fill()
            ctx.rgb(*COLOR_FG)
            self.draw_symbol(ctx, self.playback_mode)
            ctx.restore()
            self.last_playback_mode = self.playback_mode
        if self.led_mode != self.last_led_mode or self.full_redraw:
            ctx.save()
            ctx.translate(-44, 107)
            ctx.rgb(*COLOR_BG)
            ctx.rectangle(-7, -7, 12, 16).fill()
            ctx.rgb(*COLOR_FG)
            self.draw_symbol(ctx, self.led_mode + 4)
            ctx.restore()
            self.last_led_mode = self.led_mode

    def draw_circer(self, ctx):
        if self.circer_rot is None:
            self.circer_rot = [
                112 * cmath.exp(1j * math.tau * (x / 10 - 1 / 4)) for x in range(10)
            ]
        for petal in range(1, 10, 2):
            ctx.save()
            rot = self.circer_rot[petal]
            if petal == 5:
                rot -= 2j
            ctx.translate(rot.real, rot.imag)
            ctx.rgb(*COLOR_BG)
            ctx.arc(0, 0, 10, 0, math.tau, -1).fill()
            col_fg = COLOR_FG
            col_bg = COLOR_BG

            if self.label_state[PETAL_E] or petal in [PETAL_C, PETAL_E]:
                page = self
            else:
                page = self.current_page
            if page.label_state[petal] is None:
                page.label_state[petal] = -1
            state = page.label_state[petal]
            if state:
                if state > 0:
                    col_bg = [x * state for x in col_fg]
                    col_fg = [x * (1 - state) for x in col_fg]
                else:
                    col_bg = [x * -state for x in col_fg]
                ctx.rgb(*col_bg)
                ctx.arc(0, 0, 7, 0, math.tau, -1).fill()
            ctx.rgb(*COLOR_FG)
            ctx.arc(0, 0, 7, 0, math.tau, -1).stroke()
            ctx.rgb(*col_fg)
            page.draw_label(ctx, petal)
            ctx.restore()

    def draw_symbol(self, ctx, symbol):
        # -1: save symbol
        if symbol == -1:
            ctx.move_to(0, 4)
            ctx.rel_line_to(0, -8)
            ctx.rel_line_to(-5, 0)
            ctx.rel_line_to(-3, 3)
            ctx.rel_line_to(0, 5)
            ctx.rel_line_to(8, 0)
            ctx.stroke()
            ctx.move_to(-1, 4)
            ctx.rel_line_to(0, -4)
            ctx.rel_line_to(-3, 0)
            ctx.rel_line_to(0, 4)
            ctx.rel_line_to(3, 0)
            ctx.stroke()
        # 0: start of playback symbols: none
        # 1: repeat single
        # 2: repeat
        if symbol == 1 or symbol == 2:
            ctx.save()
            if symbol == 2:
                ctx.scale(1.25, 1.25)
                ctx.translate(1, 1)
            ctx.move_to(1.25, 0.5)
            ctx.rel_line_to(-2, 2)
            ctx.rel_line_to(2, 2)
            ctx.stroke()
            ctx.move_to(2.75, 2.5)
            ctx.rel_line_to(1.5, -1.5)
            ctx.rel_line_to(0, -2)
            ctx.rel_line_to(-2, -2)
            ctx.rel_line_to(-4.5, 0)
            ctx.rel_line_to(-2, 2)
            ctx.rel_line_to(0, 2)
            ctx.rel_line_to(1.5, 1.5)
            ctx.stroke()
            ctx.restore()
            if symbol == 1:
                ctx.move_to(5, 5)
                ctx.rel_line_to(2.5, -2.5)
                ctx.rel_line_to(0, 5)
                ctx.stroke()
                pass
        # 3: shuffle,
        elif symbol == 3:
            ctx.move_to(-4, -1.25)
            ctx.rel_line_to(3.25, 0)
            ctx.rel_line_to(3.5, 3.5)
            ctx.rel_line_to(4.25, 0)
            ctx.rel_line_to(-3, 3)
            ctx.stroke()
            ctx.move_to(-4, 3.25)
            ctx.rel_line_to(3.25, 0)
            ctx.stroke()
            ctx.move_to(2.75, -0.25)
            ctx.rel_line_to(4.25, 0)
            ctx.rel_line_to(-3, -3)
            ctx.stroke()
            pass
        # 4: start of rgb symbols: none
        # 5: rgb static
        elif symbol == 5:
            ctx.rgb(0, 0, 1)
            ctx.arc(0, -2.2, 2.5, 0, math.tau, 0).fill()
            ctx.rgb(0, 1, 0)
            ctx.arc(-1.5, 1.5, 2.5, 0, math.tau, 0).fill()
            ctx.rgb(1, 0, 0)
            ctx.arc(1.5, 1.5, 2.5, 0, math.tau, 0).fill()

    def draw_label(self, ctx, petal):
        if petal == PETAL_A:
            self.draw_symbol(ctx, 5)
        elif petal == PETAL_B:
            ctx.move_to(-4, -1.5)
            ctx.rel_line_to(2.5, 0)
            ctx.rel_line_to(3, 3)
            ctx.rel_line_to(2.5, 0)
            ctx.stroke()
            ctx.move_to(-4, 1.5)
            ctx.rel_line_to(2.5, 0)
            ctx.stroke()
            ctx.move_to(4, -1.5)
            ctx.rel_line_to(-2.5, 0)
            ctx.stroke()
        elif petal == PETAL_C:
            if self.current_page.highlight_is_playing and self.playing:
                ctx.move_to(-2, -3)
                ctx.rel_line_to(0, 6).stroke()
                ctx.move_to(2, -3)
                ctx.rel_line_to(0, 6).stroke()
            else:
                ctx.move_to(-2, -3)
                ctx.rel_line_to(6, 3)
                ctx.rel_line_to(-6, 3)
                ctx.rel_line_to(0, -6)
                ctx.stroke()
        elif petal == PETAL_D:
            ctx.arc(-1, -1, 2, 0, math.tau, 0).stroke()
            ctx.move_to(1, 1)
            ctx.rel_line_to(2, 2).stroke()
        elif petal == PETAL_E:
            ctx.save()
            ctx.line_width = 2
            ctx.move_to(-3, 1)
            ctx.rel_line_to(0, -2).stroke()
            ctx.move_to(0, 1)
            ctx.rel_line_to(0, -2).stroke()
            ctx.move_to(3, 1)
            ctx.rel_line_to(0, -2).stroke()
            ctx.restore()

    def background_think(self, ins, delta_ms):
        # handle playlist continuity
        if self.playing and media.get_position() >= media.get_duration():
            media.stop()
            self.playing = False
            if self.playback_mode and self.active_songlist:
                index = self.active_songlist_index
                if self.playback_mode == 2:
                    index += 1
                elif self.playback_mode == 3:
                    rng = random.randint(0, len(self.active_songlist) - 2)
                    index = -1 if rng == index else rng
                index %= len(self.active_songlist)
                self.active_songlist_index = index
                if index is not None:
                    self.play_song(self.active_songlist, self.active_songlist_index)

    def run_scan(self):
        def song_key(thing):
            if thing.artist is None:
                return (1, thing.path)
            album = "" if thing.album is None else thing.album
            return (0, thing.artist, thing.year, album, thing.track_number, thing.path)

        def artist_key(thing):
            return (thing.artist is None, thing.artist)

        try:
            song = next(self.scan_generator)
        except StopIteration:
            self.scan_generator = None
            return
        pos = self.media_page.insort(song, key=song_key, unique=True)
        if (
            pos is not None
            and self.active_songlist is self.mp3_files
            and pos <= self.active_songlist_index
        ):
            self.active_songlist_index += 1
        self.artists_page.insort(Artist(song.artist), key=artist_key, unique=True)

    def update_leds(self, delta_ms):
        if self.orig_brightness is None:
            return
        if not self.led_mode:
            leds.set_brightness(0)
            return
        if self.led_mode == 1:
            leds.set_brightness(self.orig_brightness)
        else:
            # future plans
            pass
        if self.led_pattern_request:
            led_patterns.pretty_pattern()
            leds.update()
            self.led_pattern_request = False

    def think(self, ins, delta_ms):
        super().think(ins, delta_ms)
        media.think(delta_ms)
        self.update_leds(delta_ms)
        self.draw_ms += delta_ms

        # if there's directories to be scanned do it iteratively now
        if self.scan_generator:
            self.run_scan()

        # handle end of song
        self.background_think(ins, delta_ms)

        # handle pending saves
        if self.save_request:
            self.save_timer += delta_ms
            if self.save_timer > 7000:
                self.save_data()
        else:
            self.save_timer = 0

        # handle lock
        if (
            self.input.buttons.app.middle.repeated
            and not self.app_button_release_ignore
        ):
            self.captouch_locked = not self.captouch_locked
            self.app_button_release_ignore = True
            self.full_redraw = True

        # handle shoulder button play/pause
        if self.input.buttons.app.middle.released:
            if self.app_button_release_ignore:
                self.app_button_release_ignore = False
            elif self.active_song is not None:
                if self.playing:
                    media.pause()
                else:
                    media.play()
                self.playing = not self.playing
            else:
                self.play_song(self.current_page.itemlist, self.current_page.highlight)

        # handle playlist scroll
        if self.song_reset_cooldown:
            self.song_reset_cooldown -= delta_ms
            if self.song_reset_cooldown < 0:
                self.song_reset_cooldown = 0

        lr_dir = self.input.buttons.app.right.released
        lr_dir -= self.input.buttons.app.left.released
        if lr_dir:
            if self.app_button_release_ignore:
                self.app_button_release_ignore = False
            elif self.active_songlist:
                index = self.active_songlist_index
                if self.playback_mode == 3:
                    if lr_dir > 0:
                        rng = random.randint(0, len(self.active_songlist) - 2)
                        index = -1 if rng == index else rng
                else:
                    if lr_dir < 1 and not self.song_reset_cooldown:
                        self.song_reset_cooldown += 7000
                    else:
                        index += lr_dir
                index %= len(self.active_songlist)
                self.play_song(self.active_songlist, index)

        # handle playlist seek
        lr_dir = self.input.buttons.app.right.repeated
        lr_dir -= self.input.buttons.app.left.repeated
        if lr_dir:
            self.app_button_release_ignore = True
            dur = media.get_duration()
            if dur:
                # attempt to scroll 5s, cross ur heart and hope to die
                incr = 5 * 16000 / dur
                media.seek(media.get_position() / dur + incr * lr_dir)
                # time api stops working correctly after seek. this
                # is a backend issue and should be fixed there.
                # best we can do here is estimates, and not sure if
                # that's worth the effort.
                self.time_ok = False

        if not ins.buttons.app:
            self.app_button_release_ignore = False

        # handle petal 5 load/play/pause
        if (
            not self.captouch_locked
            and self.input.captouch.petals[PETAL_C].whole.pressed
        ):
            self.label_state[PETAL_C] = None
            if self.current_page.highlight_is_playing:
                if self.playing:
                    media.pause()
                else:
                    media.play()
                self.playing = not self.playing
            else:
                self.play_song(self.current_page.itemlist, self.current_page.highlight)

        # page swaps
        if not self.captouch_locked and not self.label_state[PETAL_E]:
            playlist_page_active = self.current_page == self.playlist_page
            if self.input.captouch.petals[PETAL_D].whole.pressed:
                playlist_page_active = not playlist_page_active

            if playlist_page_active:
                new_page = self.playlist_page
            elif ins.captouch.petals[PETAL_A].pressed:
                new_page = self.artists_page
            else:
                new_page = self.media_page
            old_page = self.current_page

            if new_page is not old_page:
                if not (self.artists_page in [new_page, old_page]):
                    new_page.label_state[PETAL_D] = None
                elif (
                    self.media_page in [new_page, old_page]
                    and old_page.itemlist
                    and new_page.itemlist
                ):
                    # scrolls to current artist in other page
                    artist = old_page.itemlist[old_page.highlight].artist
                    if not new_page.itemlist[new_page.highlight].artist == artist:
                        for x, song in enumerate(new_page.itemlist):
                            if song.artist == artist:
                                new_page.highlight = x
                                break
                self.full_redraw = True
                self.current_page = new_page

        # scrolling thingies
        cw_dir = 0
        if not self.captouch_locked:
            cw_dir -= self.input.captouch.petals[0].whole.pressed
            cw_dir += self.input.captouch.petals[4].whole.pressed
            cw_dir += self.input.captouch.petals[6].whole.pressed
        overflow = bool(cw_dir)
        # run even when captouch is locked to not have discontinuity
        for thingy in self.scroll_thingies:
            thingy.think(ins, delta_ms)
            if not self.captouch_locked:
                cw_dir += thingy.events
            thingy.events = 0

        if cw_dir:
            self.current_page.scroll(cw_dir, overflow)

        # handle alt function for bottom petals
        if self.captouch_locked:
            self.label_state[PETAL_E] = False
        else:
            self.label_state[PETAL_E] = ins.captouch.petals[PETAL_E].pressed
        if self.label_state[PETAL_E]:
            if self.input.captouch.petals[PETAL_B].whole.pressed:
                self.playback_mode += 1
                self.playback_mode %= 4
                self.label_state[PETAL_B] = None
                self.save_request = True
            if self.input.captouch.petals[PETAL_A].whole.pressed:
                self.led_mode += 1
                self.led_mode %= 2
                self.label_state[PETAL_A] = None
                self.save_request = True
            if (
                self.input.captouch.petals[PETAL_D].whole.pressed
                and self.active_songlist
                and self.active_song
            ):
                index = None
                if self.current_page is self.artists_page:
                    for x, artist in enumerate(self.artists):
                        if self.active_song.artist == artist:
                            index = x
                            break
                elif self.active_songlist is self.current_page.itemlist:
                    index = self.active_songlist_index
                else:
                    try:
                        index = self.current_page.itemlist.index(self.active_song)
                    except ValueError:
                        pass
                if index is not None:
                    self.current_page.highlight = index
                    self.label_state[PETAL_D] = None

        # dirty hack:
        # pretend captouch is locked if it's active.
        # this means other think functions like track
        # highlight advancing still work.
        captouch_locked = self.captouch_locked
        if self.label_state[PETAL_E]:
            self.captouch_locked = True

        self.current_page.think(ins, delta_ms)

        self.captouch_locked = captouch_locked

    def on_enter(self, vm):
        self.enter_done = False
        self.orig_brightness = leds.get_brightness()
        self.orig_slew_rate = leds.get_slew_rate()
        leds.set_slew_rate(min(140, self.orig_slew_rate))
        self.load_data()

    def on_enter_done(self):
        self.enter_done = True

    def on_exit(self):
        leds.set_brightness(self.orig_brightness)
        if self.save_request:
            self.save_data()


if __name__ == "__main__":
    import st3m.run

    st3m.run.run_app(App)
