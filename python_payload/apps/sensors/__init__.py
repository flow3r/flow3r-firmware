from st3m.application import Application, ApplicationContext
from st3m.goose import Dict, Any, List, Optional
from st3m.ui.view import View, ViewManager
from st3m.ui import colours
from st3m.input import InputState
from ctx import Context

import json
import math

import leds
from st3m.application import Application, ApplicationContext
from st3m.ui import widgets


class App(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.offset = -60
        self.interval = 20
        self.data_exists = False
        self.rotate = 0
        self.rot_velo = 0
        self.rot_mass = 2
        self.rot_friction = 0.93

        self.alt_slow_widget = widgets.Altimeter()
        self.alt_fast_widget = widgets.Altimeter(filter_stages=0)
        self.inc_widget = widgets.Inclinometer(buffer_len=1)
        self.widgets = [self.alt_slow_widget, self.alt_fast_widget, self.inc_widget]

        self.single_line_titles = [
            "pressure",
            "temperature",
            "battery voltage",
            "relative altitude",
        ]
        self.double_line_titles = ["accelerometer", "gyroscope"]
        self.units = ["m", "deg"]
        self.units = [f"(x, y, z) [{x}/s]" for x in self.units]

    def draw_background(self, ctx):
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        interval = self.interval
        counter = self.offset

        ctx.font = "Camp Font 2"
        ctx.text_align = ctx.RIGHT
        ctx.rgb(0.8, 0.8, 0.6)
        ctx.font_size = 22

        gap = -4

        for x in self.single_line_titles:
            ctx.move_to(gap, counter)
            counter += interval
            ctx.text(x)

        for x in self.double_line_titles:
            ctx.move_to(gap, counter)
            counter += 2 * interval
            ctx.text(x)

        gap = 4

        ctx.text_align = ctx.LEFT
        ctx.rgb(0.6, 0.7, 0.6)
        ctx.font = "Arimo Bold Italic"
        ctx.font_size = 14

        counter -= 4 * interval

        for x in self.units:
            ctx.move_to(gap, counter)
            counter += 2 * interval
            ctx.text(x)

    def draw(self, ctx: Context) -> None:
        azi = self.inc_widget.azimuth
        inc = self.inc_widget.inclination
        if azi is not None and inc is not None:
            delta = azi - self.rotate
            if delta > math.pi:
                delta -= math.tau
            elif delta < -math.pi:
                delta += math.tau
            # normalize to 1
            delta /= math.pi
            inc /= math.pi
            if delta > 0:
                self.rot_velo += delta * (1 - delta) * inc * (1 - inc)
            else:
                delta = -delta
                self.rot_velo -= delta * (1 - delta) * inc * (1 - inc)
            self.rotate += self.rot_velo / self.rot_mass
            self.rotate = self.rotate % math.tau
            self.rot_velo *= self.rot_friction
            ctx.rotate(-self.rotate)

        self.draw_background(ctx)
        if not self.data_exists:
            return

        ctx.font = "Camp Font 2"
        ctx.font_size = 25

        counter = self.offset
        interval = self.interval

        gap = 4

        ctx.rgb(0.5, 0.8, 0.8)

        single_lines = [
            str(self.pressure / 100)[:6] + "hPa",
            str(self.temperature)[:5] + "degC",
            "n/a",
            str(self.altitude)[:6] + "m",
        ]

        if self.battery_voltage is not None:
            single_lines[2] = str(self.battery_voltage)[:5] + "V"

        for x in single_lines:
            ctx.move_to(gap, counter)
            counter += interval
            ctx.text(x)

        counter += interval
        ctx.text_align = ctx.MIDDLE

        double_lines = [
            ", ".join([str(y)[:4] for y in x]) for x in [self.acc, self.gyro]
        ]

        for x in double_lines:
            ctx.move_to(0, counter)
            counter += 2 * interval
            ctx.text(f"({x})")

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        for widget in self.widgets:
            widget.think(ins, delta_ms)

        self.pressure = ins.imu.pressure
        self.battery_voltage = ins.battery_voltage
        self.temperature = ins.temperature
        self.acc = ins.imu.acc
        self.gyro = ins.imu.gyro
        self.altitude = self.alt_fast_widget.meters_above_sea
        self.data_exists = True

        led_altitude = self.alt_slow_widget.meters_above_sea
        if led_altitude is not None:
            hue = 6.28 * (led_altitude % 1.0)
            leds.set_all_rgb(*colours.hsv_to_rgb(hue, 1, 1))
            leds.update()

    def on_enter(self, vm: Optional[ViewManager]) -> None:
        super().on_enter(vm)
        for widget in self.widgets:
            widget.on_enter()
        self.data_exists = False

    def on_exit(self):
        super().on_exit()
        for widget in self.widgets:
            widget.on_exit()

    def get_help(self):
        ret = (
            "Shows output of several onboard sensors. LED hue changes with smoothed "
            "relative altitude and goes full circle on a 1m height difference."
            "The Screen rotates to an upright position.\n\n"
            "The sensor data shown on the display is purposefully unfiltered to "
            "reflect the expected noise floor when using the raw data."
        )
        return ret


# For running with `mpremote run`:
if __name__ == "__main__":
    import st3m.run

    st3m.run.run_view(App(ApplicationContext()))
