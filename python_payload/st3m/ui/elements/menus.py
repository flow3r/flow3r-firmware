from st3m.goose import List
from st3m.ui.elements.visuals import Sun, GroupRing, FlowerIcon
from st3m.ui.menu import MenuController, MenuItem
from st3m.ui import widgets
from st3m.ui.view import ViewTransitionSwipeRight

from st3m import InputState

from st3m.utils import lerp, tau
import math
from ctx import Context
import captouch


class SimpleMenu(MenuController):
    """
    A simple line-by-line menu.
    """

    SIZE_LARGE = 30
    SIZE_SMALL = 20

    def draw(self, ctx: Context) -> None:
        ctx.gray(0)
        ctx.rectangle(-120, -120, 240, 240).fill()

        current = self._scroll_controller.current_position()

        ctx.gray(1)
        for ix, item in enumerate(self._items):
            offs = (ix - current) * self.SIZE_LARGE
            if offs < -120 - self.SIZE_LARGE or offs > 120:
                continue

            ctx.save()
            ctx.move_to(0, 0)
            ctx.font_size = self.SIZE_LARGE
            ctx.text_align = ctx.CENTER
            ctx.text_baseline = ctx.MIDDLE
            ctx.translate(0, offs)
            scale = lerp(
                1, self.SIZE_SMALL / self.SIZE_LARGE, abs(offs / self.SIZE_SMALL)
            )
            ctx.scale(scale, scale)
            item.draw(ctx)
            ctx.restore()

    def get_help(self):
        ret = (
            "You can scroll though this menu with the app button left and right. To go "
            "back use the os button middle, to go forward use the app button middle."
        )
        return ret


class OSMenu(SimpleMenu):
    def get_help(self):
        if self.captouch_active:
            ret = (
                "You can scroll though this menu by swiping across petals 2 and 8 or by "
                "tapping the other top petals or the app button left/right. To go back use "
                "petals 1 or 9 or the os button middle, to go forward use petals 3 and 7 "
                "or the app button middle."
            )
        else:
            ret = super().get_help()
        return ret

    class _FakeScrollController:
        def __init__(self, scroller):
            self._scroller = scroller

        def think(self, ins, delta_ms):
            pass

        def target_position(self):
            return self._scroller.item

        def current_position(self):
            return self._scroller.pos

        def at_left_limit(self):
            return not self._scroller.item

        def at_right_limit(self):
            return self._scroller.item == self._scroller.max_item

        def scroll_left(self):
            self._scroller.item -= 1

        def scroll_right(self):
            self._scroller.item += 1

        def set_item_count(self, count):
            if count:
                count -= 1
            self._scroller.max_item = count

        def scroll_to(self, position):
            self._scroller.item = int(position)

        def set_position(self, position):
            self._scroller.item = int(position)
            self._scroller.pos = int(position)

    def __init__(self, items: List[MenuItem]) -> None:
        super().__init__(items)
        self.captouch_config = captouch.Config.empty()
        for x in range(10):
            self.captouch_config.petals[x].set_min_mode(1)
        self.captouch_config.petals[5].mode = 0

        self._scroller = widgets.MultiItemScroller(
            self.captouch_config, petals=[2, 8], friction=1
        )
        self._scroller.max_item = len(items) - 1
        self._scroll_controller = self._FakeScrollController(self._scroller)
        self.sensitivity = 1.5
        self.captouch_active = True

    @property
    def captouch_active(self):
        return self._captouch_active

    @captouch_active.setter
    def captouch_active(self, value):
        # hacky
        if value:
            self._scroller.gain = complex(self.sensitivity)
        else:
            self._scroller.gain = 0
        self._captouch_active = value

    def on_enter(self, vm):
        super().on_enter(vm)
        self.captouch_config.apply()
        self._scroller.on_enter()

    def on_exit(self):
        super().on_exit()
        self._scroller.on_exit()

    def think(self, ins: InputState, delta_ms: int) -> None:
        if self.captouch_active:
            buttons = self.input.captouch.petals[4].whole.pressed
            buttons += self.input.captouch.petals[6].whole.pressed
            buttons -= self.input.captouch.petals[0].whole.pressed
            self._scroller.item += buttons

        self._scroller.think(ins, delta_ms)
        super().think(ins, delta_ms)

        if self.captouch_active:
            if any([self.input.captouch.petals[x].whole.pressed for x in [3, 7]]):
                self.select()
            if self.vm is not None and any(
                [self.input.captouch.petals[x].whole.pressed for x in [1, 9]]
            ):
                self.vm.pop(ViewTransitionSwipeRight())

    def draw(self, ctx: Context) -> None:
        ctx.gray(0)
        ctx.rectangle(-120, -120, 240, 240).fill()

        current = self._scroll_controller.current_position()

        for ix, item in enumerate(self._items):
            offs = (ix - current) * self.SIZE_LARGE
            if offs < -120 - self.SIZE_LARGE or offs > 120:
                continue
            if ix == self._scroll_controller.target_position():
                ctx.gray(1)
            else:
                ctx.gray(0.8)

            ctx.save()
            ctx.move_to(0, 0)
            ctx.font_size = self.SIZE_LARGE
            ctx.text_align = ctx.CENTER
            ctx.text_baseline = ctx.MIDDLE
            ctx.translate(0, offs)
            scale = lerp(
                1, self.SIZE_SMALL / self.SIZE_LARGE, abs(offs / self.SIZE_SMALL)
            )
            ctx.scale(scale, scale)
            item.draw(ctx)
            ctx.restore()


class SunMenu(OSMenu):
    """
    A circular menu with a rotating sun.
    """

    __slots__ = (
        "_ts",
        "_sun",
    )

    def __init__(self, items: List[MenuItem]) -> None:
        self._ts = 0
        self._sun = Sun()
        super().__init__(items)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self._sun.think(ins, delta_ms)
        self._ts += delta_ms

    def _draw_item_angled(
        self,
        ctx: Context,
        item: MenuItem,
        angle: float,
        activity: float,
        highlight: bool = False,
    ) -> None:
        if highlight:
            color = 1
        else:
            color = lerp(0, 0.8, activity)
        if color < 0.01:
            return
        size = lerp(20, 40, activity)

        ctx.save()
        ctx.translate(-120, 0).rotate(angle).translate(140, 0)
        ctx.font_size = size
        ctx.rgba(1.0, 1.0, 1.0, color).move_to(0, 0)
        item.draw(ctx)
        ctx.restore()

    def draw(self, ctx: Context) -> None:
        ctx.gray(0)
        ctx.rectangle(-120, -120, 240, 240).fill()

        self._sun.draw(ctx)

        ctx.font_size = 40
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE

        angle_per_item = 0.4

        current = self._scroll_controller.current_position()

        for ix, item in enumerate(self._items):
            rot = (ix - current) * angle_per_item
            highlight = ix == self._scroll_controller.target_position()
            self._draw_item_angled(ctx, item, rot, 1 - abs(rot), highlight)


class FlowerMenu(OSMenu):
    """
    A circular menu with flowers.
    """

    __slots__ = (
        "_ts",
        "_sun",
    )

    def __init__(self, items: List[MenuItem], name: str = "flow3r") -> None:
        self._ts = 0
        self.name = name
        self.ui = GroupRing(r=80)
        for item in items:
            self.ui.items_ring.append(FlowerIcon(label=item.label()))
        super().__init__(items)

        self.icon = FlowerIcon(label=self.name)
        self.icon.rotation_time = -5000
        self.ui.item_center = self.icon

        self.angle = 0
        self.angle_step = 0.2

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self.ui.think(ins, delta_ms)
        self._ts += delta_ms

    def draw(self, ctx: Context) -> None:
        ctx.gray(0)
        ctx.rectangle(-120, -120, 240, 240).fill()
        # for item in self.ui.items_ring:
        #    item.highlighted = False
        #    item.rotation_time = 10000
        current = self._scroll_controller.current_position()
        current_int = round(current) % len(self._items)
        # print("current", current, current_int)
        # self.ui.items_ring[current_int].highlighted = True
        # self.ui.items_ring[current_int].rotation_time = 3000
        self.ui.angle_offset = math.pi - (tau * current / len(self.ui.items_ring))

        self.ui.draw(ctx)
