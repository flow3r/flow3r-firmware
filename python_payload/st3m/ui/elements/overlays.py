"""
Composition/overlay system.

This is different from a menu system. Instead of navigation, it should be used
for persistent, anchored symbols like charging symbols, toasts, debug overlays,
etc.
"""

from st3m import Responder, InputState, Reactor, settings
from st3m.goose import Dict, Enum, List, ABCBase, abstractmethod, Optional
from st3m.utils import tau
from st3m.ui.view import ViewManager
from st3m.input import power, InputController
import st3m.power
from ctx import Context
import st3m.wifi
from st3m.application import viewmanager_is_in_application
from st3m.ui.help import Help
from st3m.ui.mixer import AudioMixer

import math
import sys_audio
import sys_kernel
import sys_display
import network


class OverlayKind(Enum):
    # Battery, USB, ...
    Indicators = 0
    # Naughty debug developers for naughty developers and debuggers.
    Debug = 1
    Touch = 2
    SystemMenu = 3
    Volume = 4


_all_kinds = [
    OverlayKind.Indicators,
    OverlayKind.Debug,
    OverlayKind.Touch,
    OverlayKind.SystemMenu,
    OverlayKind.Volume,
]


class Region:
    x0: int
    y0: int
    x1: int
    y1: int

    def __init__(self, *args):
        if not args:
            self.clear()
        else:
            self.set(*args)

    def set(self, x0, y0, x1, y1):
        self._cleared = False
        self.x0 = x0
        self.y0 = y0
        self.x1 = x1
        self.y1 = y1

    def add(self, x0, y0, x1, y1):
        if self._cleared:
            self.set(x0, y0, x1, y1)
        else:
            self.x0 = min(self.x0, x0)
            self.y0 = min(self.y0, y0)
            self.x1 = max(self.x1, x1)
            self.y1 = max(self.y1, y1)

    def add_region(self, rect):
        self.add(rect.x0, rect.y0, rect.x1, rect.y1)

    def clear(self):
        self.x0 = self.y0 = self.x1 = self.y1 = -120
        self._cleared = True

    def fill(self, ctx):
        ctx.rectangle(self.x0, self.y0, self.x1 - self.x0, self.y1 - self.y0)
        ctx.fill()

    def is_empty(self):
        return self.x1 == self.x0 or self.y1 == self.y0

    def set_clip(self):
        sys_display.overlay_clip(
            self.x0 + 120, self.y0 + 120, self.x1 + 120, self.y1 + 120
        )

    def copy(self, rect):
        self.set(rect.x0, rect.y0, rect.x1, rect.y1)

    def __eq__(self, rect):
        return (
            self.x0 == rect.x0
            and self.x1 == rect.x1
            and self.y0 == rect.y0
            and self.y1 == rect.y1
        )

    def __repr__(self):
        return f"({self.x0} x {self.y0} - {self.x1} x {self.y1})"


class Overlay(Responder):
    """
    An Overlay is a Responder with some kind of OverlayKind attached.
    """

    kind: OverlayKind

    def needs_redraw(rect: Region) -> bool:
        rect.add(-120, -120, 120, 120)
        return True


class Compositor(Responder):
    """
    The Compositor blends together the system ViewManager with active Overlays.
    Most Overlays can be enabled/disabled by kind.

    It also handles the os button (volume control, go back, system menu).
    """

    def __init__(self, main: ViewManager):
        self.main = main
        self.overlays: Dict[OverlayKind, List[Responder]] = {}
        self.enabled: Dict[OverlayKind, bool] = {
            OverlayKind.Indicators: True,
            OverlayKind.Debug: True,
            OverlayKind.Volume: True,
        }
        self._last_fps_string = ""
        self._clip_rect = Region()
        self._last_clip = Region(-120, -120, 120, 120)
        self._display_mode = None
        self._enabled: List[Responder] = []
        self._last_enabled: List[Responder] = []
        self._redraw_pending = 0

        self.input = InputController()
        self.input.buttons.os.middle.repeat_enable(1000, 1000)

        self._system_menu = OverlaySystemMenu(self.input, self)
        self.add_overlay(self._system_menu)

        self._volume = OverlayVolume(self.input)
        self.add_overlay(self._volume)

    def get_help(self):
        return self.main.get_help()

    def _enabled_overlays(self) -> List[Responder]:
        res: List[Responder] = []
        for kind in _all_kinds:
            if kind == OverlayKind.SystemMenu:
                if not self._system_menu.active:
                    continue
            elif self._system_menu.sub and kind < OverlayKind.SystemMenu:
                continue
            elif not self.enabled.get(kind, False):
                continue
            if kind == OverlayKind.Indicators and not self.main.wants_icons():
                continue
            for overlay in self.overlays.get(kind, []):
                res.append(overlay)
        return res

    def go_home(self):
        # should go all the way to the bottom of the stack
        # but we don't trust the system not to inf loop so
        # we're limiting depth to Big Number (100) for now
        self.main.pop(depth=100)

    def exit_app(self):
        # we don't trust the system not to inf loop so
        # we're limiting depth to Big Number (100) for now
        for _ in range(100):
            if not viewmanager_is_in_application(self.main):
                return
            self.main.exit_view()

    def close_system_menu(self):
        self.main._ignore_pressed()
        self._system_menu.active = False

    def think(self, ins: InputState, delta_ms: int) -> None:
        self.input.think(ins, delta_ms)
        # tell the volume overlay whether to react to
        # os_button input or not
        self._volume.override_os_button_volume = (
            self.main.override_os_button_volume and not self._system_menu.active
        )

        # tell the, uh, back-going-feature whether to react
        # to os_button input or not
        if (
            self.input.buttons.os.middle.released
            and not self._system_menu.active
            and not self.main.override_os_button_back
        ):
            self.main.exit_view()

        if self.input.buttons.os.middle.repeated:
            self._system_menu.in_app = viewmanager_is_in_application(self.main)
            self._system_menu.open()

        if not self._system_menu.active:
            self.main.think(ins, delta_ms)
        if sys_display.get_mode() & sys_display.osd == 0 or (
            settings.onoff_show_fps.value and not self._system_menu.active
        ):
            return
        self._enabled = self._enabled_overlays()
        for i in range(len(self._enabled)):
            self._enabled[i].think(ins, delta_ms)

    def draw(self, ctx: Context) -> None:
        if not self._system_menu.active:
            self.main.draw(ctx)
        display_mode = sys_display.get_mode()
        redraw = False
        if self._redraw_pending:
            redraw = True
            self._redraw_pending -= 1
        if display_mode != self._display_mode:
            self._redraw_pending = 4
        self._display_mode = display_mode
        if (display_mode & sys_display.osd) == 0:
            return
        if settings.onoff_show_fps.value and not self._system_menu.active:
            fps_string = "{0:.1f}".format(sys_display.fps())
            if redraw or fps_string != self._last_fps_string:
                octx = sys_display.ctx(sys_display.osd)
                self._last_fps_string = fps_string
                self._clip_rect.set(-120, -120, 120, -96)
                self._last_clip.add_region(self._clip_rect)
                if self._last_clip != self._clip_rect:
                    octx.save()
                    octx.compositing_mode = octx.CLEAR
                    self._last_clip.fill(octx)
                    octx.restore()
                octx.save()
                octx.rgba(0, 0, 0, 0.5)
                octx.compositing_mode = octx.COPY
                self._clip_rect.fill(octx)
                octx.restore()
                octx.gray(1)
                octx.font_size = 18
                octx.font = "Bold"
                octx.move_to(0, -103)
                octx.text_align = octx.CENTER
                octx.text(fps_string)
                sys_display.update(octx)
                self._clip_rect.set_clip()
                self._last_clip.copy(self._clip_rect)
            self._last_enabled = []
        else:
            self._last_fps_string = ""
            self._clip_rect.clear()
            for i in range(len(self._enabled)):
                redraw = (
                    self._enabled[i].needs_redraw(self._clip_rect)
                    or redraw
                    or self._enabled[i] not in self._last_enabled
                )
            # catching partial redraws here
            if self._system_menu.sub:
                if not self._clip_rect.is_empty():
                    self._system_menu.sub.full_redraw = True
                self._clip_rect.add(-120, -120, 120, 120)
            for i in range(len(self._last_enabled)):
                redraw = redraw or self._last_enabled[i] not in self._enabled
            self._last_enabled = self._enabled
            if self._clip_rect.is_empty():
                self._clip_rect.set_clip()
                return
            if self._last_clip != self._clip_rect:
                redraw = True
            if not redraw:
                return
            self._last_clip.add_region(self._clip_rect)
            octx = sys_display.ctx(sys_display.osd)
            if not self._system_menu.sub:
                octx.save()
                octx.compositing_mode = octx.CLEAR
                self._last_clip.fill(octx)
                octx.restore()
            for i in range(len(self._enabled)):
                self._enabled[i].draw(octx)
            sys_display.update(octx)
            self._clip_rect.set_clip()
            self._last_clip.copy(self._clip_rect)

    def add_overlay(self, ov: Overlay) -> None:
        """
        Add some Overlay to the Compositor. It will be drawn if enabled.
        """
        if ov.kind not in self.overlays:
            self.overlays[ov.kind] = []
        self.overlays[ov.kind].append(ov)


class DebugFragment(ABCBase):
    """
    Something which wishes to provide some text to the OverlayDebug.
    """

    @abstractmethod
    def text(self) -> str:
        ...


class DebugReactorStats(DebugFragment):
    """
    DebugFragment which provides the OverlayDebug with information about the
    active reactor.
    """

    def __init__(self, reactor: Reactor):
        self._stats = reactor.stats

    def text(self) -> str:
        res = []

        rts = self._stats.run_times
        if len(rts) > 0:
            avg = sum(rts) / len(rts)
            res.append(f"tick: {int(avg)}ms")

        rts = self._stats.render_times
        if len(rts) > 0:
            avg = sum(rts) / len(rts)
            res.append(f"fps: {int(1/(avg/1000))}")

        return " ".join(res)


class DebugBattery(DebugFragment):
    """
    DebugFragment which provides the OverlayDebug with information about the
    battery voltage.
    """

    def text(self) -> str:
        v = power.battery_voltage
        return f"bat: {v:.2f}V"


class OverlayDebug(Overlay):
    """
    Overlay which renders a text bar at the bottom of the screen with random
    bits of trivia.
    """

    kind = OverlayKind.Debug

    def __init__(self) -> None:
        self.fragments: List[DebugFragment] = []

    def add_fragment(self, f: DebugFragment) -> None:
        self.fragments.append(f)

    def think(self, ins: InputState, delta_ms: int) -> None:
        pass

    def text(self) -> str:
        text = [f.text() for f in self.fragments if f.text() != ""]
        return " ".join(text)

    def draw(self, ctx: Context) -> None:
        ctx.save()
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font = ctx.get_font_name(0)
        ctx.font_size = 10
        ctx.gray(0).rectangle(-120, 80, 240, 12).fill()
        ctx.gray(1).move_to(0, 86).text(self.text())
        ctx.restore()

    def needs_redraw(self, rect: Region) -> bool:
        rect.add(-120, 80, 120, 92)
        return True


class OverlaySystemMenu(Overlay):
    kind = OverlayKind.SystemMenu

    def __init__(self, inputcontroller, methodprovider) -> None:
        self.input = inputcontroller
        self.exit_app = methodprovider.exit_app
        self.close_menu = methodprovider.close_system_menu
        self.go_home = methodprovider.go_home
        self.get_help = methodprovider.get_help
        self._menu_pos = 0
        self._os_menu_entries = [
            "resume",
            "help",
            "mixer",
            "go home",
        ]
        self._app_menu_entries = [
            "resume",
            "help",
            "mixer",
            "exit app",
        ]
        self.active = False
        self.sub = None

    def open(self):
        self._menu_pos = 0
        self.latch = True
        self.active = True
        if self.in_app:
            self.entries = self._app_menu_entries
        else:
            self.entries = self._os_menu_entries

    def think(self, ins: InputState, delta_ms: int) -> None:
        if self.input.buttons.os.middle.released:
            if self.latch:
                self.latch = False
            elif self.sub is not None:
                if not self.sub.override_os_button_back:
                    self.sub.on_exit()
                    self.sub = None
            else:
                self.close_menu()

        if self.sub:
            self.sub.think(ins, delta_ms)
            return

        if self.input.buttons.app.middle.released:
            if self._menu_pos == 0:
                self.close_menu()
            elif self._menu_pos == 1:
                self.sub = Help(self.input, self.get_help())
            elif self._menu_pos == 2:
                self.sub = AudioMixer(self.input)
            elif self.in_app:
                self.exit_app()
                self.close_menu()
            else:
                self.go_home()
                self.close_menu()
        self._menu_pos += self.input.buttons.app.right.pressed
        self._menu_pos -= self.input.buttons.app.left.pressed
        self._menu_pos %= len(self.entries)

    def draw(self, ctx: Context) -> None:
        if self.sub is not None:
            self.sub.draw(ctx)
            return
        fg = (0x81 / 255, 0xCD / 255, 0xC6 / 255)
        bg = (0, 0, 0)
        ctx.rgb(*bg)
        ctx.round_rectangle(-42, -47, 84, 94, 5).fill()
        ctx.rgb(*fg)
        ctx.round_rectangle(-42, -47, 84, 94, 5).stroke()
        ctx.font_size = 18
        ypos = -40
        ctx.text_align = ctx.CENTER
        for x, entry in enumerate(self.entries):
            if self._menu_pos == x:
                ctx.rgb(*fg)
                ctx.round_rectangle(-35, ypos, 70, 20, 3).fill()
                ctx.rgb(*bg)
            else:
                ctx.rgb(*fg)
            ctx.move_to(0, ypos + 15)
            ctx.text(entry)
            ypos += 20

    def needs_redraw(self, rect: Region) -> bool:
        if self.sub is not None:
            pass
            # adding it later in the pipeline for partial redraw reasons
            # rect.add(-120, -120, 120, 120)
        else:
            rect.add(-44, -49, 44, 49)
        return True


class OverlayCaptouch(Overlay):
    kind = OverlayKind.Touch

    class Dot(Responder):
        def __init__(self, ix: int) -> None:
            self.ix = ix
            self.phi = 0.0
            self.rad = 0.0
            self.pressed = False

        def think(self, s: InputState, delta_ms: int) -> None:
            self.pressed = s.captouch.petals[self.ix].pressed
            (rad, phi) = s.captouch.petals[self.ix].position
            self.phi = phi
            self.rad = rad

        def draw(self, ctx: Context) -> None:
            if not self.pressed:
                return

            a = (tau / 10) * self.ix
            ctx.rotate(a)
            ctx.translate(0, -80)

            offs_x = self.phi / 1000
            offs_y = -self.rad / 1000
            ctx.rectangle(-5 + offs_x, -5 + offs_y, 10, 10)
            ctx.fill()

    def __init__(self) -> None:
        self.dots = [self.Dot(i) for i in range(10)]

    def think(self, ins: InputState, delta_ms: int) -> None:
        for dot in self.dots:
            dot.think(ins, delta_ms)

    def draw(self, ctx: Context) -> None:
        ctx.rgb(1, 0, 1)
        for dot in self.dots:
            ctx.save()
            dot.draw(ctx)
            ctx.restore()

    def needs_redraw(self, rect: Region) -> bool:
        rect.add(-120, -120, 120, 120)
        return True


class OverlayVolume(Overlay):
    """
    Renders a black square with icon and volume slider.

    Icon depends on whether headphones are plugged in or not.
    """

    kind = OverlayKind.Volume

    def __init__(self, inputcontroller) -> None:
        self.input = inputcontroller
        self._showing: Optional[int] = None
        self._volume = 0.0
        self._headphones = False
        self._muted = False

        self._started = False
        self.override_os_button_volume = False

    def think(self, ins: InputState, delta_ms: int) -> None:
        # Ignore first run cycle, to not show volume slider on startup.
        if not self._started:
            self._started = True
            return
        if self._showing is not None:
            self._showing -= delta_ms

        if not self.override_os_button_volume:
            prs = (
                self.input.buttons.os.right.pressed - self.input.buttons.os.left.pressed
            )
            if prs:
                sys_audio.adjust_volume_dB(settings.num_volume_step_db.value * prs)
                repeat = settings.num_volume_repeat_ms.value
                repeat_wait = settings.num_volume_repeat_wait_ms.value
                self._showing = max(1000, repeat_wait + 300)
                if prs == 1:
                    self.input.buttons.os.right.repeat_enable(repeat_wait, repeat)
                elif prs == -1:
                    self.input.buttons.os.left.repeat_enable(repeat_wait, repeat)
            rpt = (
                self.input.buttons.os.right.repeated
                - self.input.buttons.os.left.repeated
            )
            if rpt:
                sys_audio.adjust_volume_dB(
                    settings.num_volume_repeat_step_db.value * rpt
                )
                self._showing = max(1000, settings.num_volume_repeat_ms.value + 300)

        if self._showing is not None:
            self._volume = sys_audio.get_volume_relative()
            self._headphones = sys_audio.headphones_are_connected()
            self._muted = (self._volume == 0) or sys_audio.get_mute()

    def draw(self, ctx: Context) -> None:
        if self._showing is None:
            return

        opacity = self._showing / 200
        opacity = min(opacity, 0.8)

        # Background
        ctx.rgba(0, 0, 0, opacity)
        ctx.round_rectangle(-40, -40, 80, 80, 5)
        ctx.fill()

        # Foreground
        opacity = self._showing / 200
        opacity = max(min(opacity, 1), 0)

        muted = self._muted
        if muted:
            ctx.rgba(0.5, 0.5, 0.5, opacity)
        else:
            ctx.rgba(1.0, 1.0, 1.0, opacity)

        # Icon
        if self._headphones:
            ctx.round_rectangle(-20, -10, 5, 20, 3)
            ctx.fill()
            ctx.round_rectangle(15, -10, 5, 20, 3)
            ctx.fill()
            ctx.line_width = 3
            ctx.arc(0, -10, 17, tau / 2, tau, 0)
            ctx.stroke()
        else:
            ctx.move_to(-10, -10)
            ctx.line_to(0, -10)
            ctx.line_to(10, -20)
            ctx.line_to(10, 10)
            ctx.line_to(0, 0)
            ctx.line_to(-10, 0)
            ctx.fill()

        ctx.rgba(1.0, 1.0, 1.0, opacity)

        # Volume slider
        ctx.round_rectangle(-30, 20, 60, 10, 3)
        ctx.line_width = 2
        ctx.stroke()

        width = 60 * self._volume
        ctx.round_rectangle(-30, 20, width, 10, 3)
        ctx.fill()

    def needs_redraw(self, rect: Region) -> bool:
        if self._showing is not None:
            rect.add(-40, -40, 40, 40)
            if self._showing < 0:
                self._showing = None
        return self._showing is not None


class Icon(Responder):
    """
    A tray icon that might or might not be shown in the top icon tray.

    Should render into a 240x240 viewport. Will be scaled down by the IconTray
    that contains it.
    """

    WIDTH: int = 25
    _changed: bool = False

    @abstractmethod
    def visible(self) -> bool:
        ...

    def changed(self) -> bool:
        return self._changed


class USBIcon(Icon):
    """
    Found in the bargain bin at an Aldi Süd.

    Might or might not be related to a certain serial bus.
    """

    WIDTH: int = 30

    def visible(self) -> bool:
        return sys_kernel.usb_connected()

    def draw(self, ctx: Context) -> None:
        ctx.gray(1.0)
        ctx.arc(-90, 0, 20, 0, 6.28, 0).fill()
        ctx.line_width = 10.0
        ctx.move_to(-90, 0).line_to(90, 0).stroke()
        ctx.move_to(100, 0).line_to(70, 15).line_to(70, -15).line_to(100, 0).fill()
        ctx.move_to(-50, 0).line_to(-10, -40).line_to(20, -40).stroke()
        ctx.arc(20, -40, 15, 0, 6.28, 0).fill()
        ctx.move_to(-30, 0).line_to(10, 40).line_to(40, 40).stroke()
        ctx.rectangle(40 - 15, 40 - 15, 30, 30).fill()

    def think(self, ins: InputState, delta_ms: int) -> None:
        pass


class WifiIcon(Icon):
    WIDTH: int = 30

    def __init__(self) -> None:
        super().__init__()
        self._rssi: float = -120

    def visible(self) -> bool:
        return st3m.wifi.enabled()

    def draw(self, ctx: Context) -> None:
        ctx.gray(1.0)
        ctx.line_width = 10.0
        w = 1.5
        a = -w / 2 - 3.14 / 2
        b = w / 2 - 3.14 / 2

        r = self._rssi
        ctx.gray(1.0 if r > -75 else 0.2)
        ctx.arc(0, 65, 100, a, b, 0).stroke()
        ctx.gray(1.0 if r > -85 else 0.2)
        ctx.arc(0, 65, 70, a, b, 0).stroke()
        ctx.gray(1.0 if r > -95 else 0.2)
        ctx.arc(0, 65, 40, a, b, 0).stroke()

        self._changed = False

    def think(self, ins: InputState, delta_ms: int) -> None:
        rssi = st3m.wifi.rssi()
        if rssi != self._rssi:
            self._rssi = rssi
            self._changed = True


class BatteryIcon(Icon):
    WIDTH: int = 30

    def __init__(self) -> None:
        super().__init__()
        self._percent = 100

    def visible(self) -> bool:
        return power.has_battery

    def draw(self, ctx: Context) -> None:
        if self._percent > 30:
            ctx.rgb(0.17, 0.55, 0.04)
        else:
            ctx.rgb(0.52, 0.04, 0.17)

        height = 160 * self._percent / 100
        ctx.rectangle(-80, -50, height, 100)
        ctx.fill()

        ctx.gray(0.8)
        ctx.line_width = 10.0
        ctx.rectangle(80, -50, -160, 100)
        ctx.stroke()
        ctx.rectangle(100, -30, -20, 60)
        ctx.fill()

        ctx.font = ctx.get_font_name(1)
        ctx.move_to(-72, 32)
        ctx.font_size = 100
        ctx.rgb(255, 255, 255).text(str(self._percent))

        self._changed = False

    def think(self, ins: InputState, delta_ms: int) -> None:
        percent = power.battery_percentage
        if self._percent != percent:
            self._changed = True
            self._percent = percent


class ChargingIcon(Icon):
    WIDTH: int = 20

    def __init__(self) -> None:
        super().__init__()
        self._charging = power.battery_charging

    def visible(self) -> bool:
        if not power.has_battery:
            return False
        else:
            return power.battery_charging

    def draw(self, ctx: Context) -> None:
        ctx.rgb(255, 255, 255)
        ctx.gray(1)
        ctx.line_width = 20
        ctx.move_to(10, -65)
        ctx.line_to(-30, 20)
        ctx.line_to(30, -20)
        ctx.line_to(-10, 65)
        ctx.line_to(-20, 35)
        ctx.stroke()
        ctx.move_to(-10, 65)
        ctx.line_to(40, 35)
        ctx.stroke()

        self._changed = False

    def think(self, ins: InputState, delta_ms: int) -> None:
        charging = power.battery_charging
        if self._charging != charging:
            self._changed = True
            self._charging = charging


class IconTray(Overlay):
    """
    An overlay which renders Icons.
    """

    kind = OverlayKind.Indicators

    def __init__(self) -> None:
        self.icons = [
            ChargingIcon(),
            BatteryIcon(),
            USBIcon(),
            WifiIcon(),
        ]
        self.visible: List[Icon] = []
        self.last_visible: List[Icon] = []

    def think(self, ins: InputState, delta_ms: int) -> None:
        self.visible = [i for i in self.icons if i.visible()]
        for i in range(len(self.visible)):
            self.visible[i].think(ins, delta_ms)

    def draw(self, ctx: Context) -> None:
        self.last_visible = self.visible
        if not self.visible:
            return
        width = 0
        for i in range(len(self.visible)):
            width += self.visible[i].WIDTH
        x0 = width / -2 + self.visible[0].WIDTH / 2
        for i in range(len(self.visible)):
            ctx.save()
            ctx.translate(x0, -100)
            ctx.scale(0.13, 0.13)
            self.visible[i].draw(ctx)
            ctx.restore()
            x0 = x0 + self.visible[i].WIDTH

    def needs_redraw(self, rect: Region) -> bool:
        if self.visible:
            rect.add(-50, -120, 50, -88)
            for i in range(len(self.visible)):
                if (
                    self.visible[i].changed()
                    or self.visible[i] not in self.last_visible
                ):
                    return True
            for i in range(len(self.last_visible)):
                if self.last_visible[i] not in self.visible:
                    return True
        return False
