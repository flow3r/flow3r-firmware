import cmath
import math
import time
import sys_captouch
import captouch
import json
from st3m.ui.view import BaseView


class CaptouchCalibrator(BaseView):
    BASE_STATE = 0
    PRE_COUNTDOWN_STATE = 1
    COUNTDOWN_STATE = 2
    CALIBRATION_STATE = 3
    CALIBRATION_CHECK_STATE = 4
    SAVE_STATE = 5
    POST_SAVE_STATE = 6
    FIRST_BOOT_HINT_STATE = 7

    def __init__(self, menu=None):
        super().__init__()
        self.menu = menu
        self.vm = None
        self.timer = 0
        self.button = None
        self.calib_stage = -1
        self.calib_ok = None
        self.calib = None
        self.save_ok = None
        self.full_redraw = True
        self._selection_index = 0
        self.selections = []
        self.app_is_left = None
        self.calib_prev = None

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, val):
        if val == self.PRE_COUNTDOWN_STATE:
            self.selections = ["start 3s countdown"]
        elif val == self.CALIBRATION_CHECK_STATE:
            self.selections = ["redo", "save"]
        elif val == self.POST_SAVE_STATE:
            self.selections = ["ok"]
        elif val == self.FIRST_BOOT_HINT_STATE:
            self.selections = ["exit"]
        elif val == self.BASE_STATE:
            self.selections = ["clear", "calibrate"]
        else:
            self.selections = []
        self.selection_index = 0
        self._state = val
        self.pos = [None] * 10
        self.pos_prev = [None] * 10
        self.full_redraw = True

    @property
    def selection_index(self):
        return self._selection_index

    @selection_index.setter
    def selection_index(self, val):
        if self.selections:
            self._selection_index = val % len(self.selections)
        else:
            self._selection_index = None

    def restore_calib(self):
        if self.calib_prev is not None:
            sys_captouch.calibration_set_data(self.calib_prev)
            self.calib_prev = None

    def think(self, ins, delta_ms):
        super().think(ins, delta_ms)
        self.app_is_left = ins.buttons.app_is_left
        if self.button is not None:
            press_event = (self.button != ins.buttons.app) and ins.buttons.app
        else:
            press_event = False
        self.button = int(ins.buttons.app)
        self.draw_time += delta_ms

        if self.state in [self.BASE_STATE, self.CALIBRATION_CHECK_STATE]:
            for x in range(10):
                self.pos[x] = ins.captouch.petals[x].pos

        if self.state == self.BASE_STATE:
            if press_event:
                if self.button == ins.buttons.PRESSED_DOWN:
                    if self.selection_index:
                        self.state = self.PRE_COUNTDOWN_STATE
                    else:
                        self.state = self.BASE_STATE
                else:
                    self.selection_index += self.button
        elif self.state == self.PRE_COUNTDOWN_STATE:
            if press_event and self.button == ins.buttons.PRESSED_DOWN:
                self.state = self.COUNTDOWN_STATE
                self.timer = 2999
        elif self.state == self.COUNTDOWN_STATE:
            self.timer -= delta_ms
            if self.timer < 0:
                self.state = self.CALIBRATION_STATE
                self.calib_prev = sys_captouch.calibration_get_data()
                sys_captouch.calibration_request()
                self.calib_stage = 3
        elif self.state == self.CALIBRATION_STATE:
            self.calib_stage = sys_captouch.calibration_active()
            if not self.calib_stage:
                self.calib = sys_captouch.calibration_get_data()
                if self.calib:
                    self.state = self.CALIBRATION_CHECK_STATE
                    self.calib_stage = -1
                    self.calib_ok = None
        elif self.state == self.CALIBRATION_CHECK_STATE:
            if self.calib_ok is None:
                try:
                    self.calib_ok = [True] * 10

                    def check_entry(_arg):
                        _vals = [int(x) for x in _arg.split("/")]
                        return _vals[0] < 52000 and _vals[0]

                    for petalbias in self.calib["ad7147"]:
                        petal = petalbias["petal"]
                        if petal % 2:
                            labels = ["0d", "1d"]
                        else:
                            labels = ["0d", "1d", "2d"]
                        for label in labels:
                            entries = petalbias[label]
                            for entry in entries:
                                self.calib_ok[petal] = self.calib_ok[
                                    petal
                                ] and check_entry(entry)
                except Exception as e:
                    print(e)
                    self.calib_ok = False
                self.full_redraw = True
            if press_event:
                if self.button == ins.buttons.PRESSED_DOWN:
                    if self.selection_index:
                        self.state = self.SAVE_STATE
                        self.calib_prev = None
                    else:
                        self.restore_calib()
                        self.state = self.PRE_COUNTDOWN_STATE
                else:
                    self.selection_index += self.button
        elif self.state == self.SAVE_STATE:
            if not self.full_redraw:
                self.save_ok = bool(self.calib)
                if self.save_ok:
                    try:
                        with open(sys_captouch.calibration_path, "w") as f:
                            json.dump(self.calib, f, indent=4)
                    except:
                        self.save_ok = False
                if self.save_ok:
                    sys_captouch.calibration_set_trusted(True)
                self.state = self.POST_SAVE_STATE
        elif self.state == self.POST_SAVE_STATE:
            if press_event and self.button == ins.buttons.PRESSED_DOWN:
                if self.menu:
                    self.state = self.FIRST_BOOT_HINT_STATE
                else:
                    self.state = self.BASE_STATE
        elif self.state == self.FIRST_BOOT_HINT_STATE:
            if press_event and self.button == ins.buttons.PRESSED_DOWN:
                if self.menu:
                    if self.vm and not self.vm._history:
                        self.vm.replace(self.menu)
                    self.menu = None

    def draw_selection(self, ctx, selections, selection_index):
        ctx.save()
        col_fg = (0, 1, 1)
        col_bg = (0, 0, 0)
        text_space = ctx.text_width("   ")
        strings = ["["] + selections + ["]"]
        text_widths = [ctx.text_width(x) + text_space for x in strings]

        orig_x, orig_y = ctx.x, ctx.y
        ctx.rgb(*col_bg).rectangle(-self._width / 2, ctx.y - 18, self._width, 26).fill()
        ctx.move_to(orig_x, orig_y)

        new_width = sum(text_widths)
        if abs(new_width - self._width) < 0.1:
            ctx.rel_move_to(-new_width / 2, 0)
            for x, string in enumerate(strings):
                orig_x, orig_y = ctx.x, ctx.y
                ctx.rgb(*col_fg)
                if selections and selection_index == x - 1:
                    ctx.rectangle(
                        ctx.x, ctx.y - ctx.font_size + 4, text_widths[x], ctx.font_size
                    ).fill()
                    ctx.move_to(orig_x, orig_y)
                    ctx.rgb(*col_bg)
                ctx.rel_move_to(text_widths[x] / 2, 0)
                ctx.text(string)
                ctx.move_to(orig_x + text_widths[x], orig_y)
        else:
            if self._width < new_width:
                self._width += self.draw_time * 0.6
                if self._width > new_width:
                    self._width = new_width
            elif self._width > new_width:
                self._width -= self.draw_time * 0.6
                if self._width < new_width:
                    self._width = new_width
            orig_y = ctx.y
            ctx.rgb(*col_fg)
            ctx.move_to(-self._width / 2 + text_widths[0] / 2, orig_y)
            ctx.text(strings[0])
            ctx.move_to(self._width / 2 - text_widths[-1] / 2, orig_y)
            ctx.text(strings[-1])

        ctx.restore()

    def draw(self, ctx) -> None:
        if self.always_full_redraw:
            self.full_redraw = True
        ctx.text_align = ctx.CENTER
        if self.full_redraw:
            ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
            ctx.rgb(1, 0, 1)
            ctx.move_to(0, 0)
            ctx.font = ctx.get_font_name(4)
            ctx.font_size = 22
            ctx.move_to(0, -80)
            ctx.text("Captouch")
            ctx.move_to(0, -55)
            ctx.text("Calibrator")
        ctx.font = ctx.get_font_name(6)
        ctx.font_size = 18
        ctx.rgb(0, 1, 1)

        pos = -20
        ctx.move_to(0, pos)
        if self.state in [
            self.PRE_COUNTDOWN_STATE,
            self.COUNTDOWN_STATE,
            self.CALIBRATION_STATE,
        ]:
            if self.full_redraw:
                ctx.move_to(0, pos)
                ctx.text("do not touch the petals from")
                ctx.move_to(0, pos + 20)
                ctx.text("the front during calibration")
            else:
                ctx.rgb(0, 0, 0).rectangle(-120, pos + 35, 240, 20).fill().rgb(0, 1, 1)
            ctx.move_to(0, pos + 50)
            if self.state == self.COUNTDOWN_STATE:
                ctx.text("calibrating in " + str(1 + int(self.timer / 1000)))
            elif self.state == self.CALIBRATION_STATE:
                ctx.rgb(1.0, 0.5, 0.2)
                ctx.text("calibrating" + "." * (4 - self.calib_stage))
        elif self.full_redraw:
            if self.state == self.BASE_STATE:
                if sys_captouch.calibration_get_trusted():
                    ctx.text("trusted calibration found")
                    ctx.move_to(0, pos + 20)
                    ctx.text("you can draw on here to try it!")
                    ctx.move_to(0, pos + 50)
                    ctx.text("(top petals: 2D, bottom: 1D)")
                else:
                    ctx.rgb(1.0, 0, 0)
                    ctx.text("no trusted calibration found")
                    ctx.move_to(0, pos + 20)
                    ctx.text("please perform a calibration")
            elif self.state == self.CALIBRATION_CHECK_STATE:
                if self.calib_ok is None:
                    ctx.text("checking calibration...")
                elif not self.calib_ok:
                    ctx.rgb(1.0, 0, 0)
                    ctx.text("bad calibration, driver error")
                    ctx.move_to(0, pos + 20)
                    ctx.text("you should never see this")
                else:
                    if all(self.calib_ok):
                        ctx.text("calibration looks good!")
                        ctx.move_to(0, pos + 20)
                        ctx.text("you can draw on here to try it!")
                        ctx.move_to(0, pos + 50)
                        ctx.text("(top petals: 2D, bottom: 1D)")
                    else:
                        ctx.rgb(1.0, 0.5, 0.2)
                        ctx.text("issues found on petals:")
                        ctx.move_to(0, pos + 20)
                        ctx.text(
                            " ".join(
                                [str(x) for x in range(10) if not self.calib_ok[x]]
                            )
                        )
                        ctx.move_to(0, pos + 50)
                        ctx.text("check for dirt")
            elif self.state == self.SAVE_STATE:
                ctx.move_to(0, pos + 20)
                ctx.text("saving calibration...")
            elif self.state == self.POST_SAVE_STATE:
                ctx.move_to(0, pos + 20)
                if self.save_ok:
                    ctx.rgb(0, 1.0, 0)
                    ctx.text("saved calibration!")
                else:
                    ctx.rgb(1.0, 0, 0)
                    ctx.text("saving failed :/")
            elif self.state == self.FIRST_BOOT_HINT_STATE:
                ctx.move_to(0, pos)
                ctx.text("you can redo this anytime")
                ctx.move_to(0, pos + 20)
                ctx.text("in System -> Settings")
                ctx.move_to(0, pos + 40)
                ctx.text("-> Captouch Calibration")

        if self.state in [self.CALIBRATION_CHECK_STATE, self.BASE_STATE]:
            for x in range(10):
                if self.pos_prev[x] is not None:
                    pos = ((self.pos_prev[x] * 27) + 73) * captouch.PETAL_ROTORS[x]
                    # if abs(self.pos_prev[x].real) > 1 or abs(self.pos_prev[x].imag) > 1:
                    if abs(self.pos_prev[x]) > 1:
                        ctx.rgb(0.0, 0.0, 1.0)
                    else:
                        ctx.rgb(1.0, 1.0, 0.0)
                    ctx.rectangle(pos.real - 2, pos.imag - 2, 4, 4).fill()
                    self.pos_prev[x] = None
            ctx.rgb(1.0, 0.0, 0.0)
            for x in range(10):
                if self.pos[x] is not None:
                    pos = ((self.pos[x] * 27) + 73) * captouch.PETAL_ROTORS[x]
                    ctx.rectangle(pos.real - 1.5, pos.imag - 1.5, 3, 3).fill()
                    self.pos_prev[x] = self.pos[x]

        pos = 60
        ctx.move_to(0, pos)
        self.draw_selection(ctx, self.selections, self.selection_index)
        if (
            self.full_redraw
            and self.menu
            and self.state in [self.PRE_COUNTDOWN_STATE, self.CALIBRATION_CHECK_STATE]
            and self.app_is_left is not None
        ):
            ctx.move_to(0, pos + 25)
            ctx.rgb(0.0, 0.7, 0.7)
            ctx.font_size = 15
            shoulder_side = "left" if self.app_is_left else "right"
            ctx.text(f"({shoulder_side} shoulder button")
            ctx.move_to(0, pos + 40)
            if self.state == self.PRE_COUNTDOWN_STATE:
                ctx.text("down to confirm)")
            if self.state == self.CALIBRATION_CHECK_STATE:
                ctx.text("left/right to select)")
        self.draw_time = 0
        self.full_redraw = self.app_is_left is None

    def on_enter(self, vm) -> None:
        super().on_enter(vm)
        self.button = None
        self.state = self.PRE_COUNTDOWN_STATE if self.menu else self.BASE_STATE
        self._width = 0
        self.draw_time = 0
        self.always_full_redraw = True
        self.calibration_trusted = sys_captouch.calibration_get_trusted()

    def on_enter_done(self):
        self.always_full_redraw = False

    def on_exit(self):
        self.always_full_redraw = True
        self.restore_calib()

    def on_exit_done(self):
        self.always_full_redraw = False

    @classmethod
    def check_captouch(cls):
        if not sys_captouch.calibration_get_trusted():
            try:
                with open(sys_captouch.calibration_path) as f:
                    sys_captouch.calibration_set_data(json.load(f))
                sys_captouch.calibration_set_trusted(True)
            except:
                pass
        return sys_captouch.calibration_get_trusted()
