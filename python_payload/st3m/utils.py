import math
import os
import sys_kernel

try:
    import inspect
except ImportError:
    inspect = None

from st3m.goose import Any


class RingBuf:
    """Store max n values in a static buffer, overwriting oldest with latest."""

    size: int
    cursor: int

    def __init__(self, capacity: int) -> None:
        self.buf = [None] * capacity
        self.size = 0
        self.cursor = 0

    def append(self, val: any) -> None:
        self.buf[self.cursor] = val
        self.cursor += 1
        if self.cursor > self.size:
            self.size = self.cursor
        if self.cursor == len(self.buf):
            self.cursor = 0

    def _current(self) -> int:
        current = self.cursor - 1
        if current < 0:
            current = len(self.buf) - 1
        return current

    def peek(self) -> any:
        return self.buf[self._current()]

    def __repr__(self) -> str:
        return repr(list(self))

    def __len__(self) -> int:
        return self.size

    def __iter__(self):
        # could use yield from or slices, but ranges leave less memory behind
        if self.size == len(self.buf):
            for i in range(self.cursor, len(self.buf)):
                yield self.buf[i]
        for i in range(0, self.cursor):
            yield self.buf[i]

    def __getitem__(self, key: int) -> any:
        if self.size < len(self.buf):
            if key < 0:
                return self.buf[key - len(self.buf) + self.cursor]
            return self.buf[key]

        element = self.cursor + key
        if element >= len(self.buf):
            element -= len(self.buf)
        if element < 0:
            element += len(self.buf)
        return self.buf[element]


def lerp(a: float, b: float, v: float) -> float:
    """Interpolate between a and b, based on v in [0, 1]."""
    if v <= 0:
        return a
    if v >= 1.0:
        return b
    return a + (b - a) * v


def ease_cubic(x: float) -> float:
    """Cubic ease-in/ease-out function. Maps [0, 1] to [0, 1]."""
    if x < 0.5:
        return 4 * x * x * x
    else:
        return 1 - ((-2 * x + 2) ** 3) / 2


def ease_out_cubic(x: float) -> float:
    """Cubic ease-out function. Maps [0, 1] to [0, 1]."""
    return 1 - (1 - x) ** 3


def xy_from_polar(r: float, phi: float) -> tuple[float, float]:
    return (r * math.sin(phi), r * math.cos(phi))  # x  # y


def reduce(function: Any, iterable: Any, initializer: Any = None) -> Any:
    """functools.reduce but portable and poorly typed."""
    it = iter(iterable)
    if initializer is None:
        value = next(it)
    else:
        value = initializer
    for element in it:
        value = function(value, element)
    return value


def get_function_args(fun: Any) -> list[str]:
    """
    Returns a list of argument names for a function

    Excludes keyword-only arguments (those are in argspec[4] if anyone needs it)

    Uses either __argspec__ (an extension specific to our fork of micropython)
    or inspect.getfullargspec() which are very similar except the latter
    populates more fields and returns a named tuple
    """

    if hasattr(fun, "__argspec__"):
        return fun.__argspec__[0]
    elif inspect and hasattr(inspect, "getfullargspec"):
        return inspect.getfullargspec(fun)[0]
    else:
        raise NotImplementedError("No implementation of getfullargspec found")


def save_file_if_changed(filename: str, desired_contents: str) -> bool:
    save = False
    if not os.path.exists(filename):
        save = True
    else:
        with open(filename, "r") as f:
            save = f.read() != desired_contents

    if save:
        with open(filename, "w") as f:
            f.write(desired_contents)

    return save


def sd_card_plugged() -> bool:
    try:
        os.listdir("/sd")
        return True
    except OSError:
        # OSError: [Errno 19] ENODEV
        return False


def sd_card_unreliable():
    if not sd_card_plugged():
        return None
    elif sys_kernel.sd_oem() == 21930 and sys_kernel.sd_mfg() == 200:
        # for SDcards that have shown to reliably fail to download a firmware update
        # these occasionally fail to download apps too. This seems to be likely an issue
        # with the Espressif SDIO stack or PHY as there are many reports like this over many years
        # These cards tend to work fine in an SD card reader on a PC
        # The card with mfg id 200 and oem id 21930 is from the supplied SDcards at CCCamp 2023
        # We had identified two variants so far from that batch, the other variant (0, 12288) seems to have been fine
        # Two of each type had been tested so far.
        return True
    else:
        return False


def is_simulator() -> bool:
    return sys_kernel.hardware_version() == "simulator"


tau = math.pi * 2


def simplify_path(path):
    splt = path.split("/")
    if splt[0]:
        raise ValueError("absolute paths only")
    splt = [s for s in splt if s]
    iterate = True
    while iterate:
        iterate = False
        for x, s in enumerate(splt):
            if s == "..":
                if not x:
                    raise ValueError("path can't go behind root dir")
                splt.pop(x)
                splt.pop(x - 1)
                iterate = True
                break
    return "/" + "/".join(splt)


def rm_recursive(path):
    for x in os.ilistdir(path):
        subpath = path + "/" + x[0]
        if x[1] == 0x8000:
            os.remove(subpath)
        elif x[1] == 0x4000:
            rm_recursive(subpath)
    os.rmdir(path)


def mkdir_recursive(path):
    if path[0] != "/":
        raise ValueError("path must be absolute")
    path.rstrip("/")
    splt = path.split("/")
    for x in range(len(splt)):
        subpath = "/".join(splt[: (x + 1)])
        if not os.path.exists(subpath):
            os.mkdir(subpath)
        elif not os.path.isdir(subpath):
            raise FileExistsError


def wrap_text(text, line_width, ctx=None):
    """
    wraps text to stay below a certain line width and returns
    the wrapped lines as a list without newline characters.

    if no ctx is passed the line width is interpreted as number
    of characters, else it is the rasterized width in pixels.
    the latter mode uses ctx.text_width() to determine width so
    do set your ctx up in the way you'll render the result before
    calling this.

    may infinite-loop if not a single character fits the line
    or similar edge cases. we didn't spend a lot of time on
    this.
    """
    wrapped_lines = []
    if ctx is None:
        len_fun = len
    else:
        len_fun = ctx.text_width
    for line in text.split("\n"):
        if len_fun(line) <= line_width:
            wrapped_lines += [line]
        else:
            line_words = line.split(" ")
            subline_words = []
            while line_words:
                subline_words.append(line_words.pop(0))
                if len_fun(" ".join(subline_words)) > line_width:
                    if len(subline_words) == 1:
                        split_word = subline_words[0]
                        for x in range(len(split_word)):
                            if len_fun(split_word[:x]) > line_width:
                                break
                        x -= 1
                        subline_words[0] = split_word[:x]
                        line_words.insert(0, split_word[x:])
                    else:
                        line_words.insert(0, subline_words.pop())
                    wrapped_lines.append(" ".join(subline_words))
                    subline_words = []
            if subline_words:
                wrapped_lines.append(" ".join(subline_words))
    return wrapped_lines
