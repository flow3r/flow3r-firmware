from st3m.reactor import Reactor, Responder
from st3m.goose import Optional
from st3m.ui.elements import overlays
from st3m.ui.view import View, ViewManager, ViewTransitionBlend
from st3m.ui.captouch_calibrator import CaptouchCalibrator
from st3m.main_menu import MainMenu
from st3m.application import (
    BundleManager,
    ApplicationContext,
    setup_for_app,
)
from st3m import settings_menu as settings, logging, wifi
from st3m import application_settings
from st3m.input import InputState
import st3m.wifi
import st3m.utils

import sys_captouch
import sys_audio
import leds
import gc
import sys_buttons
import sys_display
import sys_mode
import bl00mbox
import os
import json

import machine
import network
import time


log = logging.Log(__name__, level=logging.INFO)


def _make_reactor() -> Reactor:
    reactor = Reactor()

    def _onoff_button_swap_update() -> None:
        left = not settings.onoff_button_swap.value
        sys_buttons.configure(left)

    settings.onoff_button_swap.subscribe(_onoff_button_swap_update)
    _onoff_button_swap_update()

    settings.onoff_wifi.subscribe(wifi._onoff_wifi_update)
    wifi._onoff_wifi_update()
    return reactor


def run_responder(r: Responder) -> None:
    """
    Run a given Responder in the foreground, without any menu or main firmware running in the background.

    This is useful for debugging trivial applications from the REPL.
    """
    reactor = _make_reactor()
    reactor.set_top(r)
    reactor.run()


def _make_compositor(reactor: Reactor, vm: ViewManager) -> overlays.Compositor:
    """
    Set up top-level compositor (for combining viewmanager with overlays).
    """
    compositor = overlays.Compositor(vm)

    # Tie compositor's debug overlay to setting.
    def _onoff_debug_update() -> None:
        compositor.enabled[overlays.OverlayKind.Debug] = settings.onoff_debug.value

    _onoff_debug_update()
    settings.onoff_debug.subscribe(_onoff_debug_update)

    # Configure debug overlay fragments.
    debug = overlays.OverlayDebug()
    debug.add_fragment(overlays.DebugReactorStats(reactor))
    debug.add_fragment(overlays.DebugBattery())
    compositor.add_overlay(debug)

    debug_touch = overlays.OverlayCaptouch()

    # Tie compositor's debug touch overlay to setting.
    def _onoff_debug_touch_update() -> None:
        compositor.enabled[
            overlays.OverlayKind.Touch
        ] = settings.onoff_debug_touch.value

    _onoff_debug_touch_update()
    settings.onoff_debug_touch.subscribe(_onoff_debug_touch_update)
    compositor.add_overlay(debug_touch)

    # Tie compositor's icon visibility to setting.
    def _onoff_show_tray_update() -> None:
        compositor.enabled[
            overlays.OverlayKind.Indicators
        ] = settings.onoff_show_tray.value

    _onoff_show_tray_update()
    settings.onoff_show_tray.subscribe(_onoff_show_tray_update)

    # Add icon tray.
    compositor.add_overlay(overlays.IconTray())
    return compositor


def run_views(views, debug_vm=True):
    reactor = _make_reactor()
    vm = ViewManager(ViewTransitionBlend(), debug=debug_vm)
    sys_mode.mode_set(2)  # st3m_mode_kind_app
    for view in views:
        vm.push(view)
    compositor = _make_compositor(reactor, vm)
    reactor.set_top(compositor)
    reactor.run()


def run_view(v: View, debug_vm=True) -> None:
    """
    Run a given View in the foreground, with an empty ViewManager underneath.

    This is useful for debugging simple applications from the REPL.
    """
    run_views([v], debug_vm)


def run_app(klass, bundle_path=None):
    app_ctx = ApplicationContext(bundle_path)
    CaptouchCalibrator.check_captouch()
    setup_for_app(app_ctx)
    run_view(klass(app_ctx), debug_vm=True)


def _yeet_local_changes() -> None:
    if st3m.utils.is_simulator():
        # not implemented in simulator
        return
    os.remove("/flash/sys/.sys-installed")
    machine.reset()


def run_main() -> None:
    log.info(f"starting main")
    log.info(f"free memory: {gc.mem_free()}")

    captouch_calibration_ok = CaptouchCalibrator.check_captouch()

    timer = None
    autostart_settings = application_settings.get_autostart()
    autostart_slug = autostart_settings["slug"] if captouch_calibration_ok else None
    if autostart_settings["slug"]:
        # crimes :>
        ctx = sys_display.ctx(sys_display.osd)
        sys_display.overlay_clip(0, 130, 240, 240)
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.gray(0)
        ctx.rectangle(-120, 30, 240, 110).fill()
        ctx.font_size = 15
        ctx.gray(0.8)
        ctx.move_to(0, 40)
        ctx.font_size = 15
        ctx.text("autostarting into:")
        ctx.gray(1)
        ctx.font_size = 20
        ctx.move_to(0, 65)
        ctx.text(f"{autostart_settings['name']}")
        ctx.font_size = 15
        ctx.move_to(0, 90)
        ctx.gray(0.6)
        ctx.text("hold any shoulder button")
        ctx.move_to(0, 105)
        ctx.text("to bypass")

        timer = time.ticks_ms() + 2000
        sys_display.update(ctx)

    sys_audio.headphones_set_volume_dB(settings.num_headphones_startup_volume_db.value)
    sys_audio.speaker_set_volume_dB(settings.num_speaker_startup_volume_db.value)
    sys_audio.headphones_set_minimum_volume_dB(settings.num_headphones_min_db.value)
    sys_audio.speaker_set_minimum_volume_dB(settings.num_speaker_min_db.value)
    sys_audio.headphones_set_maximum_volume_dB(settings.num_headphones_max_db.value)
    sys_audio.speaker_set_maximum_volume_dB(settings.num_speaker_max_db.value)

    sys_audio.headset_mic_set_gain_dB(settings.num_headset_mic_gain_db.value)
    sys_audio.onboard_mic_set_gain_dB(settings.num_onboard_mic_gain_db.value)
    sys_audio.line_in_set_gain_dB(settings.num_line_in_gain_db.value)

    sys_audio.headset_mic_set_allowed(settings.onoff_headset_mic_allowed.value)
    sys_audio.onboard_mic_set_allowed(settings.onoff_onboard_mic_allowed.value)
    sys_audio.line_in_set_allowed(settings.onoff_line_in_allowed.value)
    sys_audio.onboard_mic_to_speaker_set_allowed(
        settings.onoff_onboard_mic_to_speaker_allowed.value
    )
    if settings.onoff_headphones_detection_override.value:
        sys_audio.headphones_detection_override(
            settings.onoff_headphones_detection_override.value,
            settings.onoff_headphones_detection_override_state.value,
        )

    leds.set_brightness(settings.num_leds_brightness.value)
    sys_display.set_backlight(settings.num_display_brightness.value)

    leds.set_slew_rate(235)
    leds.set_gamma(1.0, 1.0, 1.0)
    leds.set_auto_update(False)

    leds.set_rgb(0, 255, 0, 0)
    leds.update()

    bl00mbox.Sys.clear()

    try:
        network.hostname(
            settings.str_hostname.value if settings.str_hostname.value else "flow3r"
        )
    except Exception as e:
        log.error(f"Failed to set hostname {e}")

    bundles = BundleManager()
    bundles.update()
    application_settings.synchronize_apps(bundles.bundles.values())

    menu_main = MainMenu(bundles)
    if captouch_calibration_ok:
        views = [menu_main]
        while timer:
            if time.ticks_ms() > timer:
                timer = None
        ins = InputState()
        if autostart_slug is None:
            pass
        elif ins.buttons.os + ins.buttons.app:
            log.info(f"Bypassing autostart")
        elif autostart_slug == "++back_button":
            ViewManager._ignore_press = True
        else:
            requested = [
                b
                for b in bundles.bundles.values()
                if application_settings.is_autostart(b.path)
            ]
            if len(requested) > 1:
                log.error(f"More than one bundle with slug {autostart_slug}")
            elif len(requested) == 0:
                log.error(f"Requested bundle {autostart_slug} not found")
            else:
                log.info(f"Autostarting into {autostart_slug}")
                views += [requested[0].load()]
    else:
        views = [CaptouchCalibrator(menu=menu_main)]

    run_views(views, debug_vm=False)


__all__ = [
    "run_responder",
    "run_view",
    "run_main",
    "run_app",
]
