import json
import os

from st3m import logging
from st3m import utils


log = logging.Log(__name__, level=logging.INFO)

# we are conciously only storing this on sd.
# if you ever feel like changing this there is an additional chore:
# - `synchronize` deletes entries for which no bundles is present.
# - you gotta save info on whether an app is stored on sd card or not
# - if sd card is not plugged in `synchronize` should not delete entries
#   for applications that are stored on sd card.
# noncompliance will result in "new" tag spam.
#
# ...speaking of...
#
# turn off new_tags with this:
show_new_tags = True

_settings_dir = "/sd/user_data/"
_autostart_settings_path = _settings_dir + "autostart.json"
_autostart_data = None
_application_settings_path = _settings_dir + "app_settings.json"
_application_data = None
# if this is set to "True" unknown bundles will not be marked as new.
# currently used when application settings file doesn't exist.
_application_settings_nothing_new = False

_delete_callback = None


def get_autostart():
    global _autostart_data
    if _autostart_data is None:
        try:
            with open(_autostart_settings_path, "r") as f:
                _autostart_data = json.load(f)
                log.info("Found autostart settings")
        except Exception as e:
            _autostart_data = {"slug": None, "name": None}
            log.info("Could not find autostart settings: " + str(e))
    return _autostart_data


def path_to_slug(path):
    if path is None:
        return None
    return path.rstrip("/").split("/")[-1]


def set_autostart(path, name):
    global _autostart_data
    data = {}
    data["slug"] = path_to_slug(path)
    data["name"] = name
    if data == _autostart_data:
        return
    if not utils.sd_card_plugged():
        e = "no SD card found"
        log.info("Could not save autostart settings: " + str(e))
        return
    save_fail = False
    if not os.path.exists(_settings_dir):
        try:
            utils.mkdir_recursive(_settings_dir)
        except Exception as e:
            log.warning("Could not save autostart settings: " + str(e))
            save_fail = True
    try:
        with open(_autostart_settings_path, "w") as f:
            f.write(json.dumps(data))
    except Exception as e:
        log.warning("Could not save autostart settings: " + str(e))
        save_fail = True
    if not save_fail:
        _autostart_data = data
        log.info("Saved autostart settings")


def is_autostart(path):
    return get_autostart()["slug"] == path_to_slug(path)


def _load_application_data():
    global _application_data
    if not _application_data is None:
        # already loaded
        return
    if utils.sd_card_plugged() and os.path.exists(_application_settings_path):
        try:
            with open(_application_settings_path, "r") as f:
                _application_data = json.load(f)
                log.info("Loaded application settings from SD card")
                return
        except Exception as e:
            log.info("Could not load application settings: " + str(e))
    global _application_settings_nothing_new
    _application_settings_nothing_new = True
    log.info("Creating empty application settings")
    _application_data = {}


def _save_application_data():
    global _application_data
    if _application_data is None:
        return
    if not utils.sd_card_plugged():
        e = "no SD card found"
        log.info("Could not save application settings: " + str(e))
        return
    if not os.path.exists(_settings_dir):
        try:
            utils.mkdir_recursive(_settings_dir)
        except Exception as e:
            log.info("Could not save application settings: " + str(e))
    with open(_application_settings_path, "w") as f:
        try:
            f.write(json.dumps(_application_data, indent=4))
        except Exception as e:
            log.info("Could not save application settings: " + str(e))


def _settings_access(fun):
    def inner(path, *args, **kwargs):
        if _application_data is None:
            _load_application_data()
        slug = path_to_slug(path)
        return fun(slug, *args, **kwargs)

    return inner


def synchronize_apps(bundles):
    _load_application_data()
    slugs = [path_to_slug(b.path) for b in bundles]
    for slug in slugs:
        if slug not in _application_data:
            _application_data[slug] = {}
            _application_data[slug]["new"] = not _application_settings_nothing_new
    slugs += ["++back_button"]
    for slug in _application_data:
        if slug not in slugs:
            del _application_data[slug]
    if get_autostart()["slug"] not in slugs:
        set_autostart(None, None)
    _save_application_data()
    for b in bundles:
        b.update_tags()


@_settings_access
def set_app_field(slug, field, val):
    if (
        slug in _application_data
        and field in _application_data[slug]
        and _application_data[slug][field] == val
    ):
        return
    if not slug in _application_data:
        _application_data[slug] = {}
    _application_data[slug][field] = val
    _save_application_data()


@_settings_access
def get_app_field(slug, field):
    if not slug in _application_data or not field in _application_data[slug]:
        return None
    return _application_data[slug][field]


@_settings_access
def get_app_tags(slug):
    ret = []
    if slug in _application_data:
        if "new" in _application_data[slug] and _application_data[slug]["new"]:
            ret += [ApplicationTagNew()]
        if "fav" in _application_data[slug] and _application_data[slug]["fav"]:
            ret += [ApplicationTagFav()]
    if _autostart_data and _autostart_data["slug"] == slug:
        ret += [ApplicationTagAutostart()]
    return ret


@_settings_access
def delete_app_data(slug, save=True):
    if slug in _application_data:
        del _application_data[slug]
        _save_application_data()
    if slug == get_autostart()["slug"]:
        set_autostart(None, None)


@_settings_access
def create_app_data(slug):
    set_app_field(slug, "new", True)
    _save_application_data()


def delete_app(path):
    path = utils.simplify_path(path)
    if path.startswith("/sd/apps/") or path.startswith("/flash/sys/apps/"):
        if os.path.exists(path):
            print(f"deleting {path}")
            utils.rm_recursive(path)
        else:
            print(f"can't delete {path}, doesn't exist")
    else:
        print(f"can't delete {path}, not in appication folder")
    slug = path.split("/")[-1]
    if not slug in _application_data:
        print(f"unknown slug: {slug}")
    delete_app_data(slug)
    if _delete_callback:
        _delete_callback(slug)


def set_delete_callback(fun):
    global _delete_callback
    _delete_callback = fun


class ApplicationTag:
    def __init__(self, rating, text="tag", color=(1, 1, 1)):
        # rating affects sort order. at rating = 0
        # sort order is not affected, higher ratings sort
        # on top of application menus.
        self.rating = rating
        self.text = text
        self.color = color

    def draw(self, ctx):
        # this draws starting at ctx.x and ctx.y, so when
        # you overload make sure that they are shifted properly
        y = ctx.y
        ctx.save()
        ctx.font_size = 20
        ctx.text_align = ctx.LEFT
        ctx.rel_move_to(5, 0)
        ctx.rgb(*self.color)
        ctx.text(self.text)
        x = ctx.x
        ctx.restore()
        ctx.move_to(x, y)


class ApplicationTagFav(ApplicationTag):
    def __init__(self):
        super().__init__(2, "<3", (0, 1, 1))


class ApplicationTagNew(ApplicationTag):
    def __init__(self):
        super().__init__(1, "new", (1, 1, 0))


class ApplicationTagAutostart(ApplicationTag):
    def __init__(self):
        super().__init__(0, "boot", (1, 0, 1))
