from typing import Protocol, List, Tuple, Union

class PetalLogFrame:
    @property
    def pressed(self) -> bool:
        """
        Identical to ``pressed`` of ``CaptouchPetalState``.
        """
        ...
    @property
    def pos(self) -> Union[complex, None]:
        """
        Identical to ``pos`` of ``CaptouchPetalState``.
        """
        ...
    @property
    def raw_pos(self) -> complex:
        """
        Identical to ``raw_pos`` of ``CaptouchPetalState``.
        """
        ...
    @property
    def raw_cap(self) -> float:
        """
        Identical to ``raw_cap`` of ``CaptouchPetalState``.
        """
        ...
    @property
    def mode(self) -> float:
        """
        Config mode setting that was used for recording the frame (see ``captouch.Config``).
        """
        ...
    @property
    def ticks_us(self) -> int:
        """
        Timestamp that reflects the approximate time at which the data was captured (to
        be exact, when the I2C transmission has completed). Behaves identical to the return
        type of ``time.ticks_us()`` and should only be used with ``time.ticks_diff()``
        to avoid overflow issues. Overflow occurs after ~10min.
        """
        ...

class CaptouchPetalState(Protocol):
    @property
    def pressed(self) -> bool:
        """
        True if the petal has been touched during the last ``think()`` cycle.

        May be affected by ``captouch.Config``.
        """
        ...
    @property
    def top(self) -> bool:
        """
        True if this is a top petal.
        """
        ...
    @property
    def bottom(self) -> bool:
        """
        True if this is a bottom petal.
        """
        ...
    @property
    def pos(self) -> Union[complex, None]:
        """
        Coordinates where this petal is touched or None if the petal isn't
        touched or positional output is turned off via `captouch.Config``.

        The coordinate system is rotated with the petal's orientation: The
        real part corresponds to the axis going from the center of the screen
        to the center of this petal, the imaginary part is perpendicular to
        that so that it increases with clockwise motion.

        Both real and imaginary part are centered around 0 and scaled to a
        [-1..1] range. We try to guarantee that the output can span this full
        range, but it may also go beyond.

        May be affected by ``captouch.Config``.

        See ``captouch.PETAL_ROTORS`` to align the output with the display
        coordinate system.
        """
        ...
    @property
    def raw_pos(self) -> complex:
        """
        Similar to ``.pos``, but never None. Will probably return garbage when
        petal is not pressed. It is mostly useful for interpolating data between
        petals.
        """
        ...
    @property
    def raw_cap(self) -> float:
        """
        Returns a the raw capacity reading from the petal in arbitrary units.
        The value that kind-of-sort-of corresponds to how much the pad
        is covered. Since the footprint of a finger expands when compressed
        (what a sentence), this could in theory be roughly used for pressure,
        but the data quality just doesn't cut it:

        It's mostly okay when not compared against fixed values, but rather
        some sort of floating average, but it's not really monotonic and also
        it doesn't react until the finger is a few mm away from the pad so it's
        kinda bad for proximity sensing too. It's tempting to use it for gating
        away light touches, but that results in poor performance in some
        environmental conditions. Test carefully, and best make nothing important
        depend on it.

        Normalized so that "1" corresponds to the upper hysteresis limit of the
        ``pressed`` API.

        May be affected by ``captouch.Config``.
        """
        ...
    @property
    def log(self) -> List[CaptouchPetalLogFrame]:
        """
        Raw frame output of the captouch driver. Must be enabled by ``captouch.Config``.

        Since ``think()`` and the captouch driver are running asynchronously - oftentimes the
        driver is even running asynchronously with _itself_! - the attributes provided in this
        class are conflated from several frames for the purpose of not missing short presses,
        hysteresis and filtering data. The filters are chosen to provide a good balance between
        response time and noise, but it's easy to miss datapoints entirely, or accidentially
        processing the same twice. For advanced captouch operation this is an issue.

        Every ``think()`` cycle this log is filled with the frames that occurred since the last
        capture. The lowest indices are the oldest frames, so that you could compile a complete log
        (or one cropped to arbitrary length) simply by appending new data.

        Is affected by REPL weirdness. # TODO
        """
        ...
    @property
    def position(self) -> Tuple[int, int]:
        """
        Legacy API

        Similar to raw_pos, but not normalized.
        # TODO: Add conversion formula
        """
        ...

class CaptouchState(Protocol):
    """
    State of captouch sensors, captured at some time.
    """

    @property
    def petals(self) -> List[CaptouchPetalState]:
        """
        State of individual petals.

        Contains 10 elements, with the zeroeth element being the petal closest to
        the USB port. Then, every other petal in a clockwise direction.

        Petals 0, 2, 4, 6, 8 are Top petals.

        Petals 1, 3, 5, 7, 9 are Bottom petals.
        """
        ...
    @property
    def ticks_us(self) -> int:
        """
        Timestamp of when the captouch data has been requested from the backend, i.e. when
        the ``think()`` cycle started. Mostly useful for comparing for to the same attribute
        of ``CaptouchPetalLogFrame``. Behaves identical to the return type of
        ``time.ticks_us()`` and should only be used with ``time.ticks_diff()`` to avoid
        overflow issues. Overflow occurs after ~10min.
        """
        ...

def read() -> CaptouchState:
    """
    LEGACY API

    Reads current captouch state from hardware and returns a snapshot in time.
    ``.pressed`` and ``.log`` attributes are broken in the REPL.

    Typically you'd want to use the captouch data provided by ``think()``, so this method
    is replaced with nothing. See workaround for reasoning.

    """
    ...

class Config:
    class PetalConfig:
        @property
        def mode(self) -> int:
            """
            What kind of data should be collected for this petal.

            0: No data at all

            1: Button Mode: All pads combined, no positional output

            2: 1D: Only radial position is provided

            3: 2D: Full positional output. Only available for top petals.

            Default: 3 (top petals), 2 (bottom petals)
            """
            ...
        @property
        def logging(self) -> bool:
            """
            Whether or not you want to collect the raw data log. This
            eats some CPU time proportional to the ``think()`` cycle
            time, use only when you actually do something with the data.

            Default: False
            """
            ...
    @property
    def petals(self) -> List[PetalConfig]:
        """
        Config of individual petals, indexed as in the ``CaptouchState`` object.
        """
        ...
    @classmethod
    def empty(cls) -> "Config":
        """
        Initializer method that returns a config with everything disabled. Ideal
        for ORing the requirements of different components together.
        """
        ...
    @classmethod
    def default(cls) -> "Config":
        """
        Initializer method that returns the default config, same as when entering an application.
        """
        ...
    @classmethod
    def current(cls) -> "Config":
        """
        Initializer method that returns the currently active config.
        """
        ...
    def apply(self) -> None:
        """
        Apply this config to the driver.
        """
        ...
    def apply_default(self) -> None:
        """
        Convenience method to restore defaults. same as ``Config.default().apply()`` but mildly faster
        if you already have one around.
        """
        ...
